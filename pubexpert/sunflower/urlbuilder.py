'''
Created on Sep 17, 2016

@author: Afiz
'''
from abc import ABCMeta

class SunflowerUrlBuilder:
    """
        Please feel free to extend the implementation for other services provided
        by Sunflower/Tulip by using this abstract class.
    """
    __metaclass__ = ABCMeta
    
    def _getBaseUrl(self, domain, port):
        self.params = {}
        self.base_url = "http://{domain}:{port}/sunflower/concept"
        if (domain and isinstance(domain, str)) and  (port and isinstance(port, int)):
            self.base_url = self.base_url.format(domain=domain, port=port)
        else:
            raise ValueError("Incorrect parameter(s) value(s).")        
        
    def responseFormat(self, response_format):
        if isinstance(response_format, str) and response_format.lower() in ['xml', 'json']:
            self.params["responseFormat"] = response_format.lower()
        else:
            raise ValueError("Incorrect parameter value or type.")


    def width(self, v=5):
        if isinstance(v, int) and v <= 5 and v >= 1:
            self.params['width'] = v
        else:
            raise ValueError("Incorrect parameter value or type.")
        
    def depth(self, v=5):
        if isinstance(v, int) and v <= 5 and v >= 1:
            self.params['depth'] = v
        else:
            raise ValueError("Incorrect parameter value or type.")
    
    def noPrunning(self, v=False):
        if isinstance(v, bool):
            self.params['noPruning'] = str(v).lower()
        else:
            raise ValueError("Incorrect parameter value or type.")
    
    def fullLabels(self, v=True):
        if isinstance(v, bool):
            self.params['fullLabels'] = str(v).lower()
        else:
            raise ValueError("Incorrect parameter value or type.")

class ConceptGraphUrlBuilder(SunflowerUrlBuilder):
    """
        Use this class to build url to get concept's graph from sunflower implementation. 
        It can be used once for each concept and reused for different concept params such
        as width, depth, noPrunning and fullLabels. 
        
        This class doesn't guarantee that returned concepts will resolved to full name. 
        Consider using ConceptPathsUrlBuilder class and extract full names.
    
    """
    def __init__(self, domain, port, concept):
        self._getBaseUrl(domain, port)
        if isinstance(concept, str):
            concept_vals = concept.strip().lower().split(' ')
            formatted_concept = '_'.join(concept_vals)
            self.base_url += "/{action}/{query}".format(action='graph', query=formatted_concept)
            
            
class ConceptPathsUrlBuilder(SunflowerUrlBuilder):
    """
        Use this class to build url to get concept's paths from sunflower implementation. 
        It can be used once for each concept and reuse for different concept params such
        as width, depth, noPrunning and fullLabels. 
    
    """
    def __init__(self, domain, port, concept):
        self._getBaseUrl(domain, port)
        if isinstance(concept, str):
            concept_vals = concept.strip().lower().split(' ')
            formatted_concept = '_'.join(concept_vals)
            self.base_url += "/{action}/{query}".format(action='paths', query=formatted_concept)   
            
    
