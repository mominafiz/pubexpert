'''
Created on Sep 17, 2016

@author: Afiz
'''
from pubexpert.novanet.db import MongoDb
from pubexpert.utility import config, convertToASCII
from pubexpert.sunflower.urlbuilder import ConceptPathsUrlBuilder
import requests
from pubexpert.wikify.annotate import multiProcessAnnotation
import time

width = 2
depth = 2

def wikiCategoriesForAConcept(concept):
    try:
            
        # build url for annotation and retrieve the data.
        url = ConceptPathsUrlBuilder(domain=config.sunflower_domain, port=config.sunflower_port, concept=concept)
        url.responseFormat("json")
        url.depth(v=depth)
        url.width(v=width)
        url.fullLabels()
        url.noPrunning()
        time.sleep(0.2)
        r = requests.get(url.base_url, url.params)
        
        if not r.status_code == 200:
            raise Exception("Error! Request return status code: {}".format(r.status_code))
        
        # update the data in collection
        jsondata = r.json()
        categories = set()
        
        for response_category in jsondata:
            # get category from 'concept' key
            categories.add(response_category.get('concept').split(':')[1])
            # get categores from 'edges' key
            for edge in response_category.get('edges'):
                categories.add(edge.get('name').split(':')[1])
        return list(categories), {'width':url.params.get('width'), 'depth': url.params.get('depth')}
    
    except Exception as e:
        print "Concept: {}  |  Msg: {}".format(concept, e.message)
        return [], {}
        # raise ValueError("Concept: {}  |  Msg: {}".format(concept, e.message))
#         traceback.print_exc(limit=10, file=sys.stdout)

def categorizeConcepts(collection_name, subcat):
    # connect to mongodb
    mongoConn = MongoDb(config.host, config.port)
    mongoConn.setDatabase(config.dbname)
    mongoConn.setCollection(collection_name)
    
    
    docCursr = mongoConn.getDocument({'nserc_cat_id': subcat, 'wikipedia_miner_annotation.category_request.width':{'$exists':False}}, {'wikipedia_miner_annotation.detectedTopics':1})
        
    for doc in docCursr:
        try:
            wiki_concepts = doc.get('wikipedia_miner_annotation').get('detectedTopics', [])
            category_request = {}
            for topic_dict in wiki_concepts:
                title = topic_dict.get('title', '')
                formatted_title = convertToASCII(title).strip().lower().split(' ')
                formatted_title = '_'.join(formatted_title)
                categories = wikiCategoriesForAConcept(formatted_title)
                topic_dict[unicode('categories')] = categories[0]
                # TODO: stop this multiple overwrite of same values.
                # TODO: take depth and width params from User.
                category_request = categories[1]
            # Comment for test    
            mongoConn.updateDocument({'_id': doc.get('_id')},
                                     {'$set': {'wikipedia_miner_annotation.detectedTopics':wiki_concepts,
                                               'wikipedia_miner_annotation.category_request': category_request
                                               }
                                    })
        except Exception as e:
            print "_id: {}  |  Msg: {}".format(doc.get('_id'), e.message)
    
    mongoConn.closeConnection()


def multiprocessConceptCategories(dataset_name, w, d):
    width = w
    depth = d
    multiProcessAnnotation(dataset_name, target=categorizeConcepts)
