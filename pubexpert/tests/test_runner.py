import unittest
from .test_novanet import test_urlbuilder
from .test_novanet import test_api
from .test_novanet import test_db
from .test_novanet import test_journal_log

loader = unittest.TestLoader()
suite = unittest.TestSuite()

suite.addTests(loader.loadTestsFromModule(test_urlbuilder))
suite.addTests(loader.loadTestsFromModule(test_api))
suite.addTests(loader.loadTestsFromModule(test_db))
suite.addTests(loader.loadTestsFromModule(test_journal_log))
unittest.TextTestRunner(verbosity=3).runClassification(suite)
