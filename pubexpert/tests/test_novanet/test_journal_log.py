import unittest
from novanet.journal_log import JournalLog
from utility import config

class TestJournalLog(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        super(TestJournalLog, cls).setUpClass()
    
    
    def test_UpdateJournalLog(self):
        jObj = JournalLog("1474-1733", "LSA01", "Immunology", "Nature Reviews in Immunology", \
                           3364, year_range=[2009, 2008, 2007], facet_topics=['Micro', 'Antibody'])
        
        
        jObj.connectDb(config.host, config.port, config.test_dbname)
              
        # separate find and update in calling function  
        jObj.update("http://www.fb.com", 10, 333, 'incomplete', 2009, ['a', 'b']) 
        
        jObj.update("http://www.google.com", 11, 333, 'incomplete', 2009, ['a', 'b', 'c'])
        
        jObj.update("http://www.ibm.com", 10, 10, 'done', 2008, ['x', 'z'])
        
        jObj.dropJournalLogCollection()
