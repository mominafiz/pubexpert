import unittest
import os
import json
from ...novanet.api import NovaNetAPI
from ...novanet.urlbuilder import UrlBuilder


class APITestCase(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        super(APITestCase,cls).setUpClass()
        print "Setting up testing for API class."
        def getExamples():
            cwd =  os.getcwd()
            resource_dir = os.path.join(cwd,'resources')
            return readlines(resource_dir)
            
        def readlines(resource_dir):
            fp,efp = None,None
            stringJson, errorJson = '{}','{}'
            try:
                fp=open(os.path.join(resource_dir,'api_documents.json'),'r')              
                lines = fp.readlines()    
                stringJson = ''.join([line.strip() for line in lines if line])
                 
                efp = open(os.path.join(resource_dir,'error_response.json'),'r')
                elines = efp.readlines()
                errorJson = ''.join([line.strip() for line in elines if line]) 
                 
            except IOError:
                raise Exception()
            finally:
                if fp:
                    fp.close()
                if efp:
                    efp.close()
            return stringJson, errorJson
        
        examples = getExamples()
        cls.json_data = json.loads(examples[0])

        cls.error_json_data =  json.loads(examples[1])
        cls.APIObj = NovaNetAPI()

    
    @classmethod
    def tearDownClass(self):
        super(APITestCase, self).tearDownClass()
        print "Clearing setup for API class tests."
        del APITestCase.json_data
        del APITestCase.error_json_data
        del APITestCase.APIObj
              
    def setUp(self):
        unittest.TestCase.setUp(self) 
        
    def tearDown(self):
        unittest.TestCase.tearDown(self)
        
    def test_GetData(self):
        urlObj = UrlBuilder(domain='dal.novanet.ca')  
        urlObj.setInstitution("DAL")
        urlObj.setVid("DAL")
        urlObj.setIndex(1)
        urlObj.setBulkSize(10)
        urlObj.setJsonPreference(json=True)  
        urlObj.addQuery('issn', 'exact', '1474-1733')
        urlObj.setResultLang('eng')
        urlObj.setLoc('adaptor', 'primo_central_multiple_fe')
        #urlObj.setQueryIncludes('facet_creationdate',['2008','2009']) # doesn't work. problem with keyword param 
        APITestCase.APIObj.getData(urlObj.base_url, urlObj.params)
        self.assertFalse(APITestCase.APIObj.hasError, msg="Response returned with an error during an API call.")
        
         
         
        urlObj = UrlBuilder(domain='dal.novanet.ca')  
        urlObj.setInstitution("DAL")
        urlObj.setVid("DAL")
        urlObj.setJsonPreference(json=True)  
        urlObj.addQuery('issn', 'exact', '1474-1733')
        urlObj.setResultLang('eng')
         
        APITestCase.APIObj.getData(urlObj.base_url, urlObj.params)
         
        self.assertTrue(APITestCase.APIObj.hasError, msg="Response returned with an error during an API call.")
        
        
    def test_Get(self):
        
        self.assertIsNone(APITestCase.APIObj.get("ERROR", APITestCase.json_data))
        self.assertIsInstance(APITestCase.APIObj.get("DOCSET", APITestCase.json_data), dict)
        self.assertIsInstance(APITestCase.APIObj.get("DOC", APITestCase.json_data), list)
        self.assertIsNone(APITestCase.APIObj.get("DOC233434", APITestCase.json_data), msg="I was expecting None to be found.")
             
        self.assertIsNot(APITestCase.APIObj.get("ERROR", APITestCase.error_json_data), None)
        self.assertEqual(APITestCase.APIObj.get('@CODE',APITestCase.error_json_data), '-1')
        
        
    def test_ParseDocument(self):
        docs = APITestCase.APIObj.parseDocuments(APITestCase.json_data)

        #=======================================================================
        # cwd =  os.getcwd()
        # resource_dir = os.path.join(cwd,'resources')
        # with open(os.path.join(resource_dir,'parsed_docs.txt'), 'a') as ofile:
        #     for doc in docs:
        #         doc['resource_date'] = doc.get('resource_date').isoformat()           
        #     ofile.writelines(json.dumps(docs))
        #=======================================================================
            
        self.assertEqual(len(docs), 10, msg="Expected documents are 10 for example data.")
    
    def test_TotalHits(self):
        self.assertEqual(APITestCase.APIObj.getTotalDocuments(APITestCase.json_data), 3363, msg="Should be 3363 for example.")
        
    def test_StartEndIndex(self):
        s,e = APITestCase.APIObj.getStartAndEndIndex(APITestCase.json_data)
        self.assertEqual(s, 1)
        self.assertEqual(e, 10)
        
        
    def test_GetTopicFacetList(self):
        self.assertEqual(len(APITestCase.APIObj.getTopicFacetList(APITestCase.json_data)),20)
        
        
    def test_GetFacetCreationDate(self):

        self.assertEqual(len(APITestCase.APIObj.getFacetCreationDate(APITestCase.json_data)),16)