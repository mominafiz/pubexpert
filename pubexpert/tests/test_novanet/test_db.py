import unittest, os, json
from ...novanet.db import MongoDb
from ...utility import config

class TestMongoDB(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        super(TestMongoDB, cls).setUpClass()
        
        
        def getExamples():
            cwd = os.getcwd()
            resource_dir = os.path.join(cwd, 'resources')

            fp = None
            cls.docs = []
            try:
                fp = open(os.path.join(resource_dir, 'parsed_docs.txt'), 'r')              
#                 lines = fp.readlines()    
                cls.docs = json.load(fp).get("docs")
                
            except IOError:
                raise Exception()
            finally:
                if fp:
                    fp.close()
        getExamples()
        cls.dbObj = MongoDb(config.host, config.port)
        cls.dbObj.setDatabase(config.test_dbname)
        cls.dbObj.setCollection(config.articles_collection_name)
        cls.dbObj.dropCollection()
       
    def test_InsertDocument(self):
        
        self.assertIsInstance(TestMongoDB.docs, list, msg="Expected list.")

        result = TestMongoDB.dbObj.insertDocument(TestMongoDB.docs[0])
        self.assertIsNot(result[0], None)
        
        TestMongoDB.dbObj.dropCollection()
        result = TestMongoDB.dbObj.insertDocument(TestMongoDB.docs)
        self.assertEqual(len(result), len(TestMongoDB.docs), msg="Expected {} results to be inserted.".format(len(TestMongoDB.docs)))
        
    def test_UpdateDocuments(self):
        pass
    
    def test_GetDocument(self):
        pass
    
    def test_GetCurrentDb(self):
        pass
    
