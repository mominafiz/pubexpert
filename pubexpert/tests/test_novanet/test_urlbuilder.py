import unittest, requests
from ...novanet.urlbuilder import UrlBuilder


class TestUrlBuilder(unittest.TestCase):
    def setUp(self):
        self.urlObj = UrlBuilder(domain='dal.novanet.ca')  
        self.urlObj.setInstitution("DAL")
        self.urlObj.setVid("DAL")
        self.urlObj.setIndex(1)
        self.urlObj.setBulkSize(10)
        self.urlObj.setJsonPreference(json=True)
        
    def tearDown(self):
        del self.urlObj
        
        
    def hasError(self, base_url, params):
        """
            Local function to help test cases.
        """
        r = requests.get(base_url, params=params)
        
        if not r.status_code == 200:
            raise Exception("Bad request.")
        result = r.json()
        if "ERROR" in result.get("SEGMENTS").get("JAGROOT").get("RESULT"):
            return True
        else:
            return False
        
    def test_MinUrl(self):
        self.urlObj.addQuery("any", "contains", "analytics")        
        self.assertFalse(self.hasError(self.urlObj.base_url, self.urlObj.params), msg="Error in API call.")
    
    def test_FacetLanguage(self):
        self.urlObj.setResultLang("eng")
        self.urlObj.addQuery("any", "contains", "analytics")
        
        self.assertFalse(self.hasError(self.urlObj.base_url, self.urlObj.params), msg="Error in API call.")
    
    def test_LocOfRepository(self):
        self.urlObj.setResultLang('eng')
        self.urlObj.addQuery('any', 'contains', 'analytics')
        self.urlObj.setLoc('adaptor', 'primo_central_multiple_fe')
        self.assertFalse(self.hasError(self.urlObj.base_url, self.urlObj.params), msg="Error in setLoc during API call.")
        
    def test_ISSNQuery(self):
        self.urlObj.addQuery('issn', 'exact', '1474-1733')
        self.urlObj.setResultLang('eng')
        self.urlObj.setLoc('adaptor', 'primo_central_multiple_fe')
        self.assertFalse(self.hasError(self.urlObj.base_url, self.urlObj.params), msg="Error in ISSN query during API call.")
        
    
    def test_QueryInclude(self):
        self.urlObj.addQuery('issn', 'exact', '1474-1733')
        self.urlObj.setResultLang('eng')
        self.urlObj.setLoc('adaptor', 'primo_central_multiple_fe')
        # Always fails for following params
        #self.urlObj.setQueryIncludes('facet_creationdate',['2008'])
        self.urlObj.setQueryIncludes('facet_rtype', ['reviews', 'articles'])
        self.assertFalse(self.hasError(self.urlObj.base_url, self.urlObj.params), msg="Error in Query include during API call.")
        
    def test_DateRange(self):
        self.urlObj.addQuery('issn', 'exact', '1474-1733')
        self.urlObj.setResultLang('eng')
        self.urlObj.setLoc('adaptor', 'primo_central_multiple_fe')
        self.urlObj.setQueryIncludes('facet_rtype', ['reviews', 'articles'])
        self.urlObj.setQueryIncludes('facet_creationdate', '[2006+TO+2006]')
#         self.urlObj.addQuery('dr_s','exact', '20060101')
#         self.urlObj.addQuery('dr_e','exact', '20061231')
        self.assertFalse(self.hasError(self.urlObj.base_url, self.urlObj.params), msg="Error in date range during API call.")
        
    def test_ExpandBeyondMyLib(self):
        self.urlObj.addQuery('issn', 'exact', '1474-1733')
        self.urlObj.setResultLang('eng')
        self.urlObj.setLoc('adaptor', 'primo_central_multiple_fe')
        self.urlObj.setQueryIncludes('facet_rtype', ['reviews', 'articles'])
        self.urlObj.setExpandBeyongMyLibrary(option=True)
        
        self.assertFalse(self.hasError(self.urlObj.base_url, self.urlObj.params), msg="Error in expand beyond my library API call.")
    
        
# def theSuite():
#     # discover tests using "test_*()" in all test classes.
#     suite = unittest.TestLoader().loadTestsFromTestCase(TestUrlBuilder)
#     
#     #------Alternate-----------
#     #suite = unittest.TestSuite()
#     #suite.addTest(WidgetTestCase('test_default_size'))
#     #suite.addTest(WidgetTestCase('test_resize'))
#     
#     # ------ Other alt -----------
#     # use test discover from commandline or VS test explorer
#     #suite = unittest.TestSuite().addTest(TestUrlBuilder)
#     return suite
# 
# if __name__ == '__main__':
#     unittest.main()
