import logging
from .test_log4mongo.test_handlers import MongoHandler
from .test_novanet.test_db import MongoDb
from pubexpert.utility import config
 
logger = logging.getLogger('test')
logger.addHandler(MongoHandler())
logger.warning('test')


class Test:
    def iterativeGetData(self, urlBuilderObj):
            
            
            results = self.getData(urlBuilderObj.base_url, urlBuilderObj.params)
            totaldocs = self.getTotalDocuments(results)
            start, end = self.getStartAndEndIndex(results)
            bulksize = end - start
            if bulksize == 0 or end == totaldocs:
                return 
            
            # obj = urlbuilder.UrlBuilder() 
            urlBuilderObj.setIndex(end + 1)
            self.iterativeGetData(urlBuilderObj)
    
def removeDuplicates():
    mongoConn = MongoDb(config.host, config.port)
    mongoConn.setDatabase(config.dbname)
    mongoConn.setCollection(config.articles_collection_name)
    
    
#===============================================================================
#              
# class OnlyOne(object):
#     instance = None
#     def __init__(self):
#         if not OnlyOne.instance:
#             OnlyOne.instance = self
#     def __new__(self):
#         if OnlyOne.instance:
#             return OnlyOne.instance
#         else:
#             self.__init__()
#         
#      
#      
# class OnlyOne(object):
#      
#     instance = None
#     def __new__(cls):
#         if not OnlyOne.instance:
#             OnlyOne.instance = repr(self)
#         return eval(repr(OnlyOne.instance))
#     def getConn(self):
#         return "mongo conn"
#      
# class OnlyOne(object):
#     class __OnlyOne:
#         def __init__(self):
#             self.val = None
#         def __str__(self):
#             return `self` + self.val
#          
#         def MongoConn(self):
#             return "Connecting to mongodb..."
#          
#     instance = None
#     def __new__(cls):  # __new__ always a classmethod
#         if not OnlyOne.instance:
#             OnlyOne.instance = OnlyOne.__OnlyOne()
#         return OnlyOne.instance
#     def __getattr__(self, name):
#         return getattr(self.instance, name)
#===============================================================================
