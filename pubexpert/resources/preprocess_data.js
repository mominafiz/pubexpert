
//------------------------------------------------------------------------------------
// Separate unique records and redundant records 
//------------------------------------------------------------------------------------
var titles = db.articles.raw.bsf.distinct('document_title');
for(var i =0; i<titles.length; i++){
	var r = db.articles.raw.bsf.find({document_title: titles[i]});
	if(r.count()==1){
		db.unique.articles.raw.bsf.insert(r.next());	
	}
	else if(r.count()>1){
		var docs = []
		var id;
		while(r.hasNext()){
			doc = r.next();
			docs.push(doc);
			id = doc._id;
		}
		db.redundant.articles.raw.bsf.insert({_id: doc._id, values: docs});
	}	
};
//------------------------------------------------------------------------------------
// 	Inspect redundant articles collection. Redundant article collection can be
//	ignored and just focus on unique article collection.
// 	Following step is not required if redundant articles are completely ignored.
//------------------------------------------------------------------------------------

function checkValue(catIdArr, value){
	var insertflag = true;
	for(var i=0; i<catIdArr.length;i++){
		if(value==catIdArr[i]){
			insertflag = false;
			break;
		}
	}
	return insertflag;
}

// temp.articles contains legit doc to be inserted in unique articles collection, else in conflict.articles.
db.redundant.articles.raw.bsf.find().addOption(DBQuery.Option.noTimeout).forEach(function(doc){
	var catIdArr = [];
	var insertdoc = doc.values[0];
	
	for(var i=0; i<doc.values.length; i++){
		var id = doc.values[i].nserc_cat_id;
		// check if article is published in more than one journal.
		// returns insert flag true if not published in other journal
		if(checkValue(catIdArr,id)==true){
			catIdArr.push(id);
		}
		if(doc.values[i].description!=""){
				insertdoc.description = doc.values[i].description;
		}
		
	}
	if(catIdArr.length==1){
		db.temp.articles.raw.bsf.insert(insertdoc);
	}
	else{
		// Just ignore such documents
		db.conflict.articles.raw.bsf.insert(doc);
	}
});
//------------------------------------------------------------------------------------


// separate out articles with full content (title and abstract)
db.unique.articles.raw.bsf.find().addOption(DBQuery.Option.noTimeout).forEach(function(doc){
	if(doc.description!=""){
		db.articles.bsf.insert(doc);
	}
});




//------------------------------------------------------------------------------------
//group by example for counting number of articles in each research topic 
//------------------------------------------------------------------------------------
db.unique.articles.raw.cs.group(
   {
     key: { nserc_cat_id: 1 },
     //cond: { ord_dt: { $gt: new Date( '01/01/2012' ) } },
     reduce: function( curr, result ) {
                 result.total += 1;
             },
     initial: { total : 0 }
	 //keyf: function(doc){return},
	 //finalize: function(result){}
   }
)
//------------------------------------------------------------------------------------


//------------------------------------------------------------------------------------
//	Export mongodb collection into .csv file.
//------------------------------------------------------------------------------------
mongoexport --db pubexpert --collection articles.bsf --out articles_bsf_v2_after_2000.csv --type=csv --fields bow_text_v2,nserc_cat_id --query="{'resource_date': {'$gt': ISODate('2000-01-01T00:00:00.000Z')}}"

// export
mongoexport --db pubexpert --collection articles.cs.after_2000.filtered --out articles_cs_v2_after_2000_filtered.csv --type=csv --fields _id,bow_v2,concepts,categories,label --query="{'resource_date': {'$gt': ISODate('2000-01-01T00:00:00.000Z')}}"


mongoexport --db pubexpert --collection articles.bsf.flat.c75 --out articles_bsf_after_2000_filtered_c75.csv --type=csv --fields _id,bow_v2,concepts,categories,label --query="{'resource_date': {'$gt': ISODate('2000-01-01T00:00:00.000Z')}}"
//------------------------------------------------------------------------------------


// Fix: Documents with multiple description removed to show one description.
var titles = db.articles.bsf.find({'wikipedia_miner_annotation': {$exists: false}}).toArray();
var tt = Array();
for(i=0; i<titles.length;i++){
	tt.push(titles[i]._id);	
}
var docs = db.articles.raw.bsf.find({'_id': {"$in":tt}});
for(i=0; i<docs.length(); i++){
	var mlength = 0;
	var text = "";
	
	for(j=0; j< docs[i].description;j++){
		var currlength = docs[i].description[j].length;
		if(mlength < currlength ){
			mlength = currlength;
			text = docs[i].description[j];
		}	
	}
	print(text);
	//print(docs[i]._id);
	db.articles.bsf.update({"_id": docs[i]._id},{$set:{description: text}});
}

//---------------------------------------------------------------------
// Create flat documents of words, concepts, cats, label, id and resource_date
// For our research we have trained and tested model on articles that have concepts and categories too.
//------------------------------------------------------------------------------------
var docs = db.articles.thesis.find({'wikipedia_miner_annotation.detectedTopics.categories': {$exists: true} },{'wikipedia_miner_annotation.detectedTopics':1, 'bow_text_v2':1, 'nserc_cat_id':1, 'resource_date':1});

for(i=0; i<docs.length(); i++){
	var doc = docs[i];
	var _id = doc._id;
	var bow_v2 = doc.bow_text_v2;
	var categories = new Array();
	var concepts = new Array();
	var label = doc.nserc_cat_id;
	
	// remove research topics that don't have enough articles.
	if(label=="LSA03" || label=="CS02" || label=="CS10" || label=="CS13" || label=="CS16"){
		continue;
	}
	
	for(j=0;j<doc.wikipedia_miner_annotation.detectedTopics.length; j++){
		var topic = doc.wikipedia_miner_annotation.detectedTopics[j];
		if(topic.weight>= 0.50){	
			concepts.push(topic.title.toLowerCase().split(" ").join("_"));
			if(topic.hasOwnProperty('categories')){
				var vals = topic.categories.join(' ');
				categories.push(vals);
			}
		}
	}
	
	db.articles.thesis.flat.insert({_id:_id, bow_v2:bow_v2,categories: categories.join(' '), concepts: concepts.join(' '), label:label, resource_date:doc.resource_date});
	if(i%1000==0){
		print(i);
	}
}

//---------------------------------------------------------------------------------
	

	
// ***************************************************************************
//	Non required code is in the below section.
// ***************************************************************************

//------------------------------------------------------------------------------------
// Map Reduce didn't work because the key couldn't be indexed by MR. 
// Max 1024 bytes are allowed for indexing. Text indexing before running
// mapreduce didn't help too.

var mapper = function(){
				var key = this.document_title;
				if(key.length <= 512){
					emit(key, this);
				}
			};
			
var reducer = function(key,values){
				var ids = [];
				values.forEach(function(value){
					ids.push(value);
				});
				if (ids.length == 1){
					
					return ids;
				}
			};

db.articles.raw.mapReduce(mapper,reducer,{out: {replace: "mapreduce.duplicate.articles"}});

db.mapreduce.duplicate.articles.find().forEach(function(doc){
		db.mapreduce.duplicate.articles.update({},
									{
										_id: '$value._id', document_title: '$value.document_title',
										doi: '$value.doi', endpage: '$value.endpage',
										
									}
		);
});
//------------------------------------------------------------------------------------

//--------------- Map reduce word count ---------------
var mapper = function(){
				var bow = this.bow;
				for(i=0; i<bow.length; i++){
			
						emit(bow[i], 1);
				
				}
			};
			
var reducer = function(key,values){
				var count = 0;
				values.forEach(function(value){
					count+= 1
				});
				return count;
			};

db.articles.gcm.mapReduce(mapper,reducer,{out: {replace: "wordcount.articles.gcm"}});



db.articles.gcm.update({},{$set: {'bow_text': {$regex: '^[0-9]*$', $options: 'm'}}})

//------------------------------------------------------------------------------------
// Following code is done for ACM DSP abstracts.
//------------------------------------------------------------------------------------
var docs = db.acm_abstracts.find({});

for(i=0; i<docs.length(); i++){
	var doc = docs[i];
	var _id = doc._id;
	var bow_v2 = doc.bow_text_v2;
	var categories = new Array();
	var concepts = new Array();
	for(j=0;j<doc.wikipedia_miner_annotation.detectedTopics.length; j++){
		var topic = doc.wikipedia_miner_annotation.detectedTopics[j];
		if(topic.weight>= 0.50){	
			concepts.push(topic.title.toLowerCase().split(" ").join("_"));
			if(topic.hasOwnProperty('categories')){
				var vals = topic.categories.join(' ');
				categories.push(vals);
			}
		}
	}
	var categories_str = '';
	var concepts_str = '';
	if(categories.length > 0){
		categories_str = categories.join(' ');
	}
	if(concepts.length > 0){
		concepts_str = concepts.join(' ');
	}
	db.acm_abstracts.flat.insert({_id:_id, bow_v2:bow_v2,categories: categories_str, concepts: concepts_str});	
}

//---------screw mongo shell. Put it in pymongo---------
var docs = db.acm_abstracts.flat.find({'$or':[{'concepts':''},{'categories':''}]});

for(i=0;i<docs.length();i++){
	var probdocs = db.acm_abstracts.results.find({'_id':docs[i]._id},{'_id':1, 'bow':1});
	print(probdocs.bow);
}

for(j=0; j<probdocs.length(); j++){
		print(probdocs);
		var newdoc = {};
		print(probdocs._id);
		newdoc['_id']= id;
		newdoc['bow'] = probdocs.bow;
		db.acm_abstracts.no_concepts.insert(newdoc);	
		db.acm_abstracts.results.remove({_id:id});
	}
	