
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.feature_selection.univariate_selection import SelectKBest, chi2
from sklearn.metrics import metrics
from sklearn.utils.extmath import density
import numpy as np
import os
from sklearn.externals import joblib
from pubexpert.utility import writeToFile, getAndChangeToRootDirectory
from sklearn.svm.classes import LinearSVC
import numpy
from sklearn.multiclass import OneVsOneClassifier
from sklearn.metrics import precision_recall_fscore_support as score

    
def __getVectorizer(ngram_range=(1, 1), stop_words=None, lowercase=False, max_df=1.0, min_df=1, max_features=None, binary=False, sublinear_tf=True, vocabulary=None):
    vectorizer = TfidfVectorizer(sublinear_tf=sublinear_tf, max_df=max_df, min_df=min_df, lowercase=lowercase, \
                                 ngram_range=ngram_range, stop_words=stop_words, max_features=max_features, binary=binary, vocabulary=vocabulary)  # ,stop_words='english')
    return vectorizer

def __printClassificationReport(target_test, pred, digits=4):
    print("classification report:")
    print(metrics.classification_report(target_test, pred, digits=4))

def __printConfusionMatrix(target_test, pred):  
    print("confusion matrix:")
    print(metrics.confusion_matrix(target_test, pred))

def createTrainTFIDF(train, vectorizer, sparse=True):
    """
        returns compressed sparse matrix
    """
    if sparse is None:
        raise ValueError("Parameter 'sparse' is invalid.")
    if sparse:
        return vectorizer.fit_transform(train)
    return vectorizer.fit_transform(train).todense()

def createTestTFIDFTransformation(test, vectorizer, sparse=True):
    """
        raises NotFittedError if vectorizer is not fitted first using fit or fit_transform
    """
    if sparse is None:
        raise ValueError("Parameter 'sparse' is invalid.")
    if sparse:
        return vectorizer.transform(test)
    return vectorizer.transform(test).todense()

def getFeatures(vectorizer):
    return vectorizer.get_feature_names()

def applyChiSquare(X_train, X_test, target_train, feature_names, total_features=500):
    """
        Current implementation uses SelectKBest: Select features according to the k highest scores
        
        Other options such as percentile, false positive rate, discovery rate, are also available
    """
    ch2 = SelectKBest(chi2, k=total_features)
    X_train = ch2.fit_transform(X_train, target_train)
    X_test = ch2.transform(X_test)
    
    if feature_names:
        # keep selected feature names
        feature_names = [feature_names[i] for i
                         in ch2.get_support(indices=True)]
    return X_train, X_test, feature_names

def getDimension(clf):
    try:
        return clf.coef_.shape[1]
    except:
        print "coef_ not supported"
        return 0

def getDensity(clf):
    try:
        return density(clf.coef_)
    except:
        print "coef_ not supported"
        return 0.0
    
def getClassifierName(clf):
    return str(clf).split('(')[0]

def getTopKWordsFromClassifier(clf, feature_names, target_labels, k=25):
    """
        return top k words for given classifier for each class as dictionary.
    """
    top = {}
    try:
        if hasattr(clf, 'coef_') and k > 0 and feature_names is not None:
            for i, category in enumerate(target_labels):
                t = np.argsort(clf.coef_[i])[-k:]
                top[category] = feature_names[t]
        return top
    except:
        print "coef_ not supported"
        return top

def paging(n_records, page_size=50):
    """
        returns start and end index
    """
    pages = []
    for x in xrange(0, n_records, page_size):
        pages.append([x, x + page_size])
    return pages
        
def classify(clf, X_train, X_test, target_train, target_test, verbosity=0, cv=0,average='weighted'):
    """
        Classify and predict data and returns accuracy, precision, recall and f1 score.
        This method raises deprecation warning . Please handle these warnings in your code
        by turning it off.
        
        Eg. with warnings.catch_warnings():
                warnings.filterwarnings("ignore", category=DeprecationWarning)
                
                # then call runClassification in this code block.
                
        Param:
            verbosity (int): if 1 then print classification report,
                             if 2 then print classification report and confusion matrix
    """
    test_idx = []
    
    
    clf.fit(X_train, target_train)
    pred = clf.predict(X_test)
    if len(target_test) == 0:
        pred = pred.tolist()
        probs = clf._predict_proba_lr(X_test)#.tolist()
        
        for i in xrange(X_test.shape[0]):
            test_idx.append([max(probs[i]),pred[i],i,probs[i]])
            
        return [test_idx]
    
    accuracy = metrics.accuracy_score(target_test, pred) * 100
    precision = metrics.precision_score(target_test, pred,average=average) * 100
    recall = metrics.recall_score(target_test, pred,average=average) * 100
    f1 = metrics.f1_score(target_test, pred,average=average) * 100 
    
    if verbosity > 0 and not isinstance(clf, LinearSVC):
        print "Accuracy: {:.2f}, Precision: {}, Recall: {}, F1-Score: {}".format(accuracy, precision, recall, f1) 
        print "Dimensionality: {}".format(getDimension(clf))
        print "Density: {}".format(getDensity(clf))
            
    if isinstance(clf, OneVsOneClassifier):
            print "probabilities not supported."
    else:
        
        # get probabilities
        probs = clf._predict_proba_lr(X_test)
        
        # get all paging indexes
        pages = paging(len(target_test), page_size=200)
        # write pages by pages  
        for start_idx, end_idx in pages:
            lines = []
            if end_idx > len(target_test):
                end_idx = len(target_test)
                
            idx = start_idx
            for v1, v2, v3 in zip(target_test[start_idx:end_idx], pred[start_idx:end_idx], probs[start_idx:end_idx]):
                lines.append("{actual},{predicted},{probs}\n".
                                format(
                                      actual=v1,
                                      predicted=v2,
                                      probs=' '.join([str(round(v, 4)) for v in v3])
                                  )
                             )
                test_idx.append([max(v3), v2, idx, v3])
                idx += 1
                
    if verbosity > 1:
        __printClassificationReport(target_test, pred)
        __printConfusionMatrix(target_test, pred)
   
    return [accuracy, precision, recall, f1, test_idx]

# @DeprecationWarning("This feature will be removed in next version to support split on n-similiar arrays \
# horizontally.")
def getCVIndexes(data, target, cv):   
    """
        Split TFIDF transformed matrix of corpus into train and test for both data and target.
        This function is a generator and gets each cross validation data on each call.
        
        Parameters:
            data (matrix): TFIDF matrix
            target (vector/list): list of labels of size (n samples) data
            cv: Cross validation indexes
            
        Returns:
            list of train and test data for X_train and target params.
    """
    for train_index, test_index in cv:        
        data_train = [ data[i] for i in train_index]
        data_test = [data[i] for i in test_index]
        
        target_train = [target[i] for i in train_index]
        target_test = [target[i] for i in test_index]
        
        yield [data_train, data_test, target_train, target_test]


def getModelAndVectorizerFilePath(clfname, subcat='', clf_type='multiclass', cv=False):
    """
        Fitted classifier.
        
        Params:
            clfname (str): Classifier name
            filename (str): name of the output file with extension.
            clf_type (str): multiclass or oneclass, defaults to multiclass.
            
        Return:
            filepath of the saved model
    """         
    
    cwd = getAndChangeToRootDirectory()
    resource_dir = os.path.join(cwd, 'resources', 'models')
    
        
    if not (isinstance(clf_type, str) and isinstance(clf_type, str) and isinstance(clf_type, str)):
        raise TypeError('param clf_type, subcat and clfname must be string.')
    
    if clf_type.lower() in ['multiclass', 'oneclass']:
        resource_dir = os.path.join(resource_dir, clf_type.lower())
    else:
        raise ValueError('clf_type must be string ["multiclass","oneclass"]')
    
    if cv:
        resource_dir = os.path.join(resource_dir, "cv")
        
    filename = clfname
    filename += '_' + subcat if subcat else ''
    absfilepath = os.path.join(resource_dir, filename)
    
    filename = clfname
    filename += '_' + subcat if subcat else ''
    filename += '_vectorizer'
    vecfilepath = os.path.join(resource_dir, filename)
    
    return absfilepath, vecfilepath
    
def saveModelToFile(clf, resource_dir):
    """
        
    Params:
        clfname: Classifier object after calling fit()
    """
    joblib.dump(clf, resource_dir)
    
def loadModelFromFile(resource_dir):
    return joblib.load(resource_dir)    

def __count__(doc_idxes, data, term_idx):
    c = 0 
    for idx in doc_idxes:
        c += numpy.where(data[idx, term_idx] > 0)[0].size    
    return c




#=================================== Previous implementation as a script =================
#
# def classify1(data_train, data_test, target_train, target_test):
#     
#     target_labels = list(sorted((set(target_test))))
#     
#     # Create virtual authors
#     # Convert data_test and target_test into dataframe
#     # testdf = pd.DataFrame({'Text':data_test, 'NCatId': target_test})
#     
#     if opts.print_virtauth_test:
#         data_test, target_test = createVirtualAuthors(data_test, target_test, opts.print_docs)
#     
#     #===========================================================================
#     # # ------------------    Calculate size of train and test set     -----------------
#     # data_train_size_mb = size_mb(data_train)
#     # data_test_size_mb = size_mb(data_test)
#     # 
#     # print
#     # print("%d documents - %0.3fMB (training set)" % (
#     #     len(data_train), data_train_size_mb))
#     # print("%d documents - %0.3fMB (test set)" % (
#     #     len(data_test), data_test_size_mb))
#     # print
#     # print("Extracting features from the training data using a sparse vectorizer")
#     # 
#     #===========================================================================
#     
#     #===========================================================================
#     # # -------------------------- Create TF.IDF    --------------------------------
#     # t0 = time()
#     # vectorizer = TfidfVectorizer(sublinear_tf=True, max_df=1.0, lowercase=False)  # ,stop_words='english')
#     # X_train = vectorizer.fit_transform(data_train)
#     # duration = time() - t0
#     # print("done in %fs at %0.3fMB/s" % (duration, data_train_size_mb / duration))
#     # print("n_samples: %d, n_features: %d" % X_train.shape)
#     # print
#     # 
#     # print("Extracting features from the test data using the same vectorizer")
#     # t0 = time()
#     # X_test = vectorizer.transform(data_test)
#     # duration = time() - t0
#     # print("done in %fs at %0.3fMB/s" % (duration, data_test_size_mb / duration))
#     # print("n_samples: %d, n_features: %d" % X_test.shape)
#     # print
#     #===========================================================================
#     
#     X_train, X_test = createTFIDF(data_train, data_test)
#     
#     # ----------------------- Select best feature using Chisquare    -----------------------
#     feature_names = getFeatures()
#     
#     if opts.select_chi2:
#         print("Extracting %d best features by a chi-squared test" % 
#               opts.select_chi2)
#         t0 = time()
#         ch2 = SelectKBest(chi2, k=opts.select_chi2)
#         X_train = ch2.fit_transform(X_train, target_train)
#         X_test = ch2.transform(X_test)
#         if feature_names:
#             # keep selected feature names
#             feature_names = [feature_names[i] for i
#                              in ch2.get_support(indices=True)]
#         print("done in %fs" % (time() - t0))
#         print
#     
#     if feature_names:
#         feature_names = np.asarray(feature_names)
#     
#     ##############################################################################
#     # Benchmark classifiers
#     def benchmark(clf):
#         print('_' * 80)
#         print("Training: ")
#         print(clf)
#         t0 = time()
#         clf.fit(X_train, target_train)
#         train_time = time() - t0
#         print("train time: %0.3fs" % train_time)
#     
#         t0 = time()
#         pred = clf.predict(X_test)
#         test_time = time() - t0
#         print("test time:  %0.3fs" % test_time)
#     
#         accuracy = metrics.accuracy_score(target_test, pred) * 100
#         precision = metrics.precision_score(target_test, pred) * 100
#         recall = metrics.recall_score(target_test, pred) * 100
#         f1 = metrics.f1_score(target_test, pred) * 100
#         scores = (accuracy, precision, recall, f1) 
#         print("accuracy:   %0.3f" % accuracy)
#         
#         if hasattr(clf, 'coef_'):
#             print("dimensionality: %d" % clf.coef_.shape[1])
#             print("density: %f" % density(clf.coef_))
#             if opts.print_top > 0 and feature_names is not None:
#                 print("top {} keywords per class:".format(opts.print_top))
#                 for i, category in enumerate(target_labels):
#                     top = np.argsort(clf.coef_[i])[-opts.print_top:]
#                     print "%s: %s" % (category, " ".join(feature_names[top]))
#             print
#     
#         if opts.print_report:
#             print("classification report:")
#             print(metrics.classification_report(target_test, pred, digits=4))
#     
#         if opts.print_cm:
#             print("confusion matrix:")
#             print(metrics.confusion_matrix(target_test, pred))
#     
#         print
#         clf_descr = str(clf).split('(')[0]
#         return [clf_descr, scores, train_time, test_time]
#         
