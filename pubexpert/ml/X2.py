from time import time
import numpy
import math
import threading

class ChiSquareMultiThread(threading.Thread):
    def __init__(self, group=None, target=None, name=None, args=(), kwargs=None, verbose=None):
        threading.Thread.__init__(self, group=group, target=target, name=name, args=args, kwargs=kwargs, verbose=verbose)
        self.target = target
        self.args = args
        self.kwargs = kwargs
        
    def run(self):
        self.target(self.args[0], self.args[1])

class ChiSquare(object):
    
    def __init__(self, attrVals, clusters, data, N):
        """
            Calculate simple chi-square and return features selected for each class.
             
            Params:
                attrVals:    numpy zeroed matrix of size M x k, 
                                where M is the number of terms and k is number of classes.
                clusters:    array of arrays containing indexes of documents for each class as an array.
                data:        tf.idf matrix of size N x M, where N is number of docs and
                                M is number of terms.
                N:            Number of docs
                
            See docs: Experiments on the Use of Feature Selection and Negative Evidence in Automated Text Categorization (paper)
        """
        self.attrVals = attrVals
        self.clusters = clusters
        self.data = data
        self.N, self.M = self.data.shape
        self.lock = threading.Lock()
     
    def simpleChiSquare(self, term_range_start=0, term_range_end=0):
        
        if term_range_end == 0:
            term_range_end = self.M

        
        for j in range(term_range_start, term_range_end):
            # Ptk = numpy.count_nonzero(data[:,j])
            Ptk = self.data[:, j].nonzero()[0].size
            Ptkp = self.N - Ptk
            
            # Ptk = numpy.where(data[:,j]>0)[0].size
            # Ptkp = numpy.where(data[:,j]==0)[0].size    
            for p in range(self.attrVals.shape[1]):
                
                clusterObjs = self.clusters[p]
                otherObjs = numpy.setdiff1d(range(self.N), clusterObjs)
                if clusterObjs.size == 0:
                    P1 = 0
                    P2 = 0
                else:
                    temp = self.data[clusterObjs, j].nonzero()[0].size
                    P1 = temp * 1.0 / clusterObjs.size
                    P2 = (clusterObjs.size - temp) * 1.0 / clusterObjs.size
    
                    # P1 = numpy.where(data[clusterObjs, j]>0)[0].size*1.0 /clusterObjs.size
                    # P2 = numpy.where(data[clusterObjs, j]==0)[0].size*1.0 /clusterObjs.size
     
                if otherObjs.size == 0:
                    P3 = 0
                    P4 = 0
                else:
                    temp = self.data[otherObjs, j].nonzero()[0].size
                    P4 = temp * 1.0 / otherObjs.size
                    P3 = (otherObjs.size - temp) * 1.0 / otherObjs.size
    
                    # P3 = numpy.where(data[otherObjs, j]==0)[0].size*1.0 / otherObjs.size
                    # P4 = numpy.where(data[otherObjs, j]>0)[0].size*1.0 / otherObjs.size
                       
                
                Pci = clusterObjs.size * 1.0 / self.N
                Pcip = otherObjs.size * 1.0 / self.N
                
#                 self.lock.acquire()
                if math.sqrt(Pci * Pcip * Ptk * Ptkp) == 0:
                    self.attrVals[j, p] = 0
                else:
                    self.attrVals[j, p] = (P1 * P3 - P4 * P2) / math.sqrt(Pci * Pcip * Ptk * Ptkp);
#                 self.lock.release()
            if j % 250 == 0:
                print 'terms: %d' % (j)
        return
    

def getFeaturesFromChi2Matrix(attrVals, feature_names, nfs=[]):
    """
        Get nf percent features from computed Chi2 matrix (attrVals)
        
        Params:
            attrVals (numpy 2d matrix):             chi2 computed matrix 
            feature_names (numpy array):  vocabulary to be used to matrix calculation.
            nfs (list):    list of number of features to be used for each class
            
        Out Params:
            X_train, X_test: Newly computed matrix for train and test data from feature set
            vectorizer: new vectorizer used for creating tf.idf.
    """   
    
    keyterm_indexes = []
    keyterms = set()
    
    # sort in desc score from chi square
    for p in range(len(nfs)):
        temp = numpy.argsort(attrVals[:, p])
        temp = temp[::-1]
        keyterm_indexes.append(temp[:nfs[p], ])
    
    # get terms from feature_names list using index in keyterm_indexes
    for i, ktarr in enumerate(keyterm_indexes):
        temparr = []
        for idx in ktarr:
#             print idx,
#             if attrVals[idx,i] >0: 
            if idx < len(feature_names):          
                temparr.append(feature_names[idx])  
        keyterms.update(temparr)

    return list(keyterms)

def chi2MP(start, end, qObj):
    
    attrVals, clusters, data = qObj.get()
    N = data.shape[0]
    
    M, k = attrVals.shape
    t0 = time()
    for j in range(start,end):
         
        # Ptk = numpy.count_nonzero(data[:,j])
        Ptk = data[:, j].nonzero()[0].size
        Ptkp = N - Ptk
        # Ptk = numpy.where(data[:,j]>0)[0].size
        # Ptkp = numpy.where(data[:,j]==0)[0].size    
        for p in range(k):
            clusterObjs = clusters[p]
            otherObjs = numpy.setdiff1d(range(N), clusterObjs)
            if clusterObjs.size == 0:
                P1 = 0
                P2 = 0
            else:
                temp = data[clusterObjs, j].nonzero()[0].size
                P1 = temp * 1.0 / clusterObjs.size
                P2 = (clusterObjs.size - temp) * 1.0 / clusterObjs.size
  
                # P1 = numpy.where(data[clusterObjs, j]>0)[0].size*1.0 /clusterObjs.size
                # P2 = numpy.where(data[clusterObjs, j]==0)[0].size*1.0 /clusterObjs.size
   
            if otherObjs.size == 0:
                P3 = 0
                P4 = 0
            else:
                temp = data[otherObjs, j].nonzero()[0].size
                P4 = temp * 1.0 / otherObjs.size
                P3 = (otherObjs.size - temp) * 1.0 / otherObjs.size
  
                # P3 = numpy.where(data[otherObjs, j]==0)[0].size*1.0 / otherObjs.size
                # P4 = numpy.where(data[otherObjs, j]>0)[0].size*1.0 / otherObjs.size
                     
              
            Pci = clusterObjs.size * 1.0 / N
            Pcip = otherObjs.size * 1.0 / N
  
            if math.sqrt(Pci * Pcip * Ptk * Ptkp) == 0:
                attrVals[j, p] = 0
            else:
                attrVals[j, p] = (P1 * P3 - P4 * P2) / math.sqrt(Pci * Pcip * Ptk * Ptkp)
          
        if j % 250 == 0:
            print 'terms: %d   in %fs' % (j, (time() - t0))
    return
 
#===============================================================================
# # do not touch this method
# def simpleChiSquare(attrVals, clusters, data, N):
#          
#     M, k = attrVals.shape
#     t0 = time()
#     for j in range(M):
#         
#         # Ptk = numpy.count_nonzero(data[:,j])
#         Ptk = data[:, j].nonzero()[0].size
#         Ptkp = N - Ptk
#         # Ptk = numpy.where(data[:,j]>0)[0].size
#         # Ptkp = numpy.where(data[:,j]==0)[0].size    
#         for p in range(k):
#             clusterObjs = clusters[p]
#             otherObjs = numpy.setdiff1d(range(N), clusterObjs)
#             if clusterObjs.size == 0:
#                 P1 = 0
#                 P2 = 0
#             else:
#                 temp = data[clusterObjs, j].nonzero()[0].size
#                 P1 = temp * 1.0 / clusterObjs.size
#                 P2 = (clusterObjs.size - temp) * 1.0 / clusterObjs.size
#  
#                 # P1 = numpy.where(data[clusterObjs, j]>0)[0].size*1.0 /clusterObjs.size
#                 # P2 = numpy.where(data[clusterObjs, j]==0)[0].size*1.0 /clusterObjs.size
#   
#             if otherObjs.size == 0:
#                 P3 = 0
#                 P4 = 0
#             else:
#                 temp = data[otherObjs, j].nonzero()[0].size
#                 P4 = temp * 1.0 / otherObjs.size
#                 P3 = (otherObjs.size - temp) * 1.0 / otherObjs.size
#  
#                 # P3 = numpy.where(data[otherObjs, j]==0)[0].size*1.0 / otherObjs.size
#                 # P4 = numpy.where(data[otherObjs, j]>0)[0].size*1.0 / otherObjs.size
#                     
#              
#             Pci = clusterObjs.size * 1.0 / N
#             Pcip = otherObjs.size * 1.0 / N
#  
#             if math.sqrt(Pci * Pcip * Ptk * Ptkp) == 0:
#                 attrVals[j, p] = 0
#             else:
#                 attrVals[j, p] = (P1 * P3 - P4 * P2) / math.sqrt(Pci * Pcip * Ptk * Ptkp)
#          
#         if j % 250 == 0:
#             print 'terms: %d   in %fs' % (j, (time() - t0))
#     return
# 
# def buildChi2ScoreMatrix(X_train, target_train, feature_names):
#     """
#         Builds chi2 score matrix of size N * M, where N is number of documents and
#         M is number of terms.
#         
#         Params:
#             X_train (csr matrix): TF.IDF matrix created from corpus of text data.
#             target_train (list): target/class labels of size N of X_train
#             feature_names (list): column names of X_train
#         
#             (output)- attrVals (ndarray): matrix of attribute values calculated using Chi2 formula.
#     """
#     attrVals = numpy.empty([X_train.shape[1], len(numpy.unique(target_train))], dtype=float)
# 
#     clusters = []
#     numpy_arr_target_labels = sorted(list(set(target_train)))
#     for label in numpy_arr_target_labels:  # where 9 is number of classes.
#         clusters.append(numpy.where(numpy.array(target_train) == label)[0])
#     
#     
#     simpleChiSquare(attrVals, clusters, X_train, X_train.shape[0])
#     return attrVals
#===============================================================================
