'''
Created on Sep 15, 2016

@author: Afiz
'''


import threading
from ..utility import config
from pubexpert.wikify.urlbuilder import AnnotationUrlBuilder
from pubexpert.utility.config import wikipedia_miner_port
import requests
from pubexpert.novanet.db import MongoDb
import multiprocessing
import time


class MultiThread(threading.Thread):
    def __init__(self, group=None, target=None, name=None, args=(), kwargs=None, verbose=None):
        threading.Thread.__init__(self, group=group, target=target, name=name, args=args, kwargs=kwargs, verbose=verbose)
        self.target = target
        self.args = args
        self.kwargs = kwargs
        
    def run(self):
        return self.target(self.args[0], self.args[1])
        
        
def wikiAnnotation(collection_name, subcat):
    # connect to mongodb
    mongoConn = MongoDb(config.host, config.port)
    mongoConn.setDatabase(config.dbname)
    mongoConn.setCollection(collection_name)
    
    
    # docCursr = mongoConn.getDocument({'nserc_cat_id': subcat, 'wikipedia_miner_annotation.detectedTopics': {'$exists': False}}, {'description':1})
    docCursr = mongoConn.getDocument({'nserc_cat_id': subcat, 'wikipedia_miner_annotation.detectedTopics': {'$exists': True}}, {'description':1})
    
    for doc in docCursr:
        try:
            # build url for annotation and retrieve the data.
            url = AnnotationUrlBuilder(config.wikipedia_miner_domain, wikipedia_miner_port)
            url.responseFormat("json")
            url.repeatConcepts("first")
            url.disambiguationPolicy("strict")
            url.source(doc.get('description'))
            url.minProbability(0.25)
            
            r = requests.get(url.base_url, url.params)
            
            if not r.status_code == 200:
                raise Exception("Error! Request return status code: {}".format(r.status_code))
            
            # update the data in collection
            jsondata = r.json()
            del jsondata['request']['source']
            mongoConn.updateDocument({'_id': doc.get('_id')}, {'$set': {'wikipedia_miner_annotation':jsondata}})
            del doc
            del jsondata
        except Exception, e:
            print "Doc: {}  |  Msg: {}".format(doc.get('_id'), e.message)
            # raise ValueError("Doc: {}  |  Msg: {}".format(doc.get('_id'),e.message))

    
    mongoConn.closeConnection()


def illinoisAnnotation(collection_name, subcat):
    def __init__(self):
        raise NotImplementedError("Illinois Wikifier not supported.")


def multithreadAnnotation(dataset_name, annotator=wikiAnnotation):
    """
        This methods implements multithreading for annotation. You can implement your own multithreading using 
        MultiThread class annotate.py.
        
        Params:
            dataset_name (string): One of these values ('gcm','cs')
            threads (int): If not provided defaults to number of classes    
    """
    # find db collection
    # connect to mongodb
    mongoConn = MongoDb(config.host, config.port)
    mongoConn.setDatabase(config.dbname)
    mongoConn.setCollection(dataset_name)
    
    # get distinct classes
    classes = mongoConn.distinctValues("nserc_cat_id")
    mongoConn.closeConnection()
    
    print "Starting multi-threading..."
    
    threads = []
    for subcat in classes:
        t = MultiThread(name=str(subcat), target=annotator, args=(dataset_name, subcat))
        threads.append(t)
       
    for t in threads:
        t.start()
         
    for t in threads:
        t.join()
    

def singleThreadAnnotation(dataset_name, target=wikiAnnotation):
    # connect to mongodb
    mongoConn = MongoDb(config.host, config.port)
    mongoConn.setDatabase(config.dbname)
    mongoConn.setCollection(dataset_name)
    
    # get distinct classes
    # classes = mongoConn.distinctValues("nserc_cat_id")
    classes = mongoConn.distinctValues("nserc_cat_id")
    
    for subcat in classes:
        wikiAnnotation(dataset_name, subcat)
    
    mongoConn.closeConnection()

def multiProcessAnnotation(dataset_name, target=wikiAnnotation):
    # find db collection
    
    # connect to mongodb
    mongoConn = MongoDb(config.host, config.port)
    mongoConn.setDatabase(config.dbname)
    mongoConn.setCollection(dataset_name)
    
    # get distinct classes
    # classes = mongoConn.distinctValues("nserc_cat_id")
    classes = mongoConn.distinctValues("nserc_cat_id")
    mongoConn.closeConnection()
    
    print "Starting multi-process..."

    pools = []
    
    for subcat in classes:
        p = multiprocessing.Process(name=subcat, target=target, args=(dataset_name, subcat))        
        pools.append(p)
    
    CPUS = multiprocessing.cpu_count()
    '''
    def ensureParallelism(alive_processes=0, current_process_idx=0):
        
        if current_process_idx == len(pools):
            return
        
        # start # of processes with limit of no. of CPUS.
        while not alive_processes >= CPUS:
            if not current_process_idx < len(pools):
                break
                         
            pools[current_process_idx].start()
            
            # ensure they are alive
            while not pools[current_process_idx].is_alive():
                time.sleep(1)
        
            # increment index and alive count
            current_process_idx += 1
            alive_processes += 1

        # check for all processes that terminated.
        time.sleep(5)
        temp_alive = 0
        for i in range(current_process_idx):
            if pools[i].is_alive():
                temp_alive += 1
        

        # recursive call the function to start processes if less than CPUS.
        ensureParallelism(temp_alive, current_process_idx)
#     ensureParallelism()
       
    # TODO: Optimize code by removing recursion with loop.
    '''
    
    if len(classes) < CPUS:
        CPUS = len(classes)
    alive_processes = 0
    current_process_idx = 0
    while current_process_idx < len(pools):
        while (alive_processes < CPUS) and (current_process_idx < len(pools)):
            pools[current_process_idx].start()    
            current_process_idx += 1
            alive_processes += 1
            
        # check for all terminated processes.
        time.sleep(10)
        alive_processes = 0
        for i in range(current_process_idx):
            if pools[i].is_alive():
                alive_processes += 1
   
    # before returning check all processes are terminated
    c = 1
    while c > 0:
        c = len(pools)
        for p in pools:
            if not p.is_alive():
                c -= 1
            else:
                p.join()
        time.sleep(10)
        
        
        
        
        
        
        
        
        
        
        
