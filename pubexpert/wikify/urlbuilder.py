'''
Created on Sep 14, 2016

@author: Afiz
'''
from abc import ABCMeta
from pubexpert.utility import convertToASCII

class WikipediaUrlBuilder:
    """
        Please feel free to extend the implementation for other services provided
        by wikipedia miner toolkit by using this abstract class.
    """
    __metaclass__ = ABCMeta
    
    def _getBaseUrl(self, domain, port, action):
        self.base_url = "http://{domain}:{port}/wikipedia-miner/services/{action}?"
        if (domain and isinstance(domain, str)) and  (port and isinstance(port, int)) \
                                    and (action and isinstance(action, str)):
            self.base_url = self.base_url.format(domain=domain, port=port, action=action)
        else:
            raise ValueError("Incorrect parameter(s) value(s).")        
        
    # def action(self, service):
    #     raise NotImplementedError("Action not implemented.")
    
    def responseFormat(self, response_format):
        if isinstance(response_format, str) and response_format.lower() in ['xml', 'json']:
            self.params["responseFormat"] = response_format.lower()
        else:
            raise ValueError("Incorrect parameter value or type.")

        
class AnnotationUrlBuilder(WikipediaUrlBuilder):
    """
        Sends request to default url: localhost:8080/wikipedia-miner. This class
        implements annotation /annotate service only.
        
    """
    def __init__(self, domain, port):
        # super.__init__(domain, port)
        self._getBaseUrl(domain, port, "wikify")
        self.params = {}
        
    def source(self, text):
        if isinstance(text, unicode):
            text = convertToASCII(text)
            
        if  text and isinstance(text, str):
            self.params['source'] = text
        else:
            raise ValueError("Incorrect value param.")
              
    
    def repeatConcepts(self, value):
        """
            whether repeat mentions of topics should be tagged or ignored

            Params:
                value (string): 'all', 'first_in_region', 'first'. Defaults to first_in_region.
        """
        if value and isinstance(value, str) and value.lower() in ['all', 'first_in_region', 'first']:
            self.params['repeatMode'] = value.lower()
        else:
            raise ValueError("Incorrect parameter value or type.")
        
    def linkFormat(self, value):
        """
            the format of links takes one of these values: 'wiki_id_weight', 'auto',
            'html_id', 'wiki', 'html_id_weight', 'html', 'wiki_id'
            
            Defaults to wiki
        """
        if value and isinstance(value, str) and value.lower() \
                in ['wiki_id_weight', 'auto', 'html_id', 'wiki', \
                    'html_id_weight', 'html', 'wiki_id']:
            self.params['linkFormat'] = value.lower()
            
    def minProbability(self, prob):
        """
            The system calculates a probability for each topic of whether \
            a Wikipedian would consider it interesting enough to link to. \
            This parameter specifies the minimum probability a topic must \
            have before it will be linked.
            
            Params:
                value (float): probability value in range 0-1.0. 
                               Defaults to 0.5, if this function is not called.
        
        """
        
        if prob and isinstance(prob, float) and prob >= 0.0 and prob <= 1.0:
            self.params['minProbability'] = prob
        else:
            raise ValueError("Incorrect parameter value or type.")
            
    def sourceMode(self, mode):
        """
            the type of the source document.
            
            params:
                mode (string): takes on of these values ('auto','wiki','html','url')
        """
        if mode and isinstance(mode, str) and mode >= 0.0 and mode <= 1.0:
            self.params['sourceMode'] = mode
        else:
            raise ValueError("Incorrect parameter value or type.")
    
    
    def topics(self, value):
        """
            true if to return a list of topics, otherwise false. Default is true.
        """
        if value and isinstance(value, bool) :
            self.params['topics'] = value
        else:
            raise ValueError("Incorrect parameter value or type.")
        
    def topicIndexes(self, value):
        """
            true to return details of where each topic was found within text, otherwise false
        """
        if value and isinstance(value, bool) :
            self.params['references'] = value
        else:
            raise ValueError("Incorrect parameter value or type.")
        
        
    def disambiguationPolicy(self, value):
        """
            wheither each term should be disambiguated to a single interpretation, 
            or to multiple ones
            
            Params:
                value (string): one of these values (strict, loose). Default is 'strict'.
        
        """
        if value and isinstance(value, str) and value.lower() in ['loose', 'strict'] :
            self.params['disambiguationPolicy'] = value.lower()
        else:
            raise ValueError("Incorrect parameter value or type.")
    
    
class IllinoisUrlBuilder:
    pass


