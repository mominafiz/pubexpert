from pubexpert.utility import getAndChangeToRootDirectory, readFileByEachLine, \
    config, convert2unicode, convertToASCII
import os
from pubexpert.novanet.db import MongoDb
from pubexpert.wikify.urlbuilder import AnnotationUrlBuilder
from pubexpert.utility.config import wikipedia_miner_port
import requests
from pubexpert.sunflower.categorize import wikiCategoriesForAConcept




def file_to_mongodb(collection_name):
    try:
        rootdir = getAndChangeToRootDirectory()
        filepath_dir = os.path.join(rootdir, 'resources', 'data')
        
        
        conn = MongoDb(config.host, config.port)
        conn.setDatabase(config.dbname)
        conn.setCollection(collection_name)
        
        # file shouldn't start with header (docs = 446)
        count_docs = 0
        for line in readFileByEachLine(filepath_dir, 'acm_abstracts.txt'):
            values = map(strip_trim, line.split('\t'))
                    
            if len(values) == 4 and len(values[0]) > 1:
                mongo_doc = {}
                
                mongo_doc['speaker'] = values[0]
                mongo_doc['document_title'] = values[1]
                mongo_doc['description'] = values[2]
                kwds = values[3].replace(';', ',').split(',')
                mongo_doc['keywords'] = [v for v in map(strip_trim, kwds) if len(v) > 0]
                
                
                mongo_doc = convert2unicode(mongo_doc)
                conn.insertDocument(mongo_doc)
                
                count_docs += 1
                
                if count_docs == 441:
                    break
                
        print "Total documents:    {}".format(count_docs)
        
    except IOError as e:
        print e
    except Exception as e:
        print e
    finally:
        if conn is not None:
            conn.closeConnection()

def wikify(collection_name):
    try:
        conn = MongoDb(config.host, config.port)
        conn.setDatabase(config.dbname)
        conn.setCollection(collection_name)
            
        for doc in conn.getDocument({}):
            
            # build url for annotation and retrieve the data.
            url = AnnotationUrlBuilder(config.wikipedia_miner_domain, wikipedia_miner_port)
            url.responseFormat("json")
            url.repeatConcepts("first")
            url.disambiguationPolicy("strict")
            url.source(doc.get('description'))
            url.minProbability(0.50)
            
            r = requests.get(url.base_url, url.params)
            
            if not r.status_code == 200:
                raise Exception("Error! Request return status code: {}".format(r.status_code))
            
            # update the data in collection
            jsondata = r.json()
            del jsondata['request']['source']
            conn.updateDocument({'_id': doc.get('_id')}, {'$set': {'wikipedia_miner_annotation':jsondata}})
            del doc
            del jsondata
    except Exception, e:
        print "Doc: {}  |  Msg: {}".format(doc.get('_id'), e.message)
        # raise ValueError("Doc: {}  |  Msg: {}".format(doc.get('_id'),e.message))
    finally:
        if conn:
            conn.closeConnection()
      
      
def wiki_categorize(collection_name):
    
    try:
        conn = MongoDb(config.host, config.port)
        conn.setDatabase(config.dbname)
        conn.setCollection(collection_name)
        
        for doc in conn.getDocument({},{'wikipedia_miner_annotation.detectedTopics':1}):
            wiki_concepts = doc.get('wikipedia_miner_annotation').get('detectedTopics', [])
            category_request = {}
            for topic_dict in wiki_concepts:
                title = topic_dict.get('title', '')
                formatted_title = convertToASCII(title).strip().lower().split(' ')
                formatted_title = '_'.join(formatted_title)
                categories = wikiCategoriesForAConcept(formatted_title)
                topic_dict[unicode('categories')] = categories[0]
                # uses default depth and width= 2
                category_request = categories[1]
            
            # Comment for test    
            conn.updateDocument({'_id': doc.get('_id')},
                                     {'$set': {'wikipedia_miner_annotation.detectedTopics':wiki_concepts,
                                               'wikipedia_miner_annotation.category_request': category_request
                                               }
                                    })
    except Exception as e:
        print "_id: {}  |  Msg: {}".format(doc.get('_id'), e.message)
    finally:
        if conn:
            conn.closeConnection() 
      
def strip_trim(string):
    if string is not None:
        string = string.strip()
        if string.startswith('"'):
            string = string[1:-1].strip()
            
        return string
    return ""

if __name__ == "__main__":
    collection_name = 'acm_abstracts'
#     file_to_mongodb(collection_name)
    wikify(collection_name)
    wiki_categorize(collection_name)