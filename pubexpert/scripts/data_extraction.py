from pubexpert.novanet.urlbuilder import UrlBuilder
from pubexpert.novanet.api import NovaNetAPI, Document
from pubexpert.novanet.journal_log import Status, JournalLog
from pubexpert.novanet.db import MongoDb
from pubexpert.utility import config, getAndChangeToRootDirectory
import sys
import os
from pubexpert.utility import getFileContent
import traceback
from pubexpert.utility.decorator import decorateHeading
from pubexpert.utility.config import resource_path as resource_dir

def build_query(issn, index, year=None, bulksize=None):

    urlObj = UrlBuilder(domain='dal.novanet.ca')  
    urlObj.setInstitution("DAL")
    urlObj.setVid("DAL")
    urlObj.setIndex(index)
    urlObj.setBulkSize(bulksize)if bulksize else urlObj.setBulkSize()
    urlObj.setJsonPreference(json=True)  
    urlObj.addQuery('issn', 'exact', issn)
    urlObj.setResultLang('eng')
    urlObj.setLoc('adaptor', 'primo_central_multiple_fe')
    urlObj.setQueryIncludes('facet_rtype', ['reviews', 'articles']) 
    if year:
        urlObj.setDateRange(year, year)
    urlObj.setExpandBeyongMyLibrary(option=True)

    return urlObj


def restart_from_year(issn, year_range):
    """
        Read the log collection (journal_log) from mongodb for a given issn 
        and list of years for which data is extracted.
        
        Parameters:
            issn (string):        ISSN number of the journal
            year_range (list):    list of years in which articles are published.
        
        Return:
            year (list): year_range - years from log
    """
    years = []
    
    try:
        conn = MongoDb(config.host, config.port)
        conn.setDatabase(config.dbname)
        conn.setCollection(config.journal_log_collection_name)
             
        for d in conn.getDocument({'issn':issn}):
            docs = d.get('docs_by_year')
            for doc in docs:
                year_in_doc = doc.get('year')
                if doc.get('status') == getattr(Status.done, 'value'):
                    years.append(year_in_doc)
        years = sorted(years, reverse=True)
        years = sorted(list((set(year_range)).difference(set(years))), reverse=True)
    
    except Exception as ex:
        print "Failed to get year list. Error Message: {}".format(ex.msg)
    
    return years
       
@decorateHeading 
def format_heading(string):
    return string       


def write_journal_log_to_file(resource_dir, lines=[]):
    resource_dir = getAndChangeToRootDirectory()
    with open(os.path.join(resource_dir, 'journallog_output_cs.txt'), 'a') as outputfile:
        outputfile.writelines(lines)

def extract_data(year, issn, year, index=0):
    """    
            Extracts data using an API and stores in mongodb
    """
    status = None
    total_docs_to_be_inserted = 0
    api_total_docs = 0
    total_inserted_in_db = 0
    
    try:               
        mongoConnObj = MongoDb(config.host, config.port)
        mongoConnObj.setDatabase(config.dbname)
        mongoConnObj.setCollection(config.articles_collection_name)
        
        apiObj = NovaNetAPI()
        qObj = build_query(issn, index, year)
        
        # get document page by page using generator
        genObj = apiObj.exhaustiveGet(qObj)
        
        # iterate of generator object 
        for return_set in genObj:  
            docs = []     
            api_total_docs = return_set[0]       
            
            for d in return_set[1]:
                # if exists don't add in docs
                cursorObj = mongoConnObj.getDocument({'document_title':d.get('document_title')}, multi=True)
                if cursorObj.count() == 0 and d.get('description') != "" and d.get('document_title') != "":
                    d[getattr(Document.nserc_cat_id, 'value')] = line[1]
                    docs.append(d)
            
            total_docs_to_be_inserted += len(docs)       
            # Inserts documents and return list of inserted ids
            total_inserted_in_db += len(mongoConnObj.insertDocument(docs))
              
        # write logs
        write_journal_log_to_file(resource_dir, lines="Year: {}    ToBeInserted: {}    TotalAPIDocs: {}    TotalInserted: {}\n" \
                .format(year, total_docs_to_be_inserted, api_total_docs, total_inserted_in_db))
              
            
        status = getattr(Status.done, 'value', 'error')

    except Exception as e:
        status = getattr(Status.incomplete, 'value', 'error')
        # TODO: All errors into the file using error logging.
        print sys.exc_info()[:2], str(e), traceback.print_tb(sys.exc_info()[2])
        
    finally:
        if mongoConnObj is not None:
            mongoConnObj.closeConnection()
        
        # return_set = (total number of documents, parsed documents, topics in particular year, return url)
        if return_set:
            return [return_set[3], len(return_set[1]), return_set[0], status, year, return_set[2]]
                
        
        
if __name__ == '__main__':
    outputfile = None
    
    
    # TODO: Ask the user for an output file and path to write journal logs.
    # TODO: Ask the user to enter input file in format: ISSN, Research Topic Label, Journal Name, Research Topic Name 
       
    try:
        
        issn_list = getFileContent(resource_dir, 'issn_list.txt')
        
        for issn in issn_list:
            # skip comment line
            if issn[0] == '#':
                continue
            # split each line into list
            line = [v.strip() for v in issn.split(';')]     
            
            # A request to an API to extract meta information such as facet creation date and
            # topics, total number of records
            apiObj = NovaNetAPI()
            qObj = build_query(line[0], 0, bulksize=2)
            result = apiObj.getData(qObj.base_url, qObj.params)    
            total = apiObj.getTotalDocuments(result)
            year_range = apiObj.getFacetCreationDate(result)
            topics = apiObj.getTopicFacetList(result)
            
            # create journal log object
            jlogObj = JournalLog(line[0], line[1], line[3], line[2], total, \
                                         year_range=year_range, facet_topics=topics)
            
            # write log to the file
            write_journal_log_to_file(resource_dir, lines=format_heading("ISSN: {}    Sub-CatId: {}    TotalDocs: {}" \
                                                               .format(line[0], line[1], total)))
            
            # restart if failed before
            year_range = restart_from_year(line[0], year_range)
            
            if len(year_range) == 0:
                write_journal_log_to_file(resource_dir, lines=["No more articles left to extract.\n"])
                

            # search by year to avoid reaching max query depth = 2000 records.
            # In some cases year_range is not provided in facet. Therefore, else condition.
            for year in year_range:
                logs = extract_data(year, line[0], 0, year)
            else:
                logs = extract_data('NA', line[0], 0, year)
            
            # log in db
            jlogObj.connectDb(config.host, config.port, config.dbname)                
            jlogObj.update(last_url=logs[0], total_docs_retrieved=logs[1], \
                            total_docs=logs[2], status=logs[3], year=logs[4], topics=logs[5])     
        
        else:
            write_journal_log_to_file(resource_dir, lines=format_heading("Reached end of the ISSN list."))
         
    except Exception:
        write_journal_log_to_file(resource_dir, lines=format_heading("{}\n{}\n".format(sys.exc_info()[0], sys.exc_info()[1])))
        # TODO: Error to the log file 
        print traceback.print_tb(sys.exc_info()[2])

    finally:
        print "Check 'preprocess_data.js file for remove unnecessary articles.\
                It is required to do additional cleaning because this \
                program is unable to take care of all odd cases."
