# -*- coding: utf-8 -*-
from pubexpert.utility import getAndChangeToRootDirectory, config, writeToFile,\
    convertToASCII
import os
import re
from pubexpert.novanet.db import MongoDb
from collections import Counter
import numpy
from sklearn.metrics import metrics
from pubexpert.scripts.hierarchical_results import print_classification_summary

if __name__ == '__main__':
    conn = MongoDb(config.host, config.port)
    conn.setDatabase(config.dbname)
    conn.setCollection('articles.hc.90per.parent.r33') #'articles.hc.100per.all_levels.r33'
    
    og, bow, boc,bok = [],[],[],[]
    
    
    for doc in conn.getDocument({},{'bow_result':1, 'boc_result':1, 'bok_result':1, 'label':1}):
        og.append(doc.get('label'))
        bow.append(doc.get('bow_result').keys()[0])
#         boc.append(doc.get('boc_result').keys()[0])
#         bok.append(doc.get('bok_result').keys()[0])           
    conn.closeConnection()     

    print "Classification report for BOW: "
    print print_classification_summary(og, bow, average=['macro','weighted','micro'])
#     print "Classification report for BOC: "
#     print print_classification_summary(og, boc, average=['macro','weighted','micro'])
#     print "Classification report for BOK: "
#     print print_classification_summary(og, bok, average=['macro','weighted','micro'])  
    
      
#===============================================================================
# if __name__ == '__main__':
# #     newconn = MongoDb(config.host, config.port)
# #     newconn.setDatabase(config.dbname)
# #     newconn.setCollection('acm_abstracts.flat')   
# #     
# #     ids = [doc.get('_id') for doc in newconn.getDocument({'concepts':''})]
# #     newconn.closeConnection()
# #     
#     
#     
# #     newconn = MongoDb(config.host, config.port)
# #     newconn.setDatabase(config.dbname)
# #     
# #     newconn.setCollection('acm_abstracts.results')
# #     newconn.updateDocument({'_id': {'$in':ids}}, {'$unset':{'boc':'', 'bok':''}},multi=True)
# #     
# #     newconn.closeConnection()
# 
# 
#     newconn = MongoDb(config.host, config.port)
#     newconn.setDatabase(config.dbname)
#     newconn.setCollection('acm_abstracts.results')   
#    
#     
#     ogconn = MongoDb(config.host, config.port)
#     ogconn.setDatabase(config.dbname)
#     ogconn.setCollection('acm_abstracts')
#     
#     fp = os.path.join(getAndChangeToRootDirectory(), 'resources','output')
#     fn = 'acm_abstracts_results_no_lr.txt'
#     
#     for doc in newconn.getDocument({}, {'multi_labels_result':1}):
#         ogCursor = ogconn.getDocument({'_id':doc.get('_id')}, {'document_title':1, 'speaker':1,'description':1,'keywords':1})
#         for ogdoc in ogCursor:
#             
#             stringformat = u"{}\t{}\t{}\t{}\t{}\n".format(ogdoc.get('speaker'),\
#                                         ','.join(doc.get('multi_labels_result')),
#                                         ogdoc.get('document_title'),ogdoc.get('description'),u','.join(ogdoc.get('keywords')))
#             stringformat = stringformat.encode('utf-8')
#             writeToFile(fp, fn, stringformat, 'a')
#     
#     newconn.closeConnection()
#     ogconn.closeConnection()
#===============================================================================
    


#===============================================================================
# if __name__ == '__main__':
#     newconn = MongoDb(config.host, config.port)
#     newconn.setDatabase(config.dbname)
#     newconn.setCollection('articles.cs.flat')
#      
#     newconn.updateDocument({}, {'$unset':{'bow_boc_embedding':'', 'bow_bok_embedding':'','bow_boc_bok_embedding':'' }}, multi=True)
#     newconn.closeConnection()
#     cursr = newconn.getDocument({})
#     for doc in cursr:
#         embedded_text = doc.get('bow_v2')
#         embedded_text += ' '
#         embedded_text += doc.get('concepts')
#          
#         embedded_text1 = doc.get('bow_v2')
#         embedded_text1 += ' '
#         embedded_text1 += doc.get('categories')
#          
#         embedded_text2 = doc.get('bow_v2')
#         embedded_text2 += ' '
#         embedded_text2 += doc.get('categories')
#         embedded_text2 += ' '
#         embedded_text2 += doc.get('concepts')
#          
#         newconn.updateDocument({'_id': doc.get('_id')}, {'$set': {'bow_boc_embedding':embedded_text,
#                                                                   'bow_bok_embedding':embedded_text1,
#                                                                   'bow_boc_bok_embedding': embedded_text2}})
#          
#     newconn.closeConnection()
#===============================================================================
    
#===============================================================================
# if __name__ == '__main__':
#     rootpath = getAndChangeToRootDirectory()
#     dirpath = os.path.join(rootpath, 'resources', 'output')
#     with open(os.path.join(dirpath, 'temp_classi_result.txt')) as infile, \
#             open(os.path.join(dirpath, 'temp_classi_csv.csv'), 'w') as outfile:
#         for line in infile.readlines():
#             if line.strip() == '' or line.strip() is None:
#                 continue
#             line = re.sub('\s+', ',', line)
#             outfile.write(line)
#             outfile.write('\n')
#===============================================================================

#===============================================================================
# if __name__ == '__main__':
#     newconn = MongoDb(config.host, config.port)
#     newconn.setDatabase(config.dbname)
#     newconn.setCollection('articles.gcm.cs.bsf.results.r33.exclude_classes')
#     
#     conn = MongoDb(config.host, config.port)
#     conn.setDatabase(config.dbname)
#     conn.setCollection('articles.cs')
#     
#     conn1 = MongoDb(config.host, config.port)
#     conn1.setDatabase(config.dbname)
#     conn1.setCollection('articles.cs.flat')
#     
#     try:
#         
#         dlengths = []
#         clengths = []
#         klengths = []
#         
#         for d in newconn.getDocument({}, {'bow':0, 'boc':0, 'bok':0}):
# #             rkeys = []
# #             rkeys.append(d.get('bow_result').keys()[0])
# #             rkeys.append(d.get('boc_result').keys()[0])
# #             rkeys.append(d.get('bok_result').keys()[0])
#             
# #             counterKeys = Counter()
# #             counterKeys.update(rkeys)
# #             key = counterKeys.most_common(1)[0][0]
#             
#             key = d.get('bow_result').keys()[0]
#     
#             if key != d.get('label') and d.get('label')[:-2]=="CS":                     
#                 idocs = [dd for dd in conn1.getDocument({'_id': d.get('_id')}, {'concepts':1, 'categories':1})]
#                 
#                 isdoc = False
#                 for cdoc in idocs:
#                     concepts = len(cdoc.get('concepts').split(' '))
#                     categories = len(cdoc.get('categories').split(' '))
#                     if concepts > 0 and categories > 0:
#                         clengths.append(concepts)
#                         klengths.append(categories)
#                         isdoc = True      
# 
#                 del idocs
#                 
#                 
#                 if isdoc==True:
#                    
#                     ccdocs = [ccd for ccd in conn.getDocument({'_id': d.get('_id')}, {'description':1})]
#                     for cdoc in ccdocs :
#                         dlengths.append(len(cdoc.get('description')))
# 
#                     
#         from scipy.stats import norm
#         mu, std = norm.fit(dlengths)
#         print "Description:"
#         print "Mean: " + str(mu)
#         print "sd: " + str(std)
#         nparray = numpy.array(dlengths)
#      
#         mu, std = norm.fit(clengths)
#         print "Concepts:"
#         print "Mean: " + str(mu)
#         print "sd: " + str(std)
#         nparray = numpy.array(clengths)
#          
#         mu, std = norm.fit(klengths)
#         print "Categories:"
#         print "Mean: " + str(mu)
#         print "sd: " + str(std)
#         nparray = numpy.array(klengths)
#     
#     finally:
#         newconn.closeConnection()
#         conn1.closeConnection()
#         if conn:
#             conn.closeConnection()
#===============================================================================

# Histogram of length of description ================================
#===============================================================================
# from pubexpert.novanet.db import MongoDb
# from pubexpert.utility import config, convertToASCII
# from pubexpert.utility.plots import histogram
# from scipy.stats import norm
# from scipy.stats.stats import scoreatpercentile
# import numpy
# 
# if __name__ == "__main__":
#     
#     newconn = MongoDb(config.host, config.port)
#     newconn.setDatabase(config.dbname)
#     newconn.setCollection('articles.bsf.flat')
#     
#     conn = MongoDb(config.host, config.port)
#     conn.setDatabase(config.dbname)
#     conn.setCollection("articles.bsf")
#     # text = convertToASCII()
#     # conn.updateDocument({"title":"A novel policy-driven reversible anonymisation scheme for XML-based services"},{"$set":{"description":"This paper proposes a reversible anonymisation scheme for XML messages that supports fine-grained enforcement of XACML-based privacy policies. Reversible anonymisation means that information in XML messages is anonymised, however the information required to reverse the anonymisation is cryptographically protected in the messages. The policy can control access down to octet ranges of individual elements or attributes in XML messages. The reversible anonymisation protocol effectively implements a multi-level privacy and security based approach, so that only authorised stakeholders can disclose confidential information up to the privacy or security level they are authorised for. The approach furthermore supports a shared secret based scheme, where stakeholders need to agree to disclose confidential information. Last, it supports time limited access to private or confidential information. This opens up for improved control of access to private or confidential information in XML messages used by a service oriented architecture. The solution provides horizontally scalable confidentiality protection for certain types of big data applications, like XML databases, secure logging and data retention repositories."}})
#     docs = conn.getDocument({}, {'description':1})
#     dlengths = []
#     clengths = []
#     klengths = []
#     
#     for d in docs:
#         
#         
#         cdocs = newconn.getDocument({'_id': d.get('_id')}, {'concepts':1, 'categories': 1})
#         for cdoc in cdocs:
#             concepts = len(cdoc.get('concepts').split(' '))
#             categories = len(cdoc.get('categories').split(' '))
#             if concepts > 0 and categories > 0:
#                 dlengths.append(len(d.get('description')))
#                 clengths.append(concepts)
#                 klengths.append(categories)
#             
#     mu, std = norm.fit(dlengths)
#     print "Description:"
#     print "Mean: " + str(mu)
#     print "sd: " + str(std)
#     nparray = numpy.array(dlengths)
# 
#     mu, std = norm.fit(clengths)
#     print "Concepts:"
#     print "Mean: " + str(mu)
#     print "sd: " + str(std)
#     nparray = numpy.array(clengths)
#     
#     mu, std = norm.fit(klengths)
#     print "Categories:"
#     print "Mean: " + str(mu)
#     print "sd: " + str(std)
#     nparray = numpy.array(klengths)
#     
#     conn.closeConnection()
#     newconn.closeConnection()
#===============================================================================

#===============================================================================
# if __name__ == "__main__":
#     conn = MongoDb(config.host, config.port)
#     conn.setDatabase(config.dbname)
#     conn.setCollection("articles.cs.after_2005")
#     
#     classes = conn.distinctValues("nserc_cat_id")
#     
#     for subcat in classes:
#         cursr = conn.getDocument({"nserc_cat_id": subcat}, {"_id":0})
#         docs = [d for d in cursr[:250]]
#         
#         nconn = MongoDb(config.host, config.port)
#         nconn.setDatabase(config.dbname)
#         nconn.setCollection("articles.cs.after_2005.mp.test")
#         
#         inserted = nconn.insertDocument(docs)
#         if len(docs)==len(inserted):
#             print "Ok: {}".format(subcat)
# 
#         nconn.closeConnection()
#         
#     conn.closeConnection()
#     
#===============================================================================
    
#===============================================================================
# import logging
# import random
# import threading
# import time
# import os
# from utility import writeToFile, readCSVToDF
# from sklearn.cross_validation import train_test_split
# from ml.classifier_helper import __getVectorizer, createTrainTFIDF, getFeatures
# import numpy
# from ml.X2 import simpleChiSquare, ChiSquare, ChiSquareMultiThread
# 
# 
# if __name__=="__main__":
#     filename = 'sample_articles_gcm.csv'
#     outputfile = 'sample_articles_gcm.csv.npy'
#     print "Input file: {}\nOutput file: {}\n".format(filename, outputfile)
#     cwd = os.path.split(getAndChangeToRootDirectory())[0]
#     cwd = os.path.split(cwd)[0]
#     dirpath = os.path.join(cwd, 'resources', 'chi2')
#     
#     if os.path.exists(os.path.join(dirpath, outputfile)):
#         overwrite = raw_input("Do you want to overwrite file '{}':[y/n] ".format(outputfile))
#         if overwrite.lower() == "y":
#             writeToFile(dirpath, outputfile, '', 'w')
#             
#     
#     df = readCSVToDF(filename,dirpath=os.path.join(cwd, 'resources', 'data'), header_names=['NCatId', 'Text'])
#     
#     data = [s[0] for s in df.as_matrix(['Text']).tolist()]
#     target = [s[0] for s in df.as_matrix(['NCatId']).tolist()]
#     
#     
#     data_train, data_test, target_train, target_test = train_test_split(data, target, test_size=0.3, random_state=33)
#     
#     vectorizer = __getVectorizer(max_df=1.00, min_df=1)
#     X_train = createTrainTFIDF(data_train, vectorizer)
#     feature_names = getFeatures(vectorizer)
#     
#     print "Total docs: {0}".format(X_train.shape[0])
#     print "Total features: {}".format(X_train.shape[1])
#     print "Applying chi2 ....."
#     
#     classes = sorted(list(set(target_train)))
#     attrVals = numpy.empty([X_train.shape[1], len(numpy.unique(classes))], dtype=float)
#     
#     clusters = []
#     for label in classes:  # where 9 is number of classes.
#         clusters.append(numpy.where(numpy.array(classes) == label)[0])
#     
#     chi2Obj= ChiSquare(attrVals, clusters, X_train, X_train.shape[0])
#     threads = []
#     thread_name = 1
#     for i in range(0,X_train.shape[1],X_train.shape[1]/5):
#         end = i + X_train.shape[1]/5
#         if end > X_train.shape[1]:
#             end = X_train.shape[1]
#             
#         t = ChiSquareMultiThread(name=str(thread_name), target=chi2Obj.simpleChiSquare, args=(i, end))
#         thread_name+=1
#         threads.append(t)
#         
#     for t in threads:
#         t.start()
#         
#     for t in threads:
#         t.join()
#         
# #     simpleChiSquare(attrVals, clusters, X_train, X_train.shape[0])
#     
#     numpy.save(os.path.join(dirpath, filename), attrVals)  
#     
#                 
#     print 'x' * 80
#===============================================================================

#===============================================================================
# class Test():
#     def __init__(self, attrVals, clusters, data, N):
#         self.attrVals = attrVals
#         self.clusters = clusters
#         self.data = data
#         
#         self.lock = threading.Lock()
#         
#     def __simpleChiSquare__(self, term_range_start=0, term_range_end=0):
#         
#         if term_range_end == 0:
#             term_range_end = self.M
#         
#         for j in range(term_range_start, term_range_end):
# #             self.lock.acquire()
#             multiplier = j*10
# #             self.lock.release()
#             for i in range(5):
#                 
#                 multiplier+=1
#                 self.lock.acquire()
#                 print "Thread: {}    Value: {}".format(threading.currentThread().getName(), multiplier)
#                 self.lock.release()
#             
# testObj = Test([3, 3, 34, 34], [[3, 87, 4], [4, 3, 1]], [[1, 2, 4], [6, 2, 4]], 10)
# threads = []
# t = threading.Thread(target=testObj.__simpleChiSquare__, name="1", args=(0, 5))
# threads.append(t)
# t = threading.Thread(target=testObj.__simpleChiSquare__, name="2", args=(5, 10))
# threads.append(t)
# t = threading.Thread(target=testObj.__simpleChiSquare__, name="3", args=(10, 15))
# threads.append(t)
# t = threading.Thread(target=testObj.__simpleChiSquare__, name="4", args=(15, 20))
# threads.append(t)
# 
# for t in threads:
#     t.start()
#     
# for t in threads:
#     t.join()
# 
# print threading.currentThread().getName()
#===============================================================================

#===============================================================================
# logging.basicConfig(level=logging.DEBUG,
#                     format='%(asctime)s (%(threadName)-2s) %(message)s',
#                     )
# 
# class ActivePool(object):
#     def __init__(self):
#         super(ActivePool, self).__init__()
#         self.active = []
#         self.lock = threading.Lock()
#         t = threading.Thread(target=self.makeActive, name=str(1), args=("a"))
#         t.start()
#         
#     def makeActive(self, name):
#         with self.lock:
#             self.active.append(name)
#             logging.debug('Running: %s', self.active)
#     def makeInactive(self, name):
#         with self.lock:
#             self.active.remove(name)
#             logging.debug('Running: %s', self.active)
# 
# def worker(s, pool):
#     logging.debug('Waiting to join the pool')
#     with s:
#         name = threading.currentThread().getName()
#         pool.makeActive(name)
#         time.sleep(0.1)
#         pool.makeInactive(name)
# 
# pool = ActivePool()
# pool.makeActive("A")
# pool.makeInactive("A")
#  
# s = threading.Semaphore(2)
# for i in range(4):
#     t = threading.Thread(target=worker, name=str(i), args=(s, pool))
#     t.start()
#===============================================================================


#===============================================================================
# import threading
# import time
# import logging
# 
# #===============================================================================
# # #------------------------------------------------------------------------
# # def worker():
# #     """thread worker function"""
# #     print 'Worker'
# #     return
# # 
# # threads = []
# # for i in range(5):
# #     t = threading.Thread(target=worker)
# #     threads.append(t)
# #     t.start()
# # #-------------------------------------------------------------------------
# # def worker1(num):
# #     """thread worker function"""
# #     print 'Worker: %s' % num
# #     return
# # 
# # threads = []
# # for i in range(5):
# #     t = threading.Thread(target=worker1, args=(i,))
# #     threads.append(t)
# #     t.start()
# #===============================================================================
# #-------------------------------------------------------------------------
# def worker2():
#     print threading.currentThread().getName(), 'Starting'
#     time.sleep(2)
#     print threading.currentThread().getName(), 'Exiting'
# 
# def my_service():
#     logging.debug('Starting')
#     time.sleep(3)
#     logging.debug('Exiting')
#     
# # t1 = threading.Thread(name='my_service', target=my_service)
# # w1 = threading.Thread(name='worker', target=worker2)
# # w2 = threading.Thread(target=worker2)  # use default name
# # 
# # w1.start()
# # w2.start()
# # t1.start()   
# #-------------------------------------------------------------------------
# 
# class Common():
#     def __init__(self):
#         self.arr =[]
#         
#     def insert(self, val):
#         self.arr.append(val)
# 
# class IdThread(threading.Thread):
#     def __init__(self, group=None, target=None, name=None, args=(), kwargs=None, verbose=None):
#         threading.Thread.__init__(self, group=group, target=target, name=name, args=args, kwargs=kwargs, verbose=verbose)
#         self.args = args
#         self.kwargs = kwargs
#         self.lock = threading.Lock()
#         self.target = target
# 
#         
#     def run(self):
#         self.target.insert(threading.currentThread().getName())
# 
# 
# c = Common()
# t1 = IdThread(args=(1,), kwargs={'a':'A{}'.format(1), 'b':'B{}'.format(1)}, name="Afiz", target=c)
# t2 = IdThread(args=(2,), kwargs={'a':'A{}'.format(2), 'b':'B{}'.format(2)}, name="Alif", target=c)
# t1.start()
# t2.start()
# 
# t1.join()
# t2.join()
# 
# print c.arr
#===============================================================================
