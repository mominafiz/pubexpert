'''
Created on Jul 18, 2016

@author: Afiz
'''
from pubexpert.utility import readCSVToDF
from optparse import OptionParser
import sys
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.decomposition.online_lda import LatentDirichletAllocation
from sklearn.cross_validation import train_test_split

# parse commandline arguments
op = OptionParser()
op.add_option("--topics", action="store", type="int", dest="n_topics",
              help="Get k topics for each class using LDA topic modelling.")
op.add_option("--filename", action="store", type='str', dest="filename",
              help="Takes .csv file as an input containing header in order 'Nserc SubCategory Id' and 'Text'")
op.add_option("--topkwords", action="store", type="int", dest="print_top",
              help="Print k most discriminative terms per class"
                   " for every classifier.")

(opts, args) = op.parse_args()
if len(args) > 0:
    op.error("this script takes no arguments.")
    sys.exit(1)

if __doc__:
    print(__doc__)
op.print_help()
print

def print_top_words(model, feature_names, n_top_words):
    for topic_idx, topic in enumerate(model.components_):
        print("Topic #%d:" % topic_idx)
        print(" ".join([feature_names[i]
                        for i in topic.argsort()[:-n_top_words - 1:-1]]))
    print()
    

if __name__ == '__main__':
    df = readCSVToDF(opts.filename, header_names=['NCatId', 'Text'])
    data = [s[0] for s in df.as_matrix(['Text']).tolist()]
    target = [s[0] for s in df.as_matrix(['NCatId']).tolist()]
    data_train, data_test, target_train, target_test = train_test_split(data, target, test_size=0.3, random_state=33)
            
    cat_at_test = sorted(set([s[0] for s in df.as_matrix(['NCatId'])]))
    
    for cat in cat_at_test:
        columns = df.columns.tolist()
        #data_cat = [s[0] for s in df[df.NCatId == cat].as_matrix(['Text']).tolist()]
        #target = [s[0] for s in df[df.NCatId == cat].as_matrix(['NCatId']).tolist()]
        data_cat = [data_train[i] for i,val in enumerate(target_train) if val==cat]
        tf_vectorizer = CountVectorizer(max_df=0.97, min_df=5)
        tf = tf_vectorizer.fit_transform(data_cat)
        
        print("Fitting LDA models with tf features...")
          
        lda = LatentDirichletAllocation(n_topics=opts.n_topics, max_iter=5,
                                    learning_method='online', learning_offset=50.,
                                    random_state=0)
        
        lda.fit(tf)
        
        print("\nTopics in LDA model for sub-category: {}".format(cat))
        tf_feature_names = tf_vectorizer.get_feature_names()
        print_top_words(lda, tf_feature_names, opts.print_top)
        break