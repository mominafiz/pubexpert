from pubexpert.utility.decorator import verbose
from pubexpert.novanet.db import MongoDb
from pubexpert.utility import config, writeToFile, getAndChangeToRootDirectory
from pubexpert.scripts.hierarchical_results import get_subdoc
from collections import Counter
import os


def getDocumentFromDbAndWriteInCSV(input_doc, multilabels):

    try:
        conn = MongoDb(config.host, config.port)
        conn.setDatabase(config.dbname)
        
        collection_name = None
        label = input_doc.get('label')[:-2]
        
        if label == "LSA":
            collection_name = 'articles.gcm'
        elif label == "LSB":
            collection_name = 'articles.bsf'
        elif label == "CS":
            collection_name = "articles.cs"
        else:
            Exception("Incorrect collection")

            
        conn.setCollection(collection_name)
        docCursor = conn.getDocument({'_id':input_doc.get("_id")}, {'description':1, 'nserc_cat_id':1})
        
        for doc in docCursor:
            text = doc.get("description")
        
            root = getAndChangeToRootDirectory()
            writefilepath = os.path.join(root, 'resources', 'data')
            # Id, Original Label, Multi-labels, Text
            csv_str_template = "{},{},{},{}\n".format(doc.get('_id'), doc.get('nserc_cat_id'), ' '.join(multilabels), text)
        
            writeToFile(writefilepath, 'multi-labels.csv', csv_str_template, 'a')
    
    finally:
        if conn:
            conn.closeConnection()
    
    
@verbose("Multi-Labeling a document based on threshold")
def multilabel_based_on_threshold(collection_name, task_list=[], top_n=3):
    
    """
        This approach again runs into class imbalance problem for different parent nodes.
        
        To avoid this problem, use threshold to follow the path and select top class, then
        combine the result based on votes
    
    """
    conn = MongoDb(config.host, config.port)
    conn.setDatabase(config.dbname)
    conn.setCollection(collection_name)
    conn.updateDocument({}, {'$unset': {'multi-labels':''}}, multi=True)
    
    
    true_samples = 0
    total_docs_multilabel_2 = 0
    total_docs_multilabel_3 = 0    
    
    docsCursor = conn.getDocument({}, {'bow':1, 'boc':1, 'bok':1, 'label':1})
    total_docs = docsCursor.count()
    
    for doc in docsCursor:
        multilabels_from_all_tasks = []
        for task in task_list:
            parent_dict = get_subdoc(doc.get(task))
            
            # list of classes that passes the threshold of 1/k, where k is number of classes.
            eligible_class_list = []
            
            # get number of classes
            threshold = 1 / float(len(parent_dict.keys()))
            
            max_parent_k = None
            # Sort keys by values
            sorted_parent = sorted(parent_dict, key=parent_dict.get, reverse=True)
            for key in sorted_parent:
                if parent_dict[key] >= threshold:
                    if key.startswith('CS'):
                        eligible_class_list.append('CS')
                        if max_parent_k is None:
                            max_parent_k = 'CS'
                    else:
                        eligible_class_list.append(key)
                        if max_parent_k is None:
                            max_parent_k = key
                    
            multilabels = {}
            for sd in doc.get(task):
                if sd.get("level") > 0:
                    prob_dict = sd.get('class_probabilities')
                    prob_keys = prob_dict.keys()
                    inner_threshold = 1 / float(len(prob_keys)) 
                    
                    
                    # track number of classes chosen for max parent key
                    max_pk = 0
                    if prob_keys[0][:-2] in eligible_class_list:
                        # sort prob_dict and get class with max probability
                        for key in sorted(prob_dict, key=prob_dict.get, reverse=True):
                            if prob_dict.get(key) >= inner_threshold:
                                multilabels[key] = prob_dict.get(key)
                                break
                                
                            
            multilabels_from_all_tasks.append(multilabels)


        top_n_multilabels = []
        if False:
        
            # Combine classes from each hierarchical classifier output,        
            # merge class in conflict by replacing it with max probability
            master_multilabels_dict = {}
            for multilabels in multilabels_from_all_tasks:
                for k, v in multilabels.iteritems():
                    if k in master_multilabels_dict:
                        if master_multilabels_dict.get(k) < multilabels.get(k):
                            master_multilabels_dict[k] = v
                    else:
                        master_multilabels_dict[k] = v
            
            # sort the dictionary and take top n. 
            
            for key in  sorted(master_multilabels_dict, key=master_multilabels_dict.get, reverse=True):
                if len(top_n_multilabels) < top_n:
                    top_n_multilabels.append(key)
            
        else:
            master_labels = []
            for mdict in multilabels_from_all_tasks:
                master_labels += mdict.keys()
                        
            counter = Counter()
            counter.update(master_labels)
            top_n_multilabels = [tup[0] for tup in counter.most_common(top_n)]
            
        
        if len(top_n_multilabels) == 2:
            total_docs_multilabel_2 += 1

        if len(top_n_multilabels) == 3:
            total_docs_multilabel_3 += 1

            
        # Check for label exists in final multilabel list
        if doc.get('label') in top_n_multilabels:
            true_samples += 1
#         else:
#             print doc.get('label') + ':    ' + str(top_n_multilabels)
    
        # update with final list of labels for each document
#         conn.updateDocument({'_id':doc.get('_id')}, {'$set':{'multi_labels_threshold_based': top_n_multilabels}})  
    
    
        
    conn.closeConnection()
    
    # print probability of having multi-label in top n probabilities
    
    print "Probability in top {}:    {}".format(top_n, true_samples / float(total_docs))
    print "Total multi-labeled=1 documents:    {}".format(total_docs - total_docs_multilabel_2 - total_docs_multilabel_3)
    print "Total multi-labeled=2 documents:    {}".format(total_docs_multilabel_2)
    print "Total multi-labeled=3 documents:    {}".format(total_docs_multilabel_3)
    
def multilabel_from_consensus_methods(collection_name):
    conn = MongoDb(config.host, config.port)
    conn.setDatabase(config.dbname)
    conn.setCollection(collection_name)
    
    
    true_samples = 0
    total_docs_multilabel_2 = 0
    total_docs_multilabel_3 = 0
    total_docs_multilabel_more_than_3 = 0
    
    docsCursor = conn.getDocument({}, {'multi_labels_result':1, 'label':1})
    total_docs = docsCursor.count()
    
    
    for doc in docsCursor:
        multilabels = doc.get('multi_labels_result')
        if doc.get('label') in doc.get('multi_labels_result'):
            true_samples += 1

        if len(multilabels) == 2:
            total_docs_multilabel_2 += 1

        if len(multilabels) == 3:
            total_docs_multilabel_3 += 1
            
        if len(multilabels) > 3:
            total_docs_multilabel_more_than_3 += 1
            
    conn.closeConnection()
            
    print "Probability in max top 3:    {}".format( true_samples / float(total_docs))    
    
    print "Total multi-labeled=1 documents:    {}".format(total_docs - total_docs_multilabel_2 - total_docs_multilabel_3)
    print "Total multi-labeled=2 documents:    {}".format(total_docs_multilabel_2)
    print "Total multi-labeled=3 documents:    {}".format(total_docs_multilabel_3)
    print "Total multi-labeled>3 documents:    {}".format(total_docs_multilabel_more_than_3)

    
    
if __name__ == '__main__':
#     collection_name = 'articles.all.binary.exclude_classes.r33'
    collection_name = 'articles.hc.100per.all_levels.r33'
    task_list = ['bow']#, 'boc', 'bok']
    #collection_name = 'articles.thesis.r33'
    # change top_n to 3
    multilabel_based_on_threshold(collection_name, task_list, top_n=3)
#     multilabel_from_consensus_methods(collection_name)
