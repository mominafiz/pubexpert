'''
Created on Sep 30, 2016

@author: Afiz
'''
from pubexpert.novanet.db import MongoDb
from pubexpert.utility import config
from nltk.classify.megam import numpy
from sklearn import metrics
from collections import Counter
from pubexpert.utility.decorator import verbose

verbosity = True

def min_max_normalization(data=[]):
    pass

def z_score_normalization(data=[]):
    pass




def get_subdoc(doc):
    key = 'level'
    if isinstance(doc, dict):
        if doc.has_key(key) and doc.get(key) == 0:
            return doc.get('class_probabilities')
    elif isinstance(doc, list):
        for d in doc:
            return get_subdoc(d)
    else:
        return {}    

@verbose("Cleaning up old results...", verbosity)   
def clear_result_attributes(collection_name, task_list=[]):
    conn = MongoDb(config.host, config.port)
    conn.setDatabase(config.dbname)
    conn.setCollection(collection_name)
    
    update_key = None
    for task in task_list:
        if task == 'bow':        
            update_key = 'bow_result'
        elif task == 'boc':
            update_key = 'boc_result'
        elif task == 'bok':
            update_key = 'bok_result'
        
        if update_key:  
            conn.updateDocument({}, {'$unset': {update_key:''}}, multi=True)
    
    conn.closeConnection()
    
@verbose("Cleaning up multilabels generated from output...", verbosity)   
def clear_multilabels(collection_name):
    conn = MongoDb(config.host, config.port)
    conn.setDatabase(config.dbname)
    conn.setCollection(collection_name)
    
    conn.updateDocument({}, {'$unset': {'multi_labels_result':''}}, multi=True)
    
    conn.closeConnection()
    
@verbose("Classification report based on voting from different classifiers...", verbosity)
def voting_based_classification_report(collection_name, task_list=[], top=1):
    conn = MongoDb(config.host, config.port)
    conn.setDatabase(config.dbname)
    conn.setCollection(collection_name)
    

    y_true, y_pred = [], []
    
    print '-' * 80
    print ' '.join(task_list)
    print '-' * 80
    
    def merge(dicts, max_keys, top):
        dicts = sorted(dicts.iteritems(),key=lambda (x,y):y, reverse=True)
        i = 0
        for t in dicts:
            max_keys.append(t[0])
            i+=1
            if i>= top:
                break
        
    docsCursor = conn.getDocument({}, {'bow_result':1, 'boc_result':1, 'bok_result':1, 'label': 1})
    for doc in docsCursor:
        y_true.append(doc.get('label'))
        
        count_keys = Counter()
        max_keys = []
        
        if 'bow' in task_list:
            merge(doc.get('bow_result'), max_keys, top)
        
        if 'boc' in task_list:
            merge(doc.get('boc_result'), max_keys, top)
    
        if 'bok' in task_list:
            merge(doc.get('bok_result'), max_keys, top)
        
        count_keys.update(max_keys)
        
        pred_class = [tup[0] for tup in count_keys.most_common(top)]
        y_pred.append(pred_class)
        
        # multi-labeling a document
        multilabels = doc.get('multi_labels_result')
        if multilabels is None:
            multilabels = pred_class
        else:
            if pred_class[0] not in multilabels:
                multilabels.extend(pred_class)
        conn.updateDocument({'_id':doc.get('_id')}, {'$set':{'multi_labels_result':multilabels}})
        
    counts = 0
    for i, true_pred in enumerate(zip(y_true, y_pred)):
        if true_pred[0] in [tup[0] for tup in true_pred[1]]:
            counts+=1
    print "Accuracy: {}".format(counts/float(len(y_true)))
    
    #===========================================================================
    # for i, true_pred in enumerate(zip(y_true, y_pred)):
    #       
    #     if true_pred[0] in [tup[0] for tup in true_pred[1]]:
    #         y_pred[i] = true_pred[0]
    #     else:
    #         y_pred[i] = true_pred[1][0][0]
    #               
    # print metrics.classification_report(y_true, y_pred)
    #   
    # print_classification_summary(y_true, y_pred, average=['macro', 'micro'])
    #===========================================================================
    conn.closeConnection()
    
@verbose("Finding maximum probability key by adding results...", verbosity)
def add_results_by_key_and_return_max_probability_class(collection_name, task_list=[]):
    conn = MongoDb(config.host, config.port)
    conn.setDatabase(config.dbname)
    conn.setCollection(collection_name)
    

    y_true, y_pred = [], []
    
    print '-' * 80
    print ' '.join(task_list)
    print '-' * 80
    
    docsCursor = conn.getDocument({}, {'bow_result':1, 'boc_result':1, 'bok_result':1, 'label': 1})
    for doc in docsCursor:
        y_true.append(doc.get('label'))
        
        # count_keys = Counter()
        temp_dict = {}
        max_keys = []
        
        if 'bow' in task_list:
            k = doc.get('bow_result').keys()[0]
            if k in temp_dict:
                temp_dict[k] += doc.get('bow_result').values()[0]
            else:
                temp_dict[k] = doc.get('bow_result').values()[0]
        
        if 'boc' in task_list:
            k = doc.get('boc_result').keys()[0]
            if k in temp_dict:
                temp_dict[k] += doc.get('boc_result').values()[0]
            else:
                temp_dict[k] = doc.get('boc_result').values()[0]
    
        if 'bok' in task_list:
            k = doc.get('bok_result').keys()[0]
            if k in temp_dict:
                temp_dict[k] += doc.get('bok_result').values()[0]
            else:
                temp_dict[k] = doc.get('bok_result').values()[0]
        
        
        maxk = None
        maxv = 0.0
        
        for k, v in temp_dict.iteritems():
            if v > maxv:
                maxk, maxv = k, v
        
        # below lines needs to be removed. 
        if doc.get('label') in temp_dict.keys():
            y_pred.append(doc.get('label'))
        else:
            y_pred.append(maxk)
                
        
        
        # y_pred.append(maxk)
        
    print metrics.classification_report(y_true, y_pred)
    
    print_classification_summary(y_true, y_pred, average=['macro', 'micro'])
    conn.closeConnection()

@verbose("Classification report based on maximum probability...", verbosity)
def max_probability_classification_report(collection_name, task_list=[], top=1):
    """
        This function takes maximum probability of the three classifier results,
        and selects the class as the predicted value. Classification report is 
        then generated using these values.
        
    """
    conn = MongoDb(config.host, config.port)
    conn.setDatabase(config.dbname)
    conn.setCollection(collection_name)
     
 
    y_true, y_pred = [], []
     
    print '-' * 80
    print ' '.join(task_list)
    print '-' * 80
     
    #===========================================================================
    #  
    # def merge(vdict, merge_in_dict):
    #     for k,v in vdict.iteritems():
    #         if k in merge_in_dict:
    #             merge_in_dict[k] += v
    #         else:
    #             merge_in_dict[k] = v
    #===========================================================================
                    
    docsCursor = conn.getDocument({}, {'bow_result':1, 'boc_result':1, 'bok_result':1, 'label': 1,'multi_labels_result':1})
    for doc in docsCursor:
        y_true.append(doc.get('label'))
         
        max_value = 0.0
        max_key = ""
         
        probs = {}
        
        if 'bow' in task_list:
            
            mv = doc.get('bow_result').values()[0]
            if mv > max_value:
                max_value = mv
                max_key = doc.get('bow_result').keys()[0]
         
        if 'boc' in task_list:                      
            mv = doc.get('boc_result').values()[0]
            if mv > max_value:
                max_value = mv
                max_key = doc.get('boc_result').keys()[0]
     
        if 'bok' in task_list:
            mv = doc.get('bok_result').values()[0]
            if mv > max_value:
                max_value = mv
                max_key = doc.get('bok_result').keys()[0]
         
        #=======================================================================
        # probs = sorted(probs.iteritems(), key=lambda (x, y): y)
        # top_probs = {}
        # 
        # i = 0
        # for k,v in probs:
        #     top_probs[k] = v
        #     i += 1
        #     if i >= top:
        #         break
        #     
        # y_pred.append(top_probs)
        #=======================================================================
        
        y_pred.append(max_key)
        
        # multi-labeling a document
        multilabels = doc.get('multi_labels_result')
        if multilabels is None:
            multilabels = [max_key]
        else:
            if max_key not in multilabels:
                multilabels.append(max_key)
                
        conn.updateDocument({'_id':doc.get('_id')}, {'$set':{'multi_labels_result':multilabels}})
    
    #===========================================================================
    # counts = 0
    # for i, true_pred in enumerate(zip(y_true, y_pred)):
    #     if true_pred[0] in true_pred[1].keys():
    #         counts+=1
    # print "Accuracy: {}".format(counts/float(len(y_true)))
    #===========================================================================
    #===========================================================================
    # for i, true_pred in enumerate(zip(y_true, y_pred)):
    #      
    #     if true_pred[0] in true_pred[1].keys():
    #         y_pred[i] = y_true[i]
    #     else:
    #         y_pred[i] = [k for k in true_pred[1].iterkeys()][0]
    #===========================================================================
          
    #===========================================================================
    # print metrics.classification_report(y_true, y_pred)
    #   
    # print_classification_summary(y_true, y_pred, average=['macro', 'micro', 'weighted'])
    #          
    #===========================================================================
    conn.closeConnection()    



@verbose("Classification report of individual classifier...", verbosity)   
def metrics_classification_report(collection_name, task=[]):
    """
            Generates separate classification report for each classifier. 
    
    """
    conn = MongoDb(config.host, config.port)
    conn.setDatabase(config.dbname)
    conn.setCollection(collection_name)
    
    
    for t in task:
        y_true, y_pred = [], []
        
        print '-' * 80
        print t.upper()
        print '-' * 80
        
        if t == 'bow':
            docsCursor = conn.getDocument({}, {'bow_result':1, 'label': 1})
            for doc in docsCursor:
                y_true.append(doc.get('label'))
                y_pred.append(doc.get('bow_result').keys()[0])
        
        elif t == 'boc':
            docsCursor = conn.getDocument({}, {'boc_result':1, 'label': 1})
            for doc in docsCursor:
                y_true.append(doc.get('label'))
                y_pred.append(doc.get('boc_result').keys()[0])
    
        elif t == 'bok':
            docsCursor = conn.getDocument({}, {'bok_result':1, 'label': 1})
            for doc in docsCursor:
                y_true.append(doc.get('label'))
                y_pred.append(doc.get('bok_result').keys()[0])
    
        elif t == 'bow_boc_bok_add':
            docsCursor = conn.getDocument({}, {'bow_boc_bok_add_result':1, 'label': 1})
            for doc in docsCursor:
                y_true.append(doc.get('label'))
                y_pred.append(doc.get('bow_boc_bok_add_result').keys()[0])
                
                   
        print metrics.classification_report(y_true, y_pred)
        
        
        print_classification_summary(y_true, y_pred, average=['macro', 'micro'])
        
        
    conn.closeConnection()
            

def print_classification_summary(y_true, y_pred, average=[]):
    
    for avg1 in average:
        if avg1 not in ['micro', 'macro', 'weighted']:
            raise ValueError("Incorrect average")
        print "--------{} average--------".format(avg1.capitalize())
        print "Accuracy:    {:.4f}".format(metrics.accuracy_score(y_true, y_pred))
        print "Precision:    {:.4f}".format(metrics.precision_score(y_true, y_pred, average=avg1))
        print "Recall:    {:.4f}".format(metrics.recall_score(y_true, y_pred, average=avg1))
        print "F1-Score:    {:.4f}".format(metrics.f1_score(y_true, y_pred, average=avg1))
    
    
@verbose("Class with maximum probability at each level", verbosity)
def path_with_max_probability_at_each_level(collection_name, task_list=[]):
    conn = MongoDb(config.host, config.port)
    conn.setDatabase(config.dbname)
    conn.setCollection(collection_name)
    
    for task in task_list:
        docsCursor = conn.getDocument({}, {task:1})
        
        
        for doc in docsCursor:
            parent_dict = get_subdoc(doc.get(task))
            
            pmax_key = None
            pmax_value = 0.0
            
            for k in parent_dict.keys():
                if parent_dict[k] > pmax_value:
                    pmax_key = k
                    pmax_value = parent_dict[k]
                    
            max_k = None
            max_v = 0.0
            for sd in doc.get(task):
                if sd.get("level") > 0:
                    prob_dict = sd.get('class_probabilities')
                    prob_keys = prob_dict.keys()
                    
                    if prob_keys[0][:-2] in pmax_key:
                        for k in prob_keys:
                            if prob_dict[k] > max_v:
                                max_k = k
                                max_v = prob_dict[k]
                
            if task == 'bow':        
                update_key = 'bow_result'
            elif task == 'boc':
                update_key = 'boc_result'
            elif task == 'bok':
                update_key = 'bok_result'
            elif task == 'bow_boc_bok_add':
                update_key = task + '_result'
                
            if update_key:  
                conn.updateDocument({'_id':doc.get('_id')}, {'$set':{update_key: {max_k:max_v}}})
    
    conn.closeConnection()   
    
@verbose("Class with maximum product at leaf from top-down...", verbosity)
def path_with_max_product_of_probability_at_leaf(collection_name, task_list=[], top=1):
    conn = MongoDb(config.host, config.port)
    conn.setDatabase(config.dbname)
    conn.setCollection(collection_name)
    
    for task in task_list:
        docsCursor = conn.getDocument({}, {task:1})
        
        
        for doc in docsCursor:
            parent_dict = get_subdoc(doc.get(task))
            
            max_key = None
            max_value = 0.0
            
            # probs = {}

            for sd in doc.get(task):
                if sd.get("level") > 0:
                    prob_dict = sd.get('class_probabilities')
                    
                    prob_keys = prob_dict.keys()
                    
                    match = filter((lambda x: prob_keys[0][:-2] in x), parent_dict.keys())
                    val = parent_dict[match[0]]
                        
                    for k in prob_keys:
                        prob_dict[k] = prob_dict[k] * val;
                    
                    # top key and value pair
                    for k in prob_keys:
                        if prob_dict[k] > max_value:
                            max_key = k
                            max_value = prob_dict[k]
                    
                    # add probs in top probs 
                    # probs.update(prob_dict)
                
            # select top n
            # probs = sorted(probs.iteritems(), key=lambda (x,y): y, reverse=True)
            
            #===================================================================
            # top_probs = {}
            # i = 0
            # for k,v in probs:
            #     top_probs[k] = v
            #     i += 1
            #     if i >= top:
            #         break
            #===================================================================
                
            
            if task == 'bow':        
                update_key = 'bow_result'
            elif task == 'boc':
                update_key = 'boc_result'
            elif task == 'bok':
                update_key = 'bok_result'
            elif task == 'bow_boc_bok_add':
                update_key = task + '_result'
                
            if update_key:  
                conn.updateDocument({'_id':doc.get('_id')}, {'$set':{update_key: {max_key:max_value}}})
    
    conn.closeConnection()   


   
def add_probabilities_across_boc_bow_bok(collection_name, task_list=[], average=False):
    """
        Calculates probability by summing results from boc, bow, and bok and
        stores results in database.
    
    """
    conn = MongoDb(config.host, config.port)
    conn.setDatabase(config.dbname)
    conn.setCollection(collection_name)
    
    docsCursor = conn.getDocument({}, {'bow':1, 'boc':1, 'bok':1, 'label': 1})
    for doc in docsCursor:
        parent = {}
        childs = {}
        for t in task_list:
            for task_doc in doc.get(t):
                if task_doc.get('level') == 0:
                    prob = task_doc.get('class_probabilities')
                    for k, v in prob.iteritems():
                        if parent.has_key(k):
                            parent[k] = parent[k] + v
                        else:
                            parent[k] = v
                            
                else:
                    prob = task_doc.get('class_probabilities')
                    for k, v in prob.iteritems():
                        
                        if childs.has_key(k[:-2]):
                            ckey = childs.get(k[:-2])
                        else:
                            ckey = {}
                            childs[k[:-2]] = ckey
                            
                        if ckey.has_key(k):
                            ckey[k] = ckey[k] + v
                        else:
                            ckey[k] = v
        
        restructure = [{'class_probabilities': parent, 'level':0}]
        for k in childs.keys():
            restructure.append({'class_probabilities': childs[k], 'level':1})   
            
        conn.updateDocument({'_id':doc.get("_id")}, {'$set': {'bow_boc_bok_add': restructure}})
                     
    conn.closeConnection()

 

if __name__ == '__main__':
    #collname = 'acm_abstracts.results'
#     collname = 'articles.all.binary.exclude_classes.r33'
    collname = 'articles.hc.90per.parent.r33'
    tlist = ['bow']#, 'boc', 'bok']
    
    clear_multilabels(collname)
    clear_result_attributes(collname, tlist)
    path_with_max_probability_at_each_level(collname, tlist)
    metrics_classification_report(collname, tlist)
#     voting_based_classification_report(collname, tlist)
#     max_probability_classification_report(collname, tlist)
    
    #===========================================================================
    # clear_result_attributes(collname, tlist)
    # path_with_max_product_of_probability_at_leaf(collname, tlist,top=1)
    # metrics_classification_report(collname, tlist)
    # voting_based_classification_report(collname, tlist,top=1)
    # max_probability_classification_report(collname, tlist, top=2)
    # add_results_by_key_and_return_max_probability_class(collname, tlist)
    #===========================================================================
