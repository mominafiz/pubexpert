# -*- coding: utf-8 -*-
from optparse import OptionParser
import sys
import time
from pubexpert.sunflower.categorize import multiprocessConceptCategories


# parse commandline arguments
op = OptionParser()
op.add_option("--multiprocess",
              action="store_true", dest="parallel_mp",
              help="Use multiprocess for processing job. Cannot use with multithread option.")
op.add_option("--dataset", type='str', dest="dataset", 
              help="GCM: Genes, Cells, Molecules and CS: Computer Science.")
op.add_option("--width", type='int', dest="width", default=2, 
              help="Width of graph tree of categories for a concept.")
op.add_option("--depth", type='str', dest="depth", default=2, 
              help="Depth of graph tree of categories for a concept.")
# op.add_option("--verbose", action="store_true", dest="verbose",
#               help="Prints detailed report.")

(opts, args) = op.parse_args()
if len(args) > 0:
    op.error("this script takes no arguments.")
    sys.exit(1)

if __doc__:
    print(__doc__)
op.print_help()
print

if __name__ == "__main__":
    t0 = time.time()

    if opts.parallel_mp:
        multiprocessConceptCategories(opts.dataset, opts.width, opts.depth)
    print("Done in {}s".format(time.time() - t0))
