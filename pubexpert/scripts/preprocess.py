import nltk
import string
from pubexpert.novanet.db import MongoDb
from pubexpert.utility import config
from nltk.corpus import stopwords
from nltk.stem.porter import PorterStemmer
import traceback
import sys
import re

class PreProcess(object):
    
    def __init__(self):
        pass
    
    def __removeUnicode(self, text):
        printable = set(string.printable)
        if isinstance(text, list):
            for v in text:
                if len(v) > 25:
                    text = filter(lambda x: x in printable, v)
                    break
        else:           
            text = filter(lambda x: x in printable, text)
        
        return text if not isinstance(text, list) else ''
        
    def tokenize(self, doc):
        text = self.__removeUnicode(doc.get('document_title'))
        text += " "
        
        text += self.__removeUnicode(doc.get('description'))
        text = text.lower()
        
        # Return a copy of the string S, where all characters have been mapped
        # through the given translation table, which must be a mapping of
        # Unicode ordinals to Unicode ordinals, Unicode strings or None.
        # Unmapped characters are left untouched. Characters mapped to None
        # are deleted.
        # table_mapping = dict((char,None)for char in string.punctuation)
        # text = text.translate(table_mapping)
        text = text.encode('utf-8').translate(None, string.punctuation)
        
        return nltk.word_tokenize(text)
        
    def removeStopWordsAndDigits(self, tokens):
        # stopwords_list = getFileContent(self.resource_path, "en-stopwords.txt")
        pattern = re.compile(r'\d+')
        return [w for w in tokens if not w in stopwords.words('english') if not pattern.search(w)]

    def stemTokens(self, tokens, stemmer=None):
        if not stemmer:
            stemmer = PorterStemmer()
            
        stemmed = []
        for item in tokens:
            stemmed.append(stemmer.stem(item))
        return stemmed
    
    
    def temp(self, text):
        return self.__removeUnicode(text)
    
if __name__ == "__main__":
    
    mongoConn = None
     
    def updateDocument(doc_id, doc):
        r = mongoConn.updateDocument({'_id':doc_id}, doc)[1]
        return True if(r == 1)else False
 
    def getConnection():
        connObj = MongoDb(config.host, config.port)
        connObj.setDatabase(config.dbname)
        
        collection_name = raw_input("Enter the collection name to preprocess. \
                        The program looks for document_title and description \
                        fields in database. After preprocess it creates new \
                        field bow_text and bow_text_v2 (stemmed).")
        connObj.setCollection(collection_name)
        return connObj
     


    mongoConn = getConnection()
    pObj = PreProcess()
    
    cursr = mongoConn.getDocument({}, {'document_title':1, 'description':1})
    cursr = cursr.add_option(4)
        
    for doc in cursr:
        try:   
            tokens = pObj.tokenize(doc)
            tokens = pObj.removeStopWordsAndDigits(tokens)
            updateDocument(doc['_id'], {'$set': {'bow_text': ' '.join(tokens)}})
            tokens = pObj.stemTokens(tokens)
            updateDocument(doc['_id'], {'$set': {'bow_text_v2': ' '.join(tokens)}})
            
        except(TypeError, ValueError, AttributeError) as e:
            print sys.exc_info()[:2], str(e), traceback.print_tb(sys.exc_info()[2])
     
     
