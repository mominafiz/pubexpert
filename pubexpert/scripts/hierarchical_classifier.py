# encoding: utf-8
'''
hierarchical_classifier -- This script runs through given files and
creates dataset for hierarchical classification. There are various
local and global classifier for hierarchical classification, but
this implementation is limited to Local Classifier Per Parent Node
(LCPN).

It defines classes_and_methods
applyChiSquare():
calculateScore():
printTopWords():
runClassification():
vectorize():


@author:     Afiz Momin
@copyright:  2016 Dalhousie University. All rights reserved.
@license:    BSD 3-Clause
@contact:    afiz.momin@dal.ca
@deffield    updated: 30th August, 2016
'''
from optparse import OptionParser
import sys
from sklearn.svm.classes import LinearSVC
from pubexpert.utility import readCSVToDF, writeToFile, config, convertToASCII

from pubexpert.ml.classifier_helper import getFeatures, createTrainTFIDF, createTestTFIDFTransformation, \
    __getVectorizer, classify, getClassifierName, applyChiSquare, getCVIndexes
from sklearn.cross_validation import StratifiedShuffleSplit
import random
import warnings
from pubexpert.novanet.db import MongoDb
import bson
import re
                             
                             
# parse commandline arguments
op = OptionParser()

op.add_option("--filenames", action="store", type='str', dest="filenames",
              help="Takes .csv files separated by comma as an input containing \
              header in order 'NCatId' and 'Text'")
op.add_option("--labels", action="store", type='str', dest="class_labels",
              help="Takes labels/class in order of dataset files separated by comma.")
op.add_option("--percent_sample", action="store", type="int", dest="percent_sample",
              help="integer value for choosing percent sample from each category as parent node.")
op.add_option("--sampling", action="store", type='str', dest='sampling',
              help="Takes n-random samples from each class. Using 'random' or \
              'stratified' for sampling.")
op.add_option("--chi2_select",
              action="store", type="int", dest="select_chi2",
              help="Select some number of features using a chi-squared test. Not used when using chi2_algorithm='custom' ")
op.add_option("--exclude_classes", action="store", type="str", dest="remove_classes",
              help="Name the classes by comma separated values. This option removes listed classes from \
              the train and test")
op.add_option("--model_type", action='store', type='str', dest='model_type',
              help='Type of classification task takes one of the values, bow,boc,bok.')
op.add_option('--store_classifiers', action='store_true', dest='store_classifiers',
              help='Stores the classifiers. Generates warning and user prompt if already exists for an \
              overwrite.')
op.add_option('--use_available_classifiers', action='store_true', dest='available_classifiers',
              help='Uses previously generated classifiers. If does not exists, new classifier models are \
              generated.')
op.add_option("--random_state", action='store', type='int', dest='random_state',
              help='Use random value of type int to reproduce the results.')
op.add_option("--cv", action="store", type='int', dest="cv", default=0,
              help="cross validation on k folds. Leave one out.")
op.add_option("--verbose", action="store", type='int', dest="verbose",
              help="Prints detailed report. 0: prints minimum information. \
              1: also prints timely progress. 2: also prints classification report. \
              3: also print confusion matrix. To write top words and classifier probabilities,\
               use verbose=3.")

(opts, args) = op.parse_args()
if len(args) > 0:
    op.error("this script takes no arguments.")
    sys.exit(1)
if __doc__:
    print(__doc__)
op.print_help()
print


clfs = (
#             (RidgeClassifier(tol=.01, solver="sag"), "Ridge Classifier"),
#             (Perceptron(n_iter=50), "Perceptron"),
#             (MultinomialNB(alpha=.01), "NB: Multinominal"),
#             (BernoulliNB(alpha=.01), "NB: BernoulliNB"),
             (LinearSVC(), "Linear SVC"),
#             (SVC(gamma=0.1),"Radial SVC"),
        )

def calculateScore(result, n_folds):
    # total results from classifier
    print
    print 'x' * 80
    n_classifier = len(clfs)
    for x in xrange(n_classifier):
        a, p, r, f1 = 0.0, 0.0, 0.0, 0.0
        for i in xrange(x * n_folds, (x * n_folds) + n_folds):
            a += result[i][0]
            p += result[i][1]
            r += result[i][2]
            f1 += result[i][3]
        a, p, r, f1 = a / n_folds, p / n_folds, r / n_folds, f1 / n_folds
        print "Classifier: {}".format(clfs[x][1])
        print('Accuracy:    %0.3f' % (a))
        print('Precision:    %0.3f' % (p))
        print('Recall:    %0.3f' % (r))
        print('F1-Score:    %0.3f' % (f1))  
        print '_' * 15
        

def vectorize(data_train, data_test, vocabulary=None, binary=False):
    #-------------------- Create TF.IDF  -------------------------
    if opts.verbose > 0:
        print "Creating TF.IDF matrix for training and test dataset."
        
    vectorizer = __getVectorizer(max_df=1.00, min_df=1, vocabulary=vocabulary, binary=binary)
    X_train = createTrainTFIDF(data_train, vectorizer)
    X_test = createTestTFIDFTransformation(data_test, vectorizer)
    return X_train, X_test , vectorizer     


def runClassification(clf, X_train, X_test, target_train, target_test, cv=0):
    
    
    #===========================================================================
    # if opts.print_virtauth_test:
    #     data_test, target_test = createVirtualAuthors(data_test, target_test, opts.print_docs)
    #===========================================================================
        
    if opts.verbose > 0:
        print "Starting classification..."
      
    with warnings.catch_warnings():
        warnings.filterwarnings("ignore", category=DeprecationWarning)
        
        print('=' * 80)
        print(getClassifierName(clf))
        print '-' * 80
    
        return_tuple = classify(clf, X_train, X_test, target_train, target_test, opts.verbose, cv, average='macro')
        
        if len(return_tuple) == 1:
            test_idx = return_tuple[0]
            
        if len(return_tuple) == 5:
            accuracy, precision, recall, f1, test_idx = return_tuple
        
        # accuracy, precision, recall, f1, test_idx = classify(clf, X_train, X_train, target_train, target_train, opts.verbose, cv, average='macro')
        probabilities = []
        for rlist in test_idx:
            probabilities.append(rlist[3].tolist())
            
        if len(return_tuple) == 1:
            return [probabilities]
        return [accuracy, precision, recall, f1, probabilities ]

def getCVData(d, cv):
    """
        Get indexes from cv and creates array of data for corresponding indices.
        
        Params:
            d (dict):
            cv (StratifiedShuffleSplit): 
            
        Return:
            tuple of temp_train_data, temp_test_data, temp_train_target, temp_test_target,
            temp_train_ids, temp_test_ids
            
    """
    
    temp_train_data, temp_test_data, temp_train_target, temp_test_target = [], [], [], []
    temp_train_ids, temp_test_ids = [], []
    
    for train_index, test_index in cv:   
        for i in train_index:
            temp_train_data.append(d.get('data')[i])
            temp_train_target.append(d.get('target')[i])
            temp_train_ids.append(d.get('Ids')[i])
        
        for i in test_index:    
            temp_test_data.append(d.get('data')[i])
            temp_test_target.append(d.get('target')[i])
            temp_test_ids.append(d.get('Ids')[i]) 
    return temp_train_data, temp_test_data, temp_train_target, temp_test_target, temp_train_ids, temp_test_ids       


def storeHierarchicalResult(probabilities, class_order, level, task, test_Ids, test_targets, collname):
    #===========================================================================
    # store_result_template = {'_id': test_Ids[i], 'label':test_targets[i],  
    #                                     'bow': [{'class_probabilities':{}, 'level':None},...], \
    #                                     'boc': [{'class_probabilities':{}, 'level':None},..], \
    #                                     'bok': [{'class_probabilities':{}, 'level':None},..]
    #                        }
    #===========================================================================
    
    conn = MongoDb(config.host, config.port)
    conn.setDatabase(config.dbname)
    conn.setCollection(collname)
    
    # Fix to allow for loop to run. but changes have consequences in the loop
    # which has been taken care.
    skip_labels = False
    if len(test_targets) == 0:
        test_targets = test_Ids
        skip_labels = True
        
    for p, did, label in zip(probabilities, test_Ids, test_targets):
        patt = re.compile('[a-f0-9]{24}')
        matches = patt.findall(did)
        did = matches[0]
            
        did = bson.ObjectId(did)
        # get document
        doc = [d for d in conn.getDocument({'_id':did})]
        
        if len(doc) == 1 : 
            # if task exists and is of type list, update the task list.
            result = {}
            result['class_probabilities'] = dict(zip(class_order, p))
            result['level'] = level
            task_subdoc = None
            if isinstance(doc[0].get(task), list):
                task_subdoc = doc[0].get(task)
                task_subdoc.append(result)
            else:
                task_subdoc = [result] 
            conn.updateDocument({'_id':did}, {'$set':{task:task_subdoc}})
                
        else:
            d, result = {}, {}
            result['level'] = level
            result['class_probabilities'] = dict(zip(class_order, p))
            if not skip_labels:
                d['label'] = label
            d['_id'] = did
            d[task] = [result]
            conn.insertDocument(d)
    conn.closeConnection()

def train_test_split(list_of_df, test_ratio=0.3):
    test_data = []
    test_target = []
    test_Ids = []
    test_target_parent_label = []  # stores label of parent node for all test data.
    
    for d in list_of_df: 
        
        # Stratified shuffle split selects percent sample and creates test data.
        # remaining data (train data) is stored in data and target of corresponding key.
        # so 30% goes to test and remaining 70% to create hierarchy and training.
        train_test_stratified_shuffle_split = StratifiedShuffleSplit(d.get('target'), 1, \
                                              test_size=test_ratio, random_state=opts.random_state)
        
        # cannot be use to get Ids of data.
        # genCV = getCVIndexes(d.get('data'), d.get('target'), train_test_stratified_shuffle_split)
        
        # alternative
        
        temp_train_data, temp_test_data, temp_train_target, temp_test_target, temp_train_ids, temp_test_ids = \
            getCVData(d, train_test_stratified_shuffle_split)        
        
        d['data'], d['target'] = temp_train_data, temp_train_target
        d['Ids'] = temp_train_ids
        
        
        test_data.extend(temp_test_data)
        test_target.extend(temp_test_target)
        test_Ids.extend(temp_test_ids)
        
        # create additional array to store labels of parent node of size target or data.
        test_target_parent_label.extend(([d.get('label')] * len(temp_test_target)))
        
        del temp_train_data, temp_test_data, temp_train_target, temp_test_target, temp_train_ids, temp_test_ids
    
    return test_data, test_target, test_Ids, test_target_parent_label 
   
def split_parent_child_data(list_of_df):
    parent_data, parent_target, parent_ids = [], [], []
     
    for d in list_of_df:
        if opts.verbose > 0:
            print "Creating dataset for Hierarchical classification..."
             
        # data_test and target_test goes to parent node, and rest to child nodes.
        # split for train-test do not occur in any of these steps as test data is already separated.
        # It selects percent sample for parent node from test after split. 
        # It assigns new label to parent node.
         
        # StratifiedKFold
        skf = StratifiedShuffleSplit(d.get('target'), 1, test_size=opts.percent_sample / 100.0, random_state=opts.random_state)  # random.randint(0, 1000))
        # genCV = getCVIndexes(d.get('data'), d.get('target'), skf)
         
        temp_train_data, temp_test_data, temp_train_target, temp_test_target, temp_train_ids, temp_test_ids = \
            getCVData(d, skf)        
         
        d['data'], d['target'] = temp_train_data, temp_train_target
        d['Ids'] = temp_train_ids
        
         
        parent_data.extend(temp_test_data)
        parent_target.extend([d.get('label')] * len(temp_test_target))
        parent_ids.extend(temp_test_ids)
         
        del temp_train_data, temp_test_data, temp_train_target, temp_test_target, temp_train_ids, temp_test_ids
        
    return parent_data, parent_target, parent_ids       
           

def getTestDataFromDb(collection_name, model_type):
    conn = MongoDb(config.host, config.port)
    conn.setDatabase(config.dbname)
    conn.setCollection(collection_name)
    docCursor = conn.getDocument({})
    
    test_data, test_target, test_Ids, test_target_parent_label = [], [], [], []
    
    for doc in docCursor:
        if model_type == 'bow':
            test_data.append(doc.get('bow_v2'))
            
        if model_type == 'boc':
            test_data.append(doc.get('categories'))
        
        if model_type == 'bok':
            test_data.append(doc.get('concepts'))
           
        if doc.get('label') is not None:    
            if 'CS' in doc.get('label'):
                test_target_parent_label.append('CSc')
            else:
                test_target_parent_label.append(convertToASCII(doc.get('label')[:-2]))
            
            test_target.append(convertToASCII(doc.get('label')))
                      
        test_Ids.append(str(doc.get('_id')))
    
    conn.closeConnection()
    return test_data, test_target, test_Ids, test_target_parent_label

        
class HierarchicalNode:
    # d = {'data':None, 'target':None, 'label': None, 'filename':None, 'data_train':None, 
        #        'data_test':None, 'target_train':None, 'target_test':None} 
    def __init__(self, level, data, target, category, filename):
        self.level = level
        self.data = None
        self.target = None
        self.category = category 
        self.filename = None
        self.data_train = None
        self.data_test = None
        self.target_train = None
        self.target_test = None
        


if __name__ == '__main__':
    # TODO: Automate n times random generation of results. 
    # TODO: Automate bow, boc, bok tasks
    # TODO: Optimize structure of probabilities for performance and quick access.
    # TODO: Implement KFold cross validation
    # TODO: Generate class names for parent category from dataset. (no user input)
    
    #=======================================================================
    # Algorithm:
    #     1.    User arg to select percent samples from each subcategories
    #     2.    User args to exclude classes, separated by comma.
    #     3.    User arg for type of sampling: stratified
    #     4.    User arg to input for the classification task: BOW, BOC, BOK (if multiple separated by comma)
    #     5.    User arg to create and store new classifier. Rewrite prompt. Goto Step 14
    #     6.    User arg to use created classifier, if exists (check pickled files).
    #                 If no pickled file, ask user whether to create new and store it. Goto Step 5, exit.
    #     7.    User args to input dataset file (one file).
    #                 CSV format file should have columns in order: _id, bow_v2, categories, concepts, labels
    #     8.    Load dataset
    #     # 9.    Create list for classification task to store classifiers in memory. (skip)
    #     10.   Exclude classes mentioned by user from the dataset.
    #     11.   Separate out 70-30 split data. 30% goes for test and save it as .csv file.
    #     12.   70% of train data for creating hierarchy is further split into x and 70-x%.
    #     13.   x% data represents main categories. Store it in a parent file.
    #     
    #     14.   If step 5 is true:
    #             foreach task:
    #                 1. Create parent node and create TF.IDF vector specific to task.
    #                 2. Apply feature selection, if applicable
    #                 3. Create classifier and apply fit on train data
    #                 4. Test the data and store results.
    #                 5. Store dataset in separate file and classifier (pickled) as another file
    #                 6. identify child nodes (sub-cats) in 70-x% dataset 
    #                 7. Group each sub-cat by corresonding parent group.
    #                 8. For each group:
    #                     1. Create a node and create TF.IDF vector specific to task
    #                     2. Create classifier and apply fit on train data.
    #                     3. Test the data and store the results.
    #                     4. Store the dataset in separate file and classifier (pickled) as another file.
    #                 #9. Store nodes for this task in list of classification tasks as an array.
    #                 10.    Free all the resources
    #          else:
    #             For each task:
    #                 1.    Load classifier from pickled files.
    #                 2.    Test data for each classifier
    #                 3.    Store the results.
    #             
    #     15. Compute complete hierarchical classification results.
    #=======================================================================

        
    try:
        opts.filenames = [v.strip() for v in opts.filenames.split(',')]
        
        if opts.class_labels:
            opts.class_labels = [v.strip() for v in opts.class_labels.split(',')]
        else:
            raise ValueError("--labels are required for parent nodes of each category.")
        
        if opts.sampling not in ['random', 'stratified']:
            raise Exception("--sampling: takes either 'random' or 'stratified' keywords.")
        
        
        if not opts.random_state:
            opts.random_state = 33
            
        if opts.cv == 0:
            print "No param value for --cv found. Skipping cross validation."
            
    except Exception as e:
        print "Error! {}".format(e.message)
        sys.exit(1)
            
   
    collname = raw_input("Enter the name of the collection to store the results: ")
    collname = collname.strip().lower()
        
        
    # Create list of datasets for each category and assign label for each dataset.
    # Load dataset and create data and targets
    for model_type in opts.model_type.split(','):
        list_of_df = []
        for filename, label in zip(opts.filenames, opts.class_labels):
            if opts.verbose > 0:
                print "Reading file: {} with label: {} and converting to dataframe...".format(filename,label) 
            
            # d = {'data':None, 'target':None, 'label': None, 'filename':None, 'data_train':None, 
            #        'data_test':None, 'target_train':None, 'target_test':None} 
            d = {}
            df = readCSVToDF(filename, header_names=['Id', 'Text', 'Concepts', 'Categories', 'NCatId'])
            # df = df[df.NCatId.apply(lambda e: e not in ['CS02','CS10','CS13','CS16'])]
            df.dropna(0, how='any', inplace=True)
            if opts.remove_classes:
                rclass = [c.strip().upper() for c in opts.remove_classes.split(',')]
                df = df[df.NCatId.apply(lambda e: e not in rclass)]
            
            select_column = None
            if model_type == 'bow':
                select_column = 'Text'
            elif model_type == 'boc':
                select_column = 'Concepts'
            elif model_type == 'bok':
                select_column = 'Categories'
            else:
                raise Exception("--model_type incorrect. Take one of these values: bow,boc,bok")
            d['data'] = [s[0] for s in df.as_matrix([select_column]).tolist()]
            d['target'] = [s[0] for s in df.as_matrix(['NCatId']).tolist()]
            d['Ids'] = [s[0] for s in df.as_matrix(['Id']).tolist()]
            
            d['label'], d['filename'] = label, filename
            # d['data_train'], d['data_test'], d['target_train'], d['target_test'] = None, None, None, None
            list_of_df.append(d)
            
            del df
            
        if opts.verbose > 0:
            print "Done reading data..."
        
        # Split data into 70-30 train and test. Test data takes 30% from each class using
        # stratified shuffle split.
        if opts.cv > 1:
            pass
        
        else:
            # splits train-test data and updates list_of_df with train data. Test data
            # is returned by the function.
            
            # default ratio
            test_ratio = 0.3
            
            use_full_train = raw_input("Do you want to use full data for training?[y/n] ")
            if use_full_train.strip().lower() == 'y':
                test_ratio = 0.01
                 
            test_data, test_target, test_Ids, test_target_parent_label = train_test_split(list_of_df, test_ratio=test_ratio)
             
            userinput = raw_input("Do you want to enter your test set?[yes/no]")
            if userinput.strip() == 'yes':
                test_collection = raw_input("Enter the name of the test collection: ")
                test_collection = test_collection.strip()
                test_data, test_target, test_Ids, test_target_parent_label = getTestDataFromDb(test_collection, model_type)
            
            # data_train, data_test, target_train, target_test = None, None, None, None
            
            #===========================================================================
            # # load chi2 score matrix from file
            # if opts.chi2_algo == 'custom' and opts.chi2_array:
            #     no_of_features = raw_input("Enter number of features to be selected for each class (comma separated): ")
            #     no_of_features = [int(val) for val in no_of_features.split(',')]
            #     dirpath = os.path.join(getAndChangeToRootDirectory(), 'resources', 'chi2')
            #     if opts.chi2_array.split('.')[-1] == 'npy' and os.path.exists(os.path.join(dirpath, opts.chi2_array)):
            #         attrVals = numpy.load(os.path.join(dirpath, opts.chi2_array))
            #     else:
            #         raise Exception("Either path to the file does not exists or incorrect input parameters.")
            #===========================================================================
            
    
            results = []
            # splits parent-child data and updates list_of_df with child data. Parent data
            # is returned by the function.
            parent_data, parent_target, parent_ids = split_parent_child_data(list_of_df)
            
            #===================================================================
            # #=======================================================================
            # #        Implementation to use 100% data at all the levels.
            # #=======================================================================
            # parent_data, parent_target,parent_ids,og_labels = [],[],[],[] 
            # for d in list_of_df:       
            #     parent_data.extend(d.get('data'))
            #     parent_target.extend([d.get('label')] * len(d.get('data')))
            #     parent_ids.extend(d.get('Ids'))
            # #=======================================================================
            #===================================================================
            
            # ==============================================================================
            #===================================================================
            # # Following code is for leaf level-classification across all categories (flat classification)
            # # ==============================================================================
            # data = []
            # target = []
            # clf = LinearSVC()
            # for d in list_of_df:       
            #     data.extend(d.get('data'))
            #     target.extend(d.get('target'))
            #      
            # X_train, X_test, vectorizer = vectorize(data, test_data)
            # feature_names = getFeatures(vectorizer)
            # print "Total features: {}".format(len(feature_names))
            # result = runClassification(clf, X_train, X_test, target, test_target)
            # calculateScore([result[:-1]], 1)
            # #================================================================================
            #===================================================================
            #sys.exit(0)
            
            if opts.verbose > 0:
                print "Waiting for classification..."
             
            if opts.verbose > 0:
                print "Classification at level 0."
             
            if model_type in ['boc', 'bok']:
                print "Creating  binary tf.idf vectors for BOC and BOK..."
                X_train, X_test, vectorizer = vectorize(parent_data, test_data, binary=True)
            
            else:
                X_train, X_test, vectorizer = vectorize(parent_data, test_data)
                
            feature_names = getFeatures(vectorizer)
            print "Total features: {}".format(len(feature_names))
            # default implementation
            if opts.select_chi2 >= 1 and opts.model == 'bow':
                X_train, X_test, feature_names = applyChiSquare(X_train, X_test, parent_target, feature_names, opts.select_chi2)
                print "Total features using chi2: {}".format(len(feature_names))
         
            clf = clfs[0][0]
            result = runClassification(clf, X_train, X_test, parent_target, test_target_parent_label)
            storeHierarchicalResult(result[-1], clf.classes_, 0, model_type , test_Ids, test_target, collname)    
            calculateScore([result[:-1]], 1)
            
            
            for d in list_of_df:
                print "-"*80
                print d.get('label')
                print "-"*80
                clf = LinearSVC()
                if model_type in ['boc', 'bok']:
                    X_train, X_test, vectorizer = vectorize(d.get('data'), test_data, binary=True)
                else:
                    X_train, X_test, vectorizer = vectorize(d.get('data'), test_data)
                    
                feature_names = getFeatures(vectorizer)
                print "Total features: {}".format(len(feature_names))
                
                if opts.select_chi2 >= 1 and opts.model == 'bow':
                    X_train, X_test, feature_names = applyChiSquare(X_train, X_test, d.get('target'), feature_names, opts.select_chi2)
                    print "Total features using chi2: {}".format(len(feature_names))
         
         
                result = runClassification(clf, X_train, X_test, d.get('target'), test_target)
                storeHierarchicalResult(result[-1], clf.classes_, 1, model_type , test_Ids, test_target, collname)
                calculateScore([result[:-1]], 1)
            
    print 'x' * 80
