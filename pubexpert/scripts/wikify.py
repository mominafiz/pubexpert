# -*- coding: utf-8 -*-
from optparse import OptionParser
import sys
from pubexpert.wikify.annotate import wikiAnnotation, multithreadAnnotation,\
     multiProcessAnnotation, singleThreadAnnotation
import time


# parse commandline arguments
op = OptionParser()
op.add_option("--wikipedia-miner",
              action="store_true", dest="wiki_miner_toolkit",
              help="Annotate using wikipedia miner toolkit.")
op.add_option("--illinois-wikifier",
              action="store_true", dest="illinois_miner",
              help="Annotate using Illinois Wikifier.")
op.add_option("--multithread",
              action="store_true", dest="parallel_mt",
              help="Use multithreading for processing job.")
op.add_option("--multiprocess",
              action="store_true", dest="parallel_mp",
              help="Use multiprocess for processing job. Cannot use with multithread option.")
op.add_option("--dataset", type='str', dest="dataset", 
              help="GCM: Genes, Cells, Molecules and CS: Computer Science.")
# op.add_option("--verbose", action="store_true", dest="verbose",
#               help="Prints detailed report.")

(opts, args) = op.parse_args()
if len(args) > 0:
    op.error("this script takes no arguments.")
    sys.exit(1)

if __doc__:
    print(__doc__)
op.print_help()
print

if __name__ == "__main__":
    
#     text = "The Lord of the Rings is an epic high-fantasy novel written \
#     by English author J. R. R. Tolkien. The story began as a sequel to Tolkien's 1937 fantasy novel The Hobbit,\
#      but eventually developed into a much larger work. Written in stages between 1937 and 1949, The Lord of the \
#      Rings is one of the best-selling novels ever written, with over 150 million copies sold.[1] \
#     The title of the novel refers to the story's main antagonist, the Dark Lord Sauron,[note 1] who\
#      had in an earlier age created the One Ring to rule the other Rings of Power as the ultimate weapon \
#      in his campaign to conquer and rule all of Middle-earth. From quiet beginnings in the Shire, a hobbit \
#      land not unlike the English countryside, the story ranges across Middle-earth, following the course of \
#      the War of the Ring through the eyes of its characters, not only the hobbits Frodo Baggins, Samwise \
#      'Sam' Gamgee, Meriadoc 'Merry' Brandybuck and Peregrin 'Pippin' Took, but also the hobbits' chief \
#      allies and travelling companions: the Men Aragorn son of Arathorn, a Ranger of the North, and Boromir, \
#      a Captain of Gondor; Gimli son of Gl�in, a Dwarf warrior; Legolas Greenleaf, an Elven prince; \
#      and Gandalf, a Wizard."
#      
     
#     wikiAnnotation('articles.gcm.after_2000', 'LSA01')
    t0 = time.time()
    if opts.wiki_miner_toolkit:
        if opts.parallel_mt:
            multithreadAnnotation(opts.dataset,annotator=wikiAnnotation)
        elif opts.parallel_mp:
            multiProcessAnnotation(opts.dataset,target = wikiAnnotation)
        else:
            singleThreadAnnotation(opts.dataset, target=wikiAnnotation)
    if opts.illinois_miner:
        #multithreadAnnotation(opts.dataset,annotator=illinoisAnnotation)
        pass
    
    print("Done in {}s".format(time.time() - t0))
