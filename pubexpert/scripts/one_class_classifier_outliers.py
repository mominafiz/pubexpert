from sklearn.cross_validation import train_test_split
from pubexpert.utility import size_mb, readCSVToDF
from time import time
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.feature_selection.univariate_selection import SelectKBest, chi2
import random
from copy import deepcopy

print(__doc__)
from sklearn import svm


#  ------------------- Initializing params ---------------------
cat_at_test = ['LSA01', 'LSA02', 'LSA03', 'LSA04', 'LSA05', 'LSA06', 'LSA07', 'LSA08', 'LSA09']
filename = 'articles_gcm.csv'
n_random_outliers = 10  # in percentage
use_chi_sq = False  # will remain false if chi-sq code is in comment
k_ch2 = 2000  # number of features
flag_outlier = False

#------------------------------ read csv file    -----------------------
df = readCSVToDF(filename, header_names=['NCatId', 'Text'])

for cat in cat_at_test:
    print '=' * 80
    print "Category: " + cat
    print '-' * 80
    # --- Create df for data,target/labels, outliers  ---------------
    #get df for particular category id
    train_sub_df = df[df.NCatId == cat]
    
    #get all text for selected nserc sub-category id
    data = [s[0] for s in train_sub_df.as_matrix(['Text']).tolist()]
    
    #get all labels/targets for selected nserc category id
    target = [s[0] for s in train_sub_df.as_matrix(['NCatId']).tolist()]
    
    # get unique list of nserc subcategories
    nserc_cat_ids = cat_at_test[:]
    
    # remove sub-category at test
    nserc_cat_ids.remove(cat)
    
    #create list of dataframes for all sub-category
    outliers_df = [df[df.NCatId == cat_id] for cat_id in nserc_cat_ids]
    
    # ---------------foreach df of the subcat randomly select n percent samples --------------
    
    # for each outlier subcat create dictionary of empty samples
    outlier_samples = {}
    for catid in nserc_cat_ids:
        outlier_samples[catid] = []
    
    outliers_count = 0
    for dframe in outliers_df:
        t_samples = int((dframe.size / 2) * (n_random_outliers / float(100)))
        outliers_count += t_samples
        for i in range(t_samples):
            v = random.random()  # actual rows of 2*2 matrix 
            index = int(v * (dframe.size / 2))
            record = dframe.irow(index)
            
            outlier_samples[record[0]].append(record[1])
    
    #------------------ Create training and test dataset ---------------------
    #cv = ShuffleSplit(X_train.shape[0], n_iter=10, test_size=0.2, random_state=0)
    data_train, data_test, target_train, target_test = train_test_split(data, target, test_size=0.3, random_state=42)

    #  Calculate and print size of data
    data_train_size_mb = size_mb(data_train)
    data_test_size_mb = size_mb(data_test)
    data_outlier_size_mb = 0
    for outlier in outlier_samples.values():
        data_outlier_size_mb += size_mb(outlier)
     
    print
    print("%d documents - %0.3fMB (training set)" % (len(data_train), data_train_size_mb))
    print("%d documents - %0.3fMB (test set)" % (len(data_test), data_test_size_mb))
    print("%d documents - %0.3fMB (outlier set)" % (outliers_count, data_outlier_size_mb))
    print
    
    # ---------------    Create TF.IDF ----------------------
    print("Extracting features from the training data using a sparse vectorizer")
    
    t0 = time()
    vectorizer = TfidfVectorizer(sublinear_tf=True, max_df=1.0, lowercase=False)  # ,stop_words='english')
    X_train = vectorizer.fit_transform(data_train)
    duration = time() - t0
    print("done in %fs at %0.3fMB/s" % (duration, data_train_size_mb / duration))
    print("n_samples: %d, n_features: %d" % X_train.shape)
    print
    
    print("Extracting features from the test data using the same vectorizer")
    t0 = time()
    X_test = vectorizer.transform(data_test)
    duration = time() - t0
    print("done in %fs at %0.3fMB/s" % (duration, data_test_size_mb / duration))
    print("n_samples: %d, n_features: %d" % X_test.shape)
    print
    
    print("Extracting features from the outliers data using the same vectorizer.")
    t0 = time()
    
    # Copy outliers dictionary having key as label and values as samples
    X_outliers = deepcopy(outlier_samples)
    del outlier_samples
    # vectorize each subcat/label's values
    samp, feat = 0, 0
    for k, v in X_outliers.iteritems():
        X_outliers[k] = vectorizer.transform(v)
        samp += X_outliers[k].shape[0]
        feat = X_outliers[k].shape[1]
         
    duration = time() - t0
    print("done in %fs at %0.3fMB/s" % (duration, data_outlier_size_mb / duration))
    print("n_samples: %d, n_features: %d" % (samp, feat))
    print
    
    
    # -------------------Chi-square-------------------
    use_chi_sq = True
    
    print("Extracting %d best features by a chi-squared test" % k_ch2)
    t0 = time()
    ch2 = SelectKBest(chi2, k=k_ch2)
    X_train = ch2.fit_transform(X_train, target_train)
    X_test = ch2.transform(X_test)
    
    # vectorize each subcat/label's values
    for k, v in X_outliers.iteritems():
        X_outliers[k] = ch2.transform(v)
    
    feature_names = vectorizer.get_feature_names()
    if feature_names:
        # keep selected feature names
        feature_names = [feature_names[i] for i
                         in ch2.get_support(indices=True)]
    print("done in %fs" % (time() - t0))
    print
    
    #Train and test model---------------------
    
    clf = svm.OneClassSVM(nu=0.1, kernel="rbf", gamma=0.1)
    clf.fit(X_train)
    y_pred_train = clf.predict(X_train)
    y_pred_test = clf.predict(X_test)
    y_pred_outliers = {}
    
    for k, v in X_outliers.iteritems():
        y_pred_outliers[k] = clf.predict(v)
    
    n_error_train = y_pred_train[y_pred_train == -1].size
    n_error_test = y_pred_test[y_pred_test == -1].size
    
    n_error_outliers = {}
    for k, v in y_pred_outliers.iteritems():
        n_error_outliers[k] = v[v == -1].size
    
    n_X_train = X_train.shape[0]
    n_X_test = X_test.shape[0]
    
    # -------------------    Print results --------------------------
    
    print "Result:"
    print "{}: {}".format("Train", n_X_train)
    print "{}: {}".format("Error", n_error_train)
    print "{}: {}".format("Percent", (n_error_train / float(n_X_train)) * 100)
    print
    print "{}: {}".format("Test", n_X_test)
    print "{}: {}".format("Error", n_error_test)
    print "{}: {}".format("Percent", (n_error_test / float(n_X_test)) * 100)
    print
    
    p_outliers = 0
    for k, v in n_error_outliers.iteritems():
        n_X_outlier = X_outliers[k].shape[0]
        print "SubCat: {}".format(k)
        print "{}: {}".format("Outlier:", n_X_outlier)
        print "{}: {}".format("Positives:", v)
        print "{}: {}".format("Percent", (v / float(n_X_outlier)) * 100)
        print "*" * 5
        
        p_outliers += v
        
    print "Outliers incorrectly Identified"
    print "Total: {}".format(outliers_count)
    print "Error: {}".format(p_outliers)
    print "Percent: {:+f}".format((p_outliers / float(outliers_count)*100))
