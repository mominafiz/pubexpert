from pubexpert.novanet.db import MongoDb
from pubexpert import utility
from pubexpert.scripts.hierarchical_results import get_subdoc, \
    print_classification_summary
from pubexpert.utility.decorator import verbose
from collections import OrderedDict
import numpy
from sklearn.metrics import metrics
from sklearn.svm.classes import SVR
from sklearn.linear_model.base import LinearRegression
from sklearn.preprocessing import LabelEncoder
import pandas
from sklearn import preprocessing
from pubexpert.utility import config


def get_max_probability_at_each_level_path_dataset(collection_name, ordered_keys, tasklist,average=False):
    try:
        conn = MongoDb(utility.config.host, utility.config.port)
        conn.setDatabase(utility.config.dbname)
        conn.setCollection(collection_name)
        
        # store class probabilities and label in list
        list_of_probs = []
        
        return_document = {'label':1}
        if 'bow' in tasklist:
            return_document['bow'] = 1
        
        if 'boc' in tasklist:
            return_document['boc'] = 1
        
        if 'bok' in tasklist:
            return_document['bok'] = 1
        
        docs = conn.getDocument({}, return_document)  # .limit(10)       
        
        for doc in docs:
            # calculate product of the probability for all task in tasklist
            # Create ordered dictionary of these probabilities with target class
            # for each task.
            avg_prob = []
            for task in tasklist:
                unified_dict = OrderedDict()
                for k in ordered_keys:
                    unified_dict[k] = 0.0
                
                parent_dict = get_subdoc(doc.get(task))
                pmax_key = None
                pmax_value = 0.0
                 
                for k in parent_dict.keys():
                    if parent_dict[k] > pmax_value:
                        pmax_key = k
                        pmax_value = parent_dict[k]
                        

                for sd in doc.get(task):
                    if sd.get("level") > 0:
                        prob_dict = sd.get('class_probabilities')
                        prob_keys = prob_dict.keys()
                        
                        if prob_keys[0][:-2] in pmax_key:
                            for k in prob_keys:
                                unified_dict[k] = prob_dict[k]
             
            
                avg_prob.append(unified_dict.values())                            

            
            
            l = utility.convertToASCII(doc.get('label'))
            _id = utility.convertToASCII(doc.get('_id'))
            
            if average==True:
                probs_list = [(a+b+c)/3.0 for a,b,c in zip(avg_prob[0],avg_prob[1],avg_prob[2])]
                probs_list.append(l)
                probs_list.append(_id)
                list_of_probs.append(probs_list)
            else:
                probs_list = [pl.extend([l,_id]) for pl in avg_prob] 
                list_of_probs.extend(avg_prob)
            
        
        return numpy.array(list_of_probs)
                        
    except Exception as e:
        print e             
    finally:
        if conn is not None:
            conn.closeConnection()

@verbose("Getting product at leaf node dataset...")
def get_product_at_leaf_dataset(collection_name, ordered_keys, tasklist):
    
    try:
        conn = MongoDb(utility.config.host, utility.config.port)
        conn.setDatabase(utility.config.dbname)
        conn.setCollection(collection_name)
        
        # store class probabilities and label in list
        list_of_probs = []
        
        return_document = {'label':1}
        if 'bow' in tasklist:
            return_document['bow'] = 1
        
        if 'boc' in tasklist:
            return_document['boc'] = 1
        
        if 'bok' in tasklist:
            return_document['bok'] = 1
        
        docs = conn.getDocument({}, return_document)  # .limit(100)       
        
        
        for doc in docs:
            # calculate product of the probability for all task in tasklist
            # Create ordered dictionary of these probabilities with target class
            # for each task.
             
            for task in tasklist:
                unified_dict = OrderedDict()
                for k in ordered_keys:
                    unified_dict[k] = 0.0
                
                parent_dict = get_subdoc(doc.get(task))
                
#                 # temp code
#                 for k,v  in parent_dict.iteritems():
#                     if doc.get('label')[:-2]  in  k:
#                         val = parent_dict[k]
                
                for sd in doc.get(task):
                    if sd.get("level") > 0:
                        prob_dict = sd.get('class_probabilities')
                        
                        prob_keys = prob_dict.keys()
                        
                        match = filter((lambda x: prob_keys[0][:-2] in x), parent_dict.keys())
                        
                        val = parent_dict[match[0]]
                        
                        for k in prob_keys:
                            unified_dict[k] = prob_dict[k] * val;
                
                probs_list = unified_dict.values()
                
                #===============================================================
                # # temp imp
                # maxk = None
                # maxv = 0.0
                #  
                # for k,v in unified_dict.iteritems():
                #     if maxv < v:
                #         maxv, maxk = v,k
                #===============================================================
                        
                # probs_list.append(maxk)
                if not len(probs_list) == 31:
                    continue 
                probs_list.append(utility.convertToASCII(doc.get('label')))  
                probs_list.append(utility.convertToASCII(doc.get('_id'))) 
                # taking the predicted probability as the classes for regression.  
#                 probs_list.append(maxk)
                list_of_probs.append(probs_list)
        
        return numpy.array(list_of_probs)
                        
    except Exception as e:
        print e               
    finally:
        if conn is not None:
            conn.closeConnection()
            

def classification_on_outputs(numpy_dataset_train, numpy_dataset_test):
    # clf = LinearSVC(random_state=33)
    clf = SVR(kernel='linear')
    # separate data and target columns
    data_train = numpy_dataset_train[:, :-1]
    target_train = numpy_dataset_train[:, -1]
    
    data_test = numpy_dataset_test[:, :-1]
    target_test = numpy_dataset_test[:, -1]
    
    
    lenc = LabelEncoder()
    lenc.fit(numpy.unique(target_train))
    target_train = lenc.transform(target_train)
    
    lenc = LabelEncoder()
    lenc.fit(numpy.unique(target_test))
    target_test = lenc.transform(target_test)
    
#     enc = OneHotEncoder()
#     enc.fit(target)
    
    # train classifier 70-30 split
    # data_train, data_test, target_train, target_test = train_test_split(data, target, test_size=0.0, random_state=33)
    
    data_train = map(lambda x: [float(a) for a in x], data_train)
    data_test = map(lambda x: [float(a) for a in x], data_test)
    
    
    # predict target for entire dataset.
    clf.fit(data_train, target_train)
    y_pred = clf.predict(data_test)

    print metrics.classification_report(target_test, y_pred)
    print_classification_summary(target_test, y_pred, ['micro', 'macro', 'weighted'])
#     
#     y_pred = clf.predict(data_test)
# 
#     print_classification_summary(target_test, y_pred, ['micro', 'macro', 'weighted'])

def scatter_plot():
    pass

def regression(np_data_train, np_data_test, collection_name, no_of_classes=1, no_of_tasks=1):
    clf = LinearRegression()
    
    
    columns = numpy.unique(np_data_train[:, -2]).tolist()
   
    
    # separate out train data and target
    data_np_arr_train = np_data_train[:, :-2]
    target_np_arr_train = np_data_train[:, -2]
    
    if np_data_test is not None:
        # separate out test data and target
        data_np_arr_test = np_data_test[:, :-2]
        target_np_arr_test = np_data_test[:, -2]
        target_np_arr_test_ids = np_data_test[:, -1]
    
    # convert to df
    # df = pandas.DataFrame(data=np_data, columns=columns,dtype=numpy.float64)
    df_data_train = pandas.DataFrame(data=data_np_arr_train, dtype=numpy.float64)
    if np_data_test is not None: 
        df_data_test = pandas.DataFrame(data=data_np_arr_test, dtype=numpy.float64)  # ,columns=columns)
    # df_target= pandas.DataFrame(data=target_np_arr,columns=['Class'])
    
    # dumpy encoding on categorical predictor
    # df_target = pandas.get_dummies(data=df_target, columns=['Class'])
    lb = preprocessing.LabelBinarizer()
    lb.fit(columns)
    
    
    tt_train = lb.transform(target_np_arr_train)
    
    
    # separate out data and target
    # df_data = data.drop(data.columns[no_of_classes:], axis=1, inplace=False)
    # df_y = data.drop(data.columns[:no_of_classes], axis=1, inplace=False)
    
    clf.fit(df_data_train, tt_train)
    pred = clf.predict(df_data_train)
    if np_data_test is not None:
        pred = clf.predict(df_data_test)
        
    
    pred_classes = lb.inverse_transform(pred)
    
#     if np_data_test is not None: 
#         print metrics.classification_report(target_np_arr_test, pred_classes)
#         print_classification_summary(target_np_arr_test, pred_classes, average=['micro', 'macro', 'weighted'])
#     else:
#         print metrics.classification_report(target_np_arr_train, pred_classes)
#         print_classification_summary(target_np_arr_train, pred_classes, average=['micro', 'macro', 'weighted'])
    
    
    # multi_label the documents
    conn = MongoDb(config.host, config.port)
    conn.setDatabase(config.dbname)
    conn.setCollection('acm_abstracts.results')
     
    pred_classes = pred_classes.tolist()
    for idx in xrange(0, len(target_np_arr_test_ids), no_of_tasks):
        x = target_np_arr_test_ids[idx]
        for doc in conn.getDocument({'_id': x}):
            # multi-labeling a document
            multilabels = doc.get('multi_labels_result')
            if multilabels is None:
                multilabels = numpy.unique(pred_classes[idx:idx + no_of_tasks]).tolist()
            else:
                for pred_class in pred_classes[idx:idx+no_of_tasks]:
                    if pred_class not in multilabels:
                        multilabels.append(pred_class)
            
            conn.updateDocument({'_id':doc.get('_id')}, {'$set':{'multi_labels_result':multilabels}})
    
if __name__ == '__main__':
    ordered_keys = ['LSA01', 'LSA02', 'LSA04', 'LSA05', 'LSA06', 'LSA07', 'LSA08', 'LSA09',
                    'CS01', 'CS03', 'CS04', 'CS05', 'CS06', 'CS07', 'CS08', 'CS09',
                    'CS11', 'CS12', 'CS14', 'CS15', 'CS17', 'CS18', 'CS19', 'CS20', 'CS21',
                    'LSB01', 'LSB02', 'LSB03', 'LSB06', 'LSB07', 'LSB09'
                    ]
    collection_name = 'articles.all.binary.exclude_classes.r33'
    task_list = [ 'bow', 'boc', 'bok']
#     np_data_test = get_product_at_leaf_dataset(collection_name,ordered_keys, task_list)
    np_data_test = None
    np_data_train = get_max_probability_at_each_level_path_dataset(collection_name,\
                                          ordered_keys, task_list)
    np_data_test = get_max_probability_at_each_level_path_dataset('articles.hc.100per.all_levels.r33', ordered_keys, task_list,average=True)
    # 'acm_abstracts.results'
    
    
    # np_data_train = get_product_at_leaf_dataset('temp', 'bow', 'boc', 'bok')
    
#     if not len(np_data_train[0])== 32:
#         raise Exception("Incorrect vector length. Expected length is 32.")
#     if len(np_data_test.shape)==1:
#         np_data_test.resize((len(np_data_test),len(np_data_test[0])))
#     if len(np_data_train.shape)==1:
#         np_data_train.resize((len(np_data_train),len(np_data_train[0])))    
    
    if np_data_test is None:
        numpy.random.shuffle(np_data_train)
        np_data_test = np_data_train
        np_data_train = np_data_train[(np_data_train.shape[0]/2):,:]
    
#     np_data_test = np_data_train
    # regression is now done by averaging bow,boc, and bok probabilities
    regression(np_data_train, np_data_test, collection_name,no_of_tasks= len(task_list))
    
#     np_data = get_max_probability_at_each_level_path_dataset('articles.all.binary.exclude_classes.r33', 'bow', 'boc','bok')
#     regression(np_data)
