'''
Created on Jul 5, 2016

@author: Afiz
'''
from sklearn.linear_model.perceptron import Perceptron
from sklearn.linear_model.ridge import RidgeClassifier
from sklearn.naive_bayes import MultinomialNB, BernoulliNB
from pubexpert.ml.classifier_helper import createTrainTFIDF, \
    createTestTFIDFTransformation, getFeatures, applyChiSquare, \
    getTopKWordsFromClassifier, \
    __getVectorizer, getCVIndexes, getClassifierName, classify
from optparse import OptionParser
from sklearn.cross_validation import KFold, train_test_split, StratifiedKFold
from sklearn.svm.classes import LinearSVC
from pubexpert.utility import readCSVToDF, writeToFile, convertToASCII, \
    getAndChangeToRootDirectory
import os
import sys
import warnings
import numpy
from pubexpert.ml.X2 import getFeaturesFromChi2Matrix
from pubexpert.utility.plots import histogram
from sklearn.multiclass import OneVsOneClassifier
import random
from pandas.core.frame import DataFrame
import pandas
from pubexpert.ml.roc import roc_curve_plot



# parse commandline arguments
op = OptionParser()
op.add_option("--chi2_select",
              action="store", type="int", dest="select_chi2",
              help="Select some number of features using a chi-squared test. Not used when using chi2_algorithm='custom' ")
op.add_option("--topkwords", action="store", type="int", dest="print_top",
              help="Print k most discriminative terms per class"
                   " for every classifier.")
op.add_option("--virtual_authors", action="store_true", dest="print_virtauth_test",
              help="Test classifier using virtual authors")
op.add_option("--docs_per_author", action='store', type='int', dest='print_docs',
              help="Use with --virtual_authors to select k docs per virtual authors.")
op.add_option("--cv", action="store", type='int', dest="cv",
              help="cross validation on k folds. Leave one out.")
op.add_option("--filename", action="store", type='str', dest="filename",
              help="Takes .csv file as an input containing header in order 'Nserc SubCategory Id' and 'Text'")
op.add_option("--exclude_classes", action="store", type="str", dest="remove_classes",
              help="Name the classes by comma separated values. This option removes listed classes from \
              the train and test")
op.add_option("--average", action="store", type="str", dest="average", default="weighted",
                            help="Average can be one of these values 'weighted','micro','binary','macro'. \
                            Default value is 'weighted'")
op.add_option("--verbose", action="store", type='int', dest="verbose",
              help="Prints detailed report. 0: prints minimum information. \
              1: also prints timely progress. 2: also prints classification report. \
              3: also print confusion matrix. To write top words and classifier probabilities,\
               use verbose=3.")
op.add_option("--chi2_algoritm", action="store", type='string', dest="chi2_algo",
              help="'scipy' or 'custom'")
op.add_option("--chi2_array", action="store", type='str', dest="chi2_array",
              help="Use the correct .npy file that is created using appropriate input file.")

(opts, args) = op.parse_args()
if len(args) > 0:
    op.error("this script takes no arguments.")
    sys.exit(1)

if __doc__:
    print(__doc__)
op.print_help()
print


clfs = (
#             (RidgeClassifier(tol=.01, solver="sag"), "Ridge Classifier"),
#             (Perceptron(n_iter=50), "Perceptron"),
#             (MultinomialNB(alpha=.01), "NB: Multinominal"),
#             (BernoulliNB(alpha=.01), "NB: BernoulliNB"),
            (LinearSVC(), "Linear SVC"),
#             (SVC(gamma=0.1),"Radial SVC"),
#             (OneVsOneClassifier(LinearSVC()), "OvO LinearSVM"),
        )

def calculateScore(result, n_folds):
    # total results from classifier
    print
    print 'x' * 80
    n_classifier = len(clfs)
    for x in xrange(n_classifier):
        a, p, r, f1 = 0.0, 0.0, 0.0, 0.0
        for i in xrange(x * n_folds, (x * n_folds) + n_folds):
            a += result[i][0]
            p += result[i][1]
            r += result[i][2]
            f1 += result[i][3]
        a, p, r, f1 = a / n_folds, p / n_folds, r / n_folds, f1 / n_folds
        print "Classifier: {}".format(clfs[x][1])
        print('Accuracy:    %0.3f' % (a))
        print('Precision:    %0.3f' % (p))
        print('Recall:    %0.3f' % (r))
        print('F1-Score:    %0.3f' % (f1))  
        print '_' * 15
        
def printTopWords(clf, feature_names, target_labels, dirpath, filename, cv=0):
    """
        Called after doing classification
    """  
    top = getTopKWordsFromClassifier(clf, feature_names, target_labels, opts.print_top)
    # write top k words to file 
    if opts.cv > 0:
        line = '=' * 80
        line += os.linesep
        line += 'CV: ' + str(cv) if cv > 0 else ''
        line += os.linesep
        line += '-' * 80
        line += os.linesep
        writeToFile(dirpath, filename, line, option='a')
    
    # print top k words for each class 
    for k, v in top.iteritems():
        print "{}: {}".format(k, ' '.join(v))                  
        # write top k words to file
        if opts.verbose > 2: 
            writeToFile(dirpath, filename, "{}:{}\n".format(k, ' '.join(v)), option='a')

def vectorize(data_train, data_test, vocabulary=None):
    #-------------------- Create TF.IDF  -------------------------
    if opts.verbose > 0:
        print "Creating TF.IDF matrix for training and test dataset."
        
    vectorizer = __getVectorizer(max_df=1.00, min_df=1, vocabulary=vocabulary)
    X_train = createTrainTFIDF(data_train, vectorizer)
    X_test = createTestTFIDFTransformation(data_test, vectorizer)
    return X_train, X_test , vectorizer     

acc = 0
top_dict = {}
def runClassification(clf, X_train, X_test, target_train, target_test, cv=0):
    
    
    #===========================================================================
    # if opts.print_virtauth_test:
    #     data_test, target_test = createVirtualAuthors(data_test, target_test, opts.print_docs)
    #===========================================================================
        
    if opts.verbose > 0:
        print "Starting classification..."
      
    # Suppress deprecation warning for some functions in classify() call.
    with warnings.catch_warnings():
        warnings.filterwarnings("ignore", category=DeprecationWarning)
        
        print('=' * 80)
        print(name)
        print '-' * 80
    
        return_tuple = classify(clf, X_train, X_test, target_train, target_test, opts.verbose, cv,average=opts.average)
        
        if len(return_tuple) == 1:
            print "This script is not yet ready for this modification"
        
        if len(return_tuple) == 5:
            accuracy, precision, recall, f1, test_idx  = return_tuple
        #=======================================================================
        # pred = test_idx[1]
        # 
        # classes = clf.classes_.tolist()
        # len_classes = float(len(classes))
        # roc_curve_plot(int(len_classes), target_test, pred, multiclass=True)
        #=======================================================================
        """
        truecounts = 0
        
        global top_dict
        top = 3
        for i in xrange(top):
            top_dict[i+1] = []
         
        for prob, pred, idx, probs in test_idx:
            top2 = sorted(zip(classes, probs), key=lambda t:t[1], reverse=True)[:top]
            for i,val in enumerate(top2):
                if val[1] >= 1/len_classes:
                    top_dict[i+1].append(val[1])
                 
            top2 = [v[0] for v in top2 if v[0] >= 1/len_classes]
 
            if target_test[idx] in top2:
                truecounts += 1
 
        accur = truecounts / float(len(target_test))
        global acc
        acc+=accur
        print "Acc: " + str(accur)  
        """     
#===============================================================================
#         for prob, pred, idx, probs in test_idx:
#             if pred != target_test[idx] and max(probs)>=0.3000 and max(probs)< 0.4000:
# #                 print idx,
#                 print '{idx}\t{actual}\t{pred}\t{probs}'.format(idx=idx,actual=target_test[idx], pred=pred, probs='\t'.join([str(round(v, 4)) for v in probs]))
# #                 for t, tfidfscore in zip(data_test[idx].split(' '),X_test.getrow(idx).data.tolist()):
# #                     print '{}\t{}'.format(t,round(tfidfscore,4))
#===============================================================================
        
        return [accuracy, precision, recall, f1]

# Script executes from here   
                
if __name__ == '__main__':
    # weighted average: http://scikit-learn.org/stable/modules/generated/sklearn.metrics.f1_score.html#sklearn-metrics-f1-score
    #--------------------- read csv file----------------------------
    if opts.verbose > 0:
        print "Reading {} and converting to dataframe...".format(opts.filename)
        
    df = readCSVToDF(opts.filename, header_names=['Id', 'Text','Concepts','Categories','NCatId'])
#     df = readCSVToDF(opts.filename, header_names=['Id','Text', 'NCatId'])
    df.dropna(0,how='any',inplace=True)
    if opts.remove_classes:
        rclass = [c.strip().upper() for c in opts.remove_classes.split(',')]
        df = df[df.NCatId.apply(lambda e: e not in rclass)]
    columns = df.columns.tolist()
    

    # select n documents from each class
#===============================================================================
#     classes = df.NCatId.unique()
#     newdf = DataFrame()
#     for subcat in classes:
#         temp_df = df[df.NCatId == subcat]
# #         print temp_df.count(axis=0)
#         temp_df = temp_df.sample(n=400)
#         newdf = pandas.concat([temp_df, newdf])
#     df = newdf
#     del newdf
#===============================================================================
    
    data = [s[0] for s in df.as_matrix(['Categories']).tolist() if s[0]]
    target = [s[0] for s in df.as_matrix(['NCatId']).tolist()]
    

    
    if opts.verbose > 0:
        print "Done reading data..."
    
    data_train, data_test, target_train, target_test = None, None, None, None
    
    # load chi2 score matrix from file
    if opts.chi2_algo == 'custom' and opts.chi2_array:
        no_of_features = raw_input("Enter number of features to be selected for each class (comma separated): ")
        no_of_features = [int(val) for val in no_of_features.split(',')]
        dirpath = os.path.join(getAndChangeToRootDirectory(), 'resources', 'chi2')
        if opts.chi2_array.split('.')[-1] == 'npy' and os.path.exists(os.path.join(dirpath, opts.chi2_array)):
            attrVals = numpy.load(os.path.join(dirpath, opts.chi2_array))
        else:
            raise Exception("Either path to the file does not exists or incorrect input parameters.")
    results = []
    for clf, name in clfs :
        
        #=======================================================================
        # dirpath = os.path.join(getAndChangeToRootDirectory(), 'resources', 'output', 'multiclass')
        # filename = getClassifierName(clf) + '_chi2_top_words.txt'   
        # if not os.path.exists(dirpath):
        #     os.mkdir(dirpath)
        # writeToFile(dirpath, filename, '', 'w')
        #=======================================================================
        
        if opts.cv > 0:
            if opts.verbose > 0:
                print "Waiting for cross-validation..."
            kf = KFold(len(data), n_folds=opts.cv, shuffle=True, random_state=33)
#             kf = StratifiedKFold(target,n_folds=10,shuffle=False)
            genCV = getCVIndexes(data, target, kf)
   
            
            for x in xrange(1, opts.cv + 1):
                print "CV: {}".format(x)
                data_train, data_test, target_train, target_test = genCV.next()
                
                
                X_train, X_test, vectorizer = vectorize(data_train, data_test)
                feature_names = getFeatures(vectorizer)
                print "Total features: {}".format(len(feature_names))
                # default implementation
                if opts.select_chi2 >= 1and opts.chi2_algo.lower() == 'scipy':
                    X_train, X_test, feature_names = applyChiSquare(X_train, X_test, target_train, feature_names, opts.select_chi2)
                elif opts.chi2_array and opts.chi2_algo.lower() == 'custom':
                    key_terms = getFeaturesFromChi2Matrix(attrVals, feature_names, nfs=no_of_features)
                    X_train, X_test, vectorizer = vectorize(data_train, data_test, vocabulary=key_terms)
                    feature_names = getFeatures(vectorizer)
                    print "Total features using chi2: {}".format(len(feature_names))
                    
                result = runClassification(clf, X_train, X_test, target_train, target_test, cv=x)
                results.append(result)
        
                #===============================================================
                # if opts.print_top:
                #     target_labels = list(sorted(set(target_test)))
                #     printTopWords(clf, feature_names, target_labels, dirpath, filename, cv=x)      
                #===============================================================
        else:
            
            
            data_train, data_test, target_train, target_test = train_test_split(data, target, test_size=0.3, random_state=33)
            
            # select n samples for test from each class
            #===================================================================
            # n = 1000
            # temp_data_test=[]
            # temp_target_test=[]
            # classes = df.NCatId.unique()
            # for subcat in classes:
            #     dtest = filter(lambda x: x[1]==subcat, zip(data_test,target_test))
            #     dtest = random.sample(dtest,n)
            #     for dt,tt in dtest:
            #         temp_data_test.append(dt)
            #         temp_target_test.append(tt)
            # data_test = temp_data_test
            # target_test = temp_target_test   
            # del temp_target_test
            # del temp_data_test
            #===================================================================
            
            X_train, X_test, vectorizer = vectorize(data_train, data_test)
            feature_names = getFeatures(vectorizer)
            print "Total features: {}".format(len(feature_names))
            
            # default implementation
            if opts.select_chi2 >= 1 and opts.chi2_algo.lower() == 'scipy':
                X_train, X_test, feature_names = applyChiSquare(X_train, X_test, target_train, feature_names, opts.select_chi2)
                print "Total features using chi2: {}".format(len(feature_names))
                #===============================================================
                # 
                # print "Writing selected features to file output/multiclass/selected_features.txt"
                # writeToFile(dirpath, "selected_features.txt", "\n" +"-"*80 + "\n", 'a')
                # writeToFile(dirpath, "selected_features.txt", "Scipy Chi2\n", 'a')
                # writeToFile(dirpath, "selected_features.txt", "-"*80 + "\n", 'a')
                # writeToFile(dirpath, "selected_features.txt", feature_names, 'a')
                # writeToFile(dirpath, "selected_features.txt", "\n" + "-"*80 + "\n", 'a')
                #===============================================================
                
            elif opts.chi2_array and opts.chi2_algo.lower() == 'custom':
                key_terms = getFeaturesFromChi2Matrix(attrVals, feature_names, nfs=no_of_features)   
                
                #===============================================================
                # writeToFile(dirpath, "selected_features.txt", "\n" +"-"*80 + "\n", 'a')
                # writeToFile(dirpath, "selected_features.txt", "Custom Chi2" + "\n", 'a')
                # writeToFile(dirpath, "selected_features.txt", "-"*80 + "\n", 'a')
                # writeToFile(dirpath, "selected_features.txt", key_terms, 'a')
                # writeToFile(dirpath, "selected_features.txt", "\n" + "-"*80 + "\n", 'a')
                #===============================================================
                
                print "Total features select using chi2: {}".format(len(key_terms))
                
                X_train, X_test, vectorizer = vectorize(data_train, data_test, vocabulary=key_terms)
                feature_names = getFeatures(vectorizer)
                
            if opts.verbose > 0:
                print "Waiting for classification..."                 
            result = runClassification(clf, X_train, X_test, target_train, target_test)
            results.append(result)
            
            # plot histogram
            #===================================================================
            # histogram(top_dict)
            #===================================================================
            
#             myPCA(X_train.toarray(),target_train,list(set(target_train)))
            #===================================================================
            # if opts.print_top:
            #     target_labels = list(sorted(set(target_test)))
            #     printTopWords(clf, feature_names, target_labels, dirpath, filename)
            #===================================================================
    
    if opts.cv == 0 or opts.cv is None:
        opts.cv = 1      
    calculateScore(results, opts.cv)
    print "Accuracy in top2: {}".format(acc / opts.cv)
    print 'x' * 80
