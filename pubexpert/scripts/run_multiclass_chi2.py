'''
Created on Jul 5, 2016

Ignore this script if you don't want to spend days to retrieve
results using our implementation of Chi2. This is purely
experimental and waste of time. It proved what it was done
earlier in 1998. :(

@author: Afiz

'''
# from sklearn.linear_model.perceptron import Perceptron
# from sklearn.linear_model.ridge import RidgeClassifier
# from sklearn.naive_bayes import MultinomialNB, BernoulliNB
from pubexpert.ml.classifier_helper import createTrainTFIDF, \
    createTestTFIDFTransformation, getFeatures, \
    getTopKWordsFromClassifier, \
    __getVectorizer, getCVIndexes, getClassifierName, classify
from optparse import OptionParser
from sklearn.cross_validation import KFold, train_test_split
from sklearn.svm.classes import LinearSVC
from pubexpert.utility import readCSVToDF, writeToFile,\
    getAndChangeToRootDirectory

import numpy
import os
import sys
import warnings
from pubexpert.ml.X2 import ChiSquare


# parse commandline arguments
op = OptionParser()
op.add_option("--chi2_select",
              action="store", type="int", dest="select_chi2",
              help="Select some number of features using a chi-squared test")
op.add_option("--topkwords", action="store", type="int", dest="print_top",
              help="Print k most discriminative terms per class"
                   " for every classifier.")
op.add_option("--virtual_authors", action="store_true", dest="print_virtauth_test",
              help="Test classifier using virtual authors")
op.add_option("--docs_per_author", action='store', type='int', dest='print_docs',
              help="Use with --virtual_authors to select k docs per virtual authors.")
op.add_option("--cv", action="store", type='int', dest="cv",
              help="cross validation on k folds. Leave one out.")
op.add_option("--filename", action="store", type='str', dest="filename",
              help="Takes .csv file as an input containing header in order 'Nserc SubCategory Id' and 'Text'")
op.add_option("--verbose", action="store", type='int', dest="verbose",
              help="Prints detailed report. 0: prints minimum information. \
              1: also prints timely progress. 2: also prints classification report. \
              3: also print confusion matrix. To write top words and classifier probabilities,\
               use verbose=3.")



(opts, args) = op.parse_args()
if len(args) > 0:
    op.error("this script takes no arguments.")
    sys.exit(1)

if __doc__:
    print(__doc__)
op.print_help()
print


clfs = (
#             (RidgeClassifier(tol=.01, solver="sag"), "Ridge Classifier"),
#             (Perceptron(n_iter=50), "Perceptron"),
#             (MultinomialNB(alpha=.01), "NB: Multinominal"),
#             (BernoulliNB(alpha=.01), "NB: BernoulliNB"),
             (LinearSVC(), "Linear SVC"),
#             (SVC(gamma=0.1),"Radial SVC"),
        )

def calculateScore(result, n_folds):
    # total results from classifier
    print
    print 'x' * 80
    n_classifier = len(clfs)
    for x in xrange(n_classifier):
        a, p, r, f1 = 0.0, 0.0, 0.0, 0.0
        for i in xrange(x, len(result), n_classifier):
            a += result[i][0]
            p += result[i][1]
            r += result[i][2]
            f1 += result[i][3]
        a, p, r, f1 = a / n_folds, p / n_folds, r / n_folds, f1 / n_folds
        print "Classifier: {}".format(clfs[x][1])
        print('Accuracy:    %0.3f' % (a))
        print('Precision:    %0.3f' % (p))
        print('Recall:    %0.3f' % (r))
        print('F1-Score:    %0.3f' % (f1))  
        print '_' * 15
        
def printTopWords(clf, feature_names, target_labels, dirpath, filename, cv=0):
    """
        Called after doing classification
    """  
    top = getTopKWordsFromClassifier(clf, feature_names, target_labels, opts.print_top)
    # write top k words to file 
    if opts.cv > 0:
        line = '=' * 80
        line += os.linesep
        line += 'CV: ' + str(cv) if cv > 0 else ''
        line += os.linesep
        line += '-' * 80
        line += os.linesep
        writeToFile(dirpath, filename, line, option='a')
    
    # print top k words for each class 
    for k, v in top.iteritems():
        print "{}: {}".format(k, ' '.join(v))                  
        # write top k words to file
        if opts.verbose > 2: 
            writeToFile(dirpath, filename, "{}:{}\n".format(k, ' '.join(v)), option='a')

def vectorize(data_train, data_test, vocabulary=None):
    #-------------------- Create TF.IDF  -------------------------
    if opts.verbose > 0:
        print "Creating TF.IDF matrix for training and test dataset."
        
    vectorizer = __getVectorizer(max_df=1.00, min_df=1, vocabulary=vocabulary)
    X_train = createTrainTFIDF(data_train, vectorizer)
    X_test = createTestTFIDFTransformation(data_test, vectorizer)
    return X_train, X_test , vectorizer     

def buildChi2ScoreMatrix(X_train, feature_names):
    print "Total docs: {0}".format(X_train.shape[0])

    if opts.verbose > 0:
        print "Applying chi2 ....."
     
    attrVals = numpy.empty([X_train.shape[1], len(numpy.unique(target_train))], dtype=float)

    clusters = []
    numpy_arr_target_labels = sorted(list(set(target_train)))
    for label in numpy_arr_target_labels:  # where 9 is number of classes.
        clusters.append(numpy.where(numpy.array(target_train) == label)[0])
    obj = ChiSquare()
    obj.simpleChiSquare(attrVals, clusters, X_train, X_train.shape[0])
    return attrVals

def getFeaturesFromChi2Matrix(attrVals,feature_names, nfs=[]):
    """
        Get nf percent features from computed Chi2 matrix (attrVals)
        
        Params:
            attrVals (numpy 2d matrix):             chi2 computed matrix 
            feature_names (numpy array):  vocabulary to be used to matrix calculation.
            nfs (list):    list of number of features to be used for each class
            
        Out Params:
            X_train, X_test: Newly computed matrix for train and test data from feature set
            vectorizer: new vectorizer used for creating tf.idf.
    """
                   
    
    keyterm_indexes = []
    keyterms = set()
#         attIDTemp = numpy.argmax(attrVals,axis=1)
    
    # sort in desc score from chi square
    for p in range(len(nfs)):
#         if opts.verbose > 0:
#             print "Applying chi2 to select {} features from class {}.".format(nfs[p], p + 1)
        temp = numpy.argsort(attrVals[:, p])
        temp = temp[::-1]
        keyterm_indexes.append(temp[:nfs[p]])
    
    # get terms from feature_names list using index in keyterm_indexes
    for ktarr in keyterm_indexes:
        temparr = []
        for idx in ktarr:
            temparr.append(feature_names[idx])  
        keyterms.update(temparr)
    print
    print "total features: "+ str(len(keyterms))
    print 
    return vectorize(data_train, data_test, vocabulary=list(keyterms))
        
def runClassification(clf, X_train, X_test, target_train, target_test, cv=0):
    
    
    #===========================================================================
    # if opts.print_virtauth_test:
    #     data_test, target_test = createVirtualAuthors(data_test, target_test, opts.print_docs)
    #===========================================================================
        
    if opts.verbose > 0:
        print "Starting classification..."
      
    with warnings.catch_warnings():
        warnings.filterwarnings("ignore", category=DeprecationWarning)
        
        print('=' * 80)
        print(name)
        print '-' * 80
    
        return_tuple= classify(clf, X_train, X_test, target_train, target_test, opts.verbose, cv)
        if len(return_tuple) == 1:
            print "This script is not yet ready for this modification"
        
        if len(return_tuple) == 5:
            accuracy, precision, recall, f1, test_idx  = return_tuple
        #=======================================================================
        # truecounts = 0
        # for prob, pred, idx, probs in test_idx:
        #     top2 = sorted(zip(range(1, 10), probs), key=lambda t:t[1], reverse=True)[:3]
        #     top2 = ['LSA0' + str(v[0]) for v in top2]
        #     if target_test[idx] in top2:
        #         truecounts += 1
        # print "Acc: " + str(truecounts / float(len(target_test)))       
        #=======================================================================
#===============================================================================
#         for prob, pred, idx, probs in test_idx:
#             if pred != target_test[idx] and max(probs)>=0.3000 and max(probs)< 0.4000:
# #                 print idx,
#                 print '{idx}\t{actual}\t{pred}\t{probs}'.format(idx=idx,actual=target_test[idx], pred=pred, probs='\t'.join([str(round(v, 4)) for v in probs]))
# #                 for t, tfidfscore in zip(data_test[idx].split(' '),X_test.getrow(idx).data.tolist()):
# #                     print '{}\t{}'.format(t,round(tfidfscore,4))
#===============================================================================
        
        return [accuracy, precision, recall, f1]

def getTotalFeaturesForEachClass(data_train, target_train):
    
    values = numpy.unique(target_train,return_counts=True)
    labels = values[0].tolist()
    counts = values[1].tolist()
    
    nfs = []
    for l in labels:
        data = filter(lambda x:x[1] == l, zip(data_train, target_train))
        data = numpy.array(data)
         
        vectorizer = vectorize(data[:, 0].tolist(), data[:3,0])[2]
        totalfeatures = len(getFeatures(vectorizer))
        nf = totalfeatures * (opts.select_chi2 / 100.0)
        nfs.append(int(nf))
    
    #===========================================================================
    # for c in counts:
    #     nfs.append(int(c*(opts.select_chi2/100.0)))
    #===========================================================================
    return nfs
     
# Script executes from here   
                
if __name__ == '__main__':

    #--------------------- read csv file----------------------------
    if opts.verbose > 0:
        print "Reading {} and converting to dataframe...".format(opts.filename)
        
    df = readCSVToDF(opts.filename, header_names=['NCatId', 'Text'])

    columns = df.columns.tolist()
    data = [s[0] for s in df.as_matrix(['Text']).tolist()]
    target = [s[0] for s in df.as_matrix(['NCatId']).tolist()]
    
    if opts.verbose > 0:
        print "Done reading data..."
    
    data_train, data_test, target_train, target_test = None, None, None, None
    nfs = [500]*9
    #nfs = [100,100,100,100,100,100,100,100,100]   
    results = []
    for clf, name in clfs :
        
        dirpath = os.path.join(getAndChangeToRootDirectory(), 'resources', 'output', 'multiclass')
        filename = getClassifierName(clf) + '_chi2_top_words.txt'   
        if not os.path.exists(dirpath):
            os.mkdir(dirpath)
        writeToFile(dirpath, filename, '', 'w')
        
        if opts.cv > 0:
            if opts.verbose > 0:
                print "Waiting for cross-validation..."
            kf = KFold(len(data), n_folds=opts.cv, shuffle=True, random_state=33)
            genCV = getCVIndexes(data, target, kf)
   
            
            for x in xrange(1, opts.cv + 1):
                print "CV: {}".format(x)
                data_train, data_test, target_train, target_test = genCV.next()
                
                X_train, X_test, vectorizer = vectorize(data_train, data_test)
                feature_names = getFeatures(vectorizer)
                
                    
                attrVals = buildChi2ScoreMatrix(X_train, feature_names)
                     
#                     for opts.select_chi2 in [99, 98, 96, 94, 92, 90, 85, 80]:  # 70, 60, 50, 40, 20, 10, 0]:
#                         opts.select_chi2 = 100 - opts.select_chi2
#                         nfs = getTotalFeaturesForEachClass(data_train, target_train)
                            
                # comment this line for default implmentation
                X_train, X_test, vectorizer = getFeaturesFromChi2Matrix(attrVals, feature_names, nfs=nfs)
                feature_names = getFeatures(vectorizer)
                
                result = runClassification(clf, X_train, X_test, target_train, target_test, cv=x)
                results.append(result)
        
                if opts.print_top:
                    target_labels = list(sorted(set(target_test)))
                    printTopWords(clf, feature_names, target_labels, dirpath, filename, cv=x)
        
            calculateScore(results, opts.cv)
        
        else:
            if opts.verbose > 0:
                print "Waiting for classic classification..."
            data_train, data_test, target_train, target_test = train_test_split(data, target, test_size=0.3, random_state=33)
            
            X_train, X_test, vectorizer = vectorize(data_train, data_test)
            feature_names = getFeatures(vectorizer)

            attrVals = buildChi2ScoreMatrix(X_train, feature_names)
                
                
#                 for opts.select_chi2 in [99, 98, 96, 94, 92, 90, 85, 80,70, 60, 50, 40, 20, 10, 0]:
#                     opts.select_chi2 = 100 - opts.select_chi2
#                     print "Percentage of features: " + str(opts.select_chi2)
#                     nfs = getTotalFeaturesForEachClass(data_train, target_train)
                              
            X_train, X_test, vectorizer = getFeaturesFromChi2Matrix(attrVals, feature_names, nfs=nfs)
            feature_names = getFeatures(vectorizer)
                                                   
            result = runClassification(clf, X_train, X_test, target_train, target_test)
            results.append(result)
                
            if opts.print_top:
                target_labels = list(sorted(set(target_test)))
                printTopWords(clf, feature_names, target_labels, dirpath, filename)
                
            calculateScore([result], 1)
                
        print 'x' * 80

