'''
Created on Jul 5, 2016

@author: Afiz
'''

from pubexpert.ml.classifier_helper import createTrainTFIDF, \
    createTestTFIDFTransformation, getFeatures, applyChiSquare, __getVectorizer
from optparse import OptionParser
from sklearn.cross_validation import  train_test_split
from pubexpert.utility import readCSVToDF
from time import time
from sklearn import manifold, decomposition
import matplotlib.pyplot as plt
import numpy as np
import sys
from matplotlib import offsetbox
from matplotlib.pyplot import cm

# parse commandline arguments
op = OptionParser()
op.add_option("--chi2_select",
              action="store", type="int", dest="chi2_select",
              help="Select some number of features using a chi-squared test")
op.add_option("--filenames", action="store", type='str', dest="filenames",
              help="Takes .csv files separated by comma as an input containing header in order \
               'Nserc SubCategory Id' and 'Text'")
op.add_option("--exclude_classes", action="store", type='str', dest="remove_classes",
              help="comma separated class names")
op.add_option("--model_type", action='store', type='str', dest='model_type',
              help='Type of classification task takes one of the values, bow,boc,bok.')
op.add_option("--verbose", action="store", type='int', dest="verbose",
              help="Prints detailed report. 0: prints minimum information. \
              1: also prints timely progress. 2: also prints classification report. \
              3: also print confusion matrix. To write top words and classifier probabilities,\
               use verbose=3.")



(opts, args) = op.parse_args()
if len(args) > 0:
    op.error("this script takes no arguments.")
    sys.exit(1)

if __doc__:
    print(__doc__)
op.print_help()
print


cmaps = [           
            'Accent', 'Dark2', 'Paired', 'Pastel1',
            'Pastel2', 'Set1', 'Set2', 'Set3'
        ]


def vectorize(data_train, data_test, vocabulary=None, binary=False):
    #-------------------- Create TF.IDF  -------------------------
    if opts.verbose > 0:
        print "Creating TF.IDF matrix for training and test dataset."
        
    vectorizer = __getVectorizer(max_df=1.00, min_df=1, vocabulary=vocabulary, binary=binary)
    X_train = createTrainTFIDF(data_train, vectorizer)
    X_test = createTestTFIDFTransformation(data_test, vectorizer)
    return X_train, X_test , vectorizer     
                
                
# Scale and visualize the embedding vectors
def plot_embedding(X, X_data, X_target, title=None):
    x_min, x_max = np.min(X, 0), np.max(X, 0)
    X = (X - x_min) / (x_max - x_min)

    plt.figure()
    
   
    # color list for LSA 
    color_list = getattr(cm, 'Set2')(np.linspace(0, 1, 10))
    # color list for LSB
    color_list = np.append(color_list, getattr(cm, 'Accent')(np.linspace(0, 0.6, 10)), axis=0)
    # color list for computer science cat 1-10
    color_list = np.append(color_list, getattr(cm, 'flag')(np.linspace(0, 0.65, 10)), axis=0)
    # color list for computer science topics 11-21
    color_list = np.append(color_list, getattr(cm, 'Dark2')(np.linspace(0, 1, 11)), axis=0)
    
    # ax = plt.subplot(111)
    for i in range(X.shape[0]):
        plt.text(X[i, 0], X[i, 1], str(X_target[i]),
                 color=color_list[int(X_target[i])-1],  # plt.cm.Set1(gradient[int(X_target[i])]),
                 fontdict={'weight': 'bold', 'size': 9})

    if hasattr(offsetbox, 'AnnotationBbox'):
        # only print thumbnails with matplotlib > 1.0
        shown_images = np.array([[1., 1.]])  # just something big
        for i in range(X_data.shape[0]):
            dist = np.sum((X[i] - shown_images) ** 2, 1)
            if np.min(dist) < 4e-3:
                # don't show points that are too close
                continue
            shown_images = np.r_[shown_images, [X[i]]]
            #===================================================================
            # imagebox = offsetbox.AnnotationBbox(
            #     offsetbox.OffsetImage(digits.images[i], cmap=plt.cm.gray_r),
            #     X[i])
            # ax.add_artist(imagebox)
            #===================================================================
    plt.xticks([]), plt.yticks([])
    if title is not None:
        plt.title(title)
        

if __name__ == '__main__':

    # --------------------- read csv file----------------------------
    
    # for implementing t-SNE
    data, target = [], []
    for fn in opts.filenames.split(','):
        if opts.verbose > 0:
            print "Reading {} and converting to dataframe...".format(fn)
            
        #df = readCSVToDF(fn, header_names=[ 'Text', 'NCatId'])
        df = readCSVToDF(fn, header_names=['Id', 'Text', 'Concepts', 'Categories', 'NCatId'])
        df.dropna(0, how='any', inplace=True)
        
        if opts.remove_classes:
            rclass = [c.strip().upper() for c in opts.remove_classes.split(',')]
            df = df[df.NCatId.apply(lambda e: e not in rclass)]
            
        # df = df.groupby("NCatId").head(500).reset_index(drop=True)
        
        for c in df["NCatId"].unique():
            if c[:-2] == "CS":
                df["NCatId"].replace(c, 20 + int(c[-2:]), inplace=True)
            elif c[:-2] == "LSA":
                df["NCatId"].replace(c, int(c[-2:]), inplace=True)
            elif c[:-2] == "LSB":
                df["NCatId"].replace(c, 10 + int(c[-2:]), inplace=True)
                
        select_column = None
        if opts.model_type == 'bow':
            select_column = 'Text'
        elif opts.model_type == 'boc':
            select_column = 'Concepts'
        elif opts.model_type == 'bok':
            select_column = 'Categories'
        else:
            raise Exception("--model_type incorrect. Take one of these values: bow,boc,bok")
        
        columns = df.columns.tolist()
        target.extend([s[0] for s in df.as_matrix(['NCatId']).tolist()])
        data.extend([s[0] for s in df.as_matrix([select_column]).tolist()])
    
    if opts.verbose > 0:
        print "Done reading data..."
    
    data_train, data_test, target_train, target_test = None, None, None, None


    if opts.verbose > 0:
        print "Waiting for classic classification..."
    
    data_train, data_test, target_train, target_test = train_test_split(data, target, test_size=0.10, random_state=33)
    
    if opts.model_type in ['boc', 'bok']:
        print "Creating  binary tf.idf vectors for BOC and BOK..."
        X_train, X_test, vectorizer = vectorize(data_train, data_test, binary=True)
    else:
        X_train, X_test, vectorizer = vectorize(data_train, data_test)
    
    feature_names = getFeatures(vectorizer)
    
    if opts.chi2_select > 100:
        if len(feature_names) < opts.chi2_select:
            opts.chi2_select = len(feature_names)  
        X_train, X_test, feature_names = applyChiSquare(X_train, X_test, target_train, feature_names, total_features=opts.chi2_select)
    
    
    print "Shape of vector: {}".format(X_train.shape)
    
    #----------------------------------------------------------------------
    # Projection on to the first 2 principal components
    
    print("Computing PCA projection to n dimension to scale on t-SNE")
    t0 = time()
    X_pca = decomposition.TruncatedSVD(n_components=10).fit_transform(X_train)
#     plot_embedding(X_pca,
#                    "Principal Components projection of the digits (time %.2fs)" %
#                    (time() - t0))
    
    
    # t-SNE embedding of the digits dataset
    print("Computing t-SNE embedding")
    tsne = manifold.TSNE(n_components=2, perplexity=30.0, random_state=33, verbose=2)
    t0 = time()
    X_tsne = tsne.fit_transform(X_pca)
    
    plot_embedding(X_tsne, X_train, target_train,
                   "t-SNE embedding of the digits (time %.2fs)" % 
                   (time() - t0))
    
    plt.show()

    print "x"* 80
    
    #===========================================================================
    # # save to file for tSNE
    # train_samples = X_train.toarray().tolist()
    # train_samples_tsv= [ '\t'.join([str(v) for v in l]) for l in train_samples]
    # rootpath = getAndChangeToRootDirectory()
    # outfilepath = os.path.join(rootpath, 'resources','output',)
    # writeToFile(outfilepath, 'tsne_chi2_data.txt', train_samples_tsv, option='w')
    # 
    # 
    # target_train_tsv = [str(v) for v in target_train]
    # writeToFile(outfilepath, 'tsne_chi2_labels.txt', target_train_tsv,option='w')
    # # ----------------                                       
    #             
    # print 'x' * 80
    #===========================================================================
