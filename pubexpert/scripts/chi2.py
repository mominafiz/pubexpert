'''
Created on Aug 15, 2016
@author: Afiz

Description: This script is used for ChiSquare feature selection
using own implementation of chi2 average.


'''
from  pubexpert.ml.classifier_helper import __getVectorizer, createTrainTFIDF, getFeatures
import numpy
from pubexpert.utility import readCSVToDF, writeToFile,\
    getAndChangeToRootDirectory
from pubexpert.ml.X2 import  ChiSquare, ChiSquareMultiThread
from optparse import OptionParser
import os
import sys
from sklearn.cross_validation import train_test_split
import time

 
# parse commandline arguments
op = OptionParser()
op.add_option("--filename", action="store", type='str', dest="filename",
              help="Takes .csv file as an input containing header in order 'Nserc SubCategory Id' and 'Text'")
op.add_option("--outputfile", action="store", type='str', dest="outputfile",
              help="Filename with .npy extension to write ndarray to binary file.")
op.add_option("--multithread", action="store", type='int', dest="threads",
              help="Total number of threads to be used.")

(opts, args) = op.parse_args()
if len(args) > 0:
    op.error("this script takes no arguments.")
    sys.exit(1)

if __doc__:
    print(__doc__)
op.print_help()
print


# Script executes from here   
                
if __name__ == '__main__':
    print "Input file: {}\nOutput file: {}\n".format(opts.filename, opts.outputfile)
    dirpath = os.path.join(getAndChangeToRootDirectory(), 'resources', 'chi2')
    if opts.outputfile.split('.')[-1] != 'npy':
        print "filename should have .npy extension."
    
    if os.path.exists(os.path.join(dirpath, opts.outputfile)):
        overwrite = raw_input("Do you want to overwrite file '{}':[y/n] ".format(opts.outputfile))
        if overwrite.lower() == "y":
            writeToFile(dirpath, opts.outputfile, '', 'w')
            
            
    df = readCSVToDF(opts.filename, header_names=['NCatId', 'Text'])
    
    data = [s[0] for s in df.as_matrix(['Text']).tolist()]
    target = [s[0] for s in df.as_matrix(['NCatId']).tolist()]
    

    data_train, data_test, target_train, target_test = train_test_split(data, target, test_size=0.3, random_state=33)
    
    vectorizer = __getVectorizer(max_df=1.00, min_df=1)
    X_train = createTrainTFIDF(data_train, vectorizer)
    feature_names = getFeatures(vectorizer)
    
    print "Total docs: {0}".format(X_train.shape[0])
    print "Total features: {}".format(X_train.shape[1])
    print "Applying chi2 ....."
    
    classes = sorted(list(set(target_train)))
    attrVals = numpy.empty([X_train.shape[1], len(numpy.unique(classes))], dtype=float)

    clusters = []
    for label in classes:  # where 9 is number of classes.
        clusters.append(numpy.where(numpy.array(target_train) == label)[0])
    
    
    chi2Obj = ChiSquare(attrVals, clusters, X_train, X_train.shape[0])
    
    
    if opts.threads <= 0:
        opts.threads = 1
    t0 = time.time()
    threads = []
    thread_name = 1
    for i in range(0, X_train.shape[1], (X_train.shape[1] / opts.threads)):
        end = i + X_train.shape[1] / opts.threads
        if end > X_train.shape[1]:
            end = X_train.shape[1]
             
        t = ChiSquareMultiThread(name=str(thread_name), target=chi2Obj.simpleChiSquare, args=(i, end))
        thread_name += 1
        threads.append(t)
         
    for t in threads:
        t.start()
         
    for t in threads:
        t.join()
         
    print 'Chi2 {} computations done in {}s using {} threads'.format(X_train.shape[0]*X_train.shape[1], (time.time() - t0), len(threads))   

    numpy.save(os.path.join(dirpath, opts.outputfile), attrVals)  
    
                
    print 'x' * 80

