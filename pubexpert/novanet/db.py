from pymongo import MongoClient
from pymongo.errors import ConfigurationError
import pymongo


# TODO: Create Singleton class for MongoDb
class MongoDb:
    """
        This class allows to interact with MongoDB and exposes CRUD operations.
    
    """
    def __init__(self, host, port):
        self.__connection = MongoClient(host, port)
        self.__client = self.__connection
        self.__collection = None
        self.__dbname = None
        
    def insertDocument(self, doc):
        """
            Insert individual document or list of document in a collection.
            It raises ValueError if collection and database is not set and 
            raises TypeError for incorrect param type 
            
            Returns inserted ids (primary key)
            
            Params:
                doc: list of document of type dict or dict
            
        """
        
        if not (self.getCollection() or self.getCurrentDb()):
            raise ValueError("Collection/Database does not exists. Illegal call to insert document.")
        
        ids = []
        if isinstance(doc, dict):
            ids.append(self.__connection.insert_one(doc).inserted_id)
        elif isinstance(doc, list):
            for d in doc:
                try:
                    ids.append(self.__connection.insert_one(d).inserted_id)
                except pymongo.errors.DuplicateKeyError:
                    pass 
#                     print d.get("document_title")
        else:
            raise TypeError("<doc> of type dict or list is expected.")
        
        return ids
        
    def updateDocument(self, find, update_param, multi=False, upsert=False):
        """
            Update document in collection or insert when not exists, if 'upsert' is True. 
            The nature of update of documents (single or multiple) depends on params 'multi'.
            Raises ValueError for incorrect param values. 
             
            Returns number of docs matches and modified as tuple.
            
            Params:
                find:
                update_param:
                multi:
                upsert:
        
        """
        if isinstance(find, dict) and isinstance(update_param, dict):
            result = self.__connection.update_one(find, update_param, upsert=upsert) if not multi else self.__connection.update_many(find, update_param, upsert=upsert) 
            return result.matched_count, result.modified_count
        else:
            raise ValueError("parameters cannot be None.")
            
    def getDocument(self, find, projection=None, multi=True):
        """
            Returns the cursor object of class Cursor.
        """
        return self.__connection.find_one(find, projection) if not multi else self.__connection.find(find, projection, no_cursor_timeout=True)
    
    def setDatabase(self, dbname):
        self.__dbname = dbname
        self.__connection = self.__connection[dbname]
        
    def getCurrentDb(self):
        """
            Throws ConfigurationError if not database is set.
        """
        if not self.__dbname:
            raise ConfigurationError("No database provided.")
        return self.__connection.get_default_database()
    
    def setCollection(self, name):
        if self.__collection is not None:
            raise Exception("Cannot set collections again. Create another connection.")
        self.__collection = name
        if not self.__dbname:
            raise ValueError("Call setDatabase() first.")
        
        self.__connection = getattr(self.__connection, name) 
       
    def getCollection(self):
        return self.__collection

    def dropCollection(self):
        if self.getCollection():
            self.__connection.drop()
            return True
        else:
            return False
 

    def distinctValues(self, column, query={}):
        return self.__connection.distinct(column, query)
    
    def removeDocuments(self,find, multi=True):
        if isinstance(find,dict):
            if multi==True:
                self.__connection.delete_many(find)
            else:
                self.__connection.delete_one(find)
                
    def closeConnection(self):
        self.__client.close()
