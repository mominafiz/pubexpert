from enum import Enum

class Params(Enum):
    institution = 'institution'
    vid = 'vid'
    query = 'query'
    search_scope = 'search_scope'  # not supported. Used only for Deeplinks.
    group = 'group'  # not supported.
    prefLang = 'prefLang'
    loc = 'loc'
    indx = 'indx'
    bulkSize = 'bulkSize'
    sortField = 'sortField'
    query_inc = 'query_inc'
    query_exc = 'query_exc'  
    more = 'more'
    lang = 'lang'
    pcAvailability = 'pcAvailability'
    json = 'json'
    
class UrlBuilder:
    """
        Builds the URL for querying Novanet API (Limited to DeepLinks).
        
        
        Documentation: https://developers.exlibrisgroup.com/primo/apis
        
    """
    
    
    def __init__(self, host='', port='', domain=''):
        """
            Initializes base url and query params template.
        """
        self.params = {}
        self.base_url = 'http://{host}/PrimoWebServices/xservice/search/brief?'
        
        if (not host or not port) and not domain:
            raise ValueError("host/port or domain cannot be empty.")
        else:
            ip = ''
            if host and port:
                ip = "{0}:{1}".format(host, port)
            else:
                ip = domain
        
            self.base_url = self.base_url.format(host=ip)
    
    def addQuery(self, field, precision, value):
        """
            It is mandatory to send request and it iteratively builds the query params. 
            
            Example: &query=rid,exact,PRIMO1234 

            Params:
                field (str): following possible values -
                    a) wildcard - any
                    b) regular fields - 
                        desc, title, creator, sub (subject), rtype (resource type), isbn, issn, 
                        rectype (record type), dlink (down link), ftext (full text), general, toc, fmt, 
                        lang, cdate (creation date), sid (source ID), rid (record ID), addsrcrid (additional source ID), 
                        addtitle, pnxtype, alttitle, abstract, and fiction       
                    b) facet fields - 
                        facet_creator, facet_lang, facet_rtype, facet_pfilter, facet_topic, facet_creationdate, facet_dcc, 
                        facet_lcc, facet_rvk, facet_tlevel,facet_domain, facet_fsize, facet_fmt, facet_frbrgroupid, 
                        facet_frbrtype, facet_local1 - facet_local50 
                
                precision (str):
                    a) exact - exact match for this phrase. 
                         
                    b) contains - When searching for one term, will return results for any field 
                       containing this term. When searching for a phrase, will return 
                       results for any field containing at least one of the given terms. 
                       For example: &query=title,contains,queens+of+the+stone+age 

                    c) begins_with - Works only against the sort title field (swstitle)! 
                
                value (str): Any value is allowed. Replace ',' with ' ' (space)
                      The value can also hold multiple values with an OR, AND, or NOT operator                  
        
        """
        if not self.params.has_key(Params.query.value):
            self.params[Params.query.value] = []
        self.params.get(Params.query.value).append("{},{},{}" 
                                   .format(field, precision, value)) 
    
    def setInstitution(self, inst):
        """ Institution code """
        if inst:
            self.params[Params.institution.value] = inst
            
    def setVid(self, vid):
        """ Institution Id. """
        if vid:
            self.params[Params.institution.value] = vid
    
    def setLoc(self, target, location_ref):
        """
            Sets 
            <target> possible values: 

                1) local - For a local search 
                2) adaptor - For a deep search 
                3) remote - For a remote search against Metalib         
            
            <location reference> possible values: 
            
                1) for local - define the scope to search on 
                    Example: loc=local,scope:(VOLCANO) 
                2) for adaptor - need to define the name of the requested adaptor 
                    Example: loc=adaptor,primo_central_multiple_fe 
                3) for remote - need to define a quick set name 
                    Example: loc=remote,Business and Sciences
        """
        if target and location_ref:
            self.params[Params.loc.value] = "{},{}".format(target, location_ref)
           
    def setIndex(self, index=1):
        """
            Sets the start index of the result set.
        """
        if index >= 0:
            self.params[Params.indx.value] = index
        else:
            raise Exception("Incorrect index value.")
    
    def setBulkSize(self, bulkSize=500):
        """
            sets number of results to be returned. Max -> 1000
        """
        if bulkSize <= 0:
            raise ValueError("Incorrect bulkSize parameter.")
        self.params[Params.bulkSize.value] = bulkSize
    
    def getBulkSize(self):
        return self.params.get(Params.bulkSize.value)
    
    def setPreferredLang(self, lang):
        """
            Code for the representation of name of language conform to ISO-639
            Returns language to display on UI. (not for xservices API)
        """
        if lang:
            self.params[Params.lang.value] = lang
    
    def setSortField(self, srt):
        """
            1) stitle - performs Title sort. 
            2) scdate - performs Date sort. 
            3) screator - performs Author sort. 
            4) popularity - Performs Popularity sort. 
        """
        if srt and srt in ["stitle", "scdate", "screator", "popularity"]:
            self.params[Params.sortField.value] = srt
    
    def setExpandBeyongMyLibrary(self, option=True):
        """
            Emulates the "Expand My Results" checkbox in the Primo UI.
        """
        self.params[Params.pcAvailability.value] = option
        
    def setJsonPreference(self, json=True):
        self.params[Params.json.value] = json
        
    def setMoreOption(self, more=True):
        """
            Applicable only for remote search. It simulates the "Get More" 
            functionality. 
        """
        self.params[Params.more.value] = more
        
    def setDateRange(self, start, end):
        """
            Date should be in format YYYY
        """
        dtrange = '[{}+TO+{}]'.format(start,end)
        self.addQuery('facet_creationdate', 'exact', dtrange)
#         self.setQueryIncludes('facet_creationdate',dtrange)
#         self.addQuery('dr_s', 'exact', start)
#         self.addQuery('dr_e', 'exact', end)
        
    def setResultLang(self, lang):
        """
            Returns the results that have documents in English language.
        """
        self.addQuery('facet_lang', 'exact', lang)  
        
    def setQueryIncludes(self,field,*values):
        """
            Special usage for facet search. Does not support facet_creationdate, peer_review, fulltext options.
            <precision> in the query parameter only supports exact.
            <values> separated by ',' are treated as logical OR
            
            Supported facets: 
                    facet_creator, facet_lang, facet_rtype, facet_pfilter, facet_topic, 
                    facet_creationdate, facet_dcc, facet_lcc, facet_rvk, 
                    facet_tlevel,facet_domain, facet_fsize, facet_fmt, facet_frbrgroupid, 
                    facet_frbrtype, facet_local1 - facet_local50 
                    
            Example: query_inc=<field>,<precision>,<value1,value2,...> 
                    &query_inc=facet_rtype,exact,reviews,articles                   
                
        """
        if field and values:
            if not self.params.has_key(Params.query_inc.value):
                self.params[Params.query_inc.value] = []

            self.params.get(Params.query_inc.value).append("{},exact,{}".format(field,','.join(*values))) 
        else:
            raise ValueError("Parameters cannot be None.")
    
    def setQueryExclude(self,field, *values):
        """
            Special usage for facet search. Does not support facet_creationdate, peer_review, fulltext options.
            <precision> in the query parameter only supports exact.
            <values> separated by ',' are treated as logical OR
            
            Supported facets: 
                    facet_creator, facet_lang, facet_rtype, facet_pfilter, facet_topic, 
                    facet_creationdate, facet_dcc, facet_lcc, facet_rvk, 
                    facet_tlevel,facet_domain, facet_fsize, facet_fmt, facet_frbrgroupid, 
                    facet_frbrtype, facet_local1 - facet_local50 
                    
            Example: query_exc=<field>,<precision>,<value1,value2,...> 
                    &query_exc=facet_rtype,exact,reviews,articles
        """
        

        if field and values:
            if not self.params.has_key(Params.query_inc.value):
                self.params[Params.query_exc.value] = []
            self.params.get(Params.query_exc.value).append("{},exact,{}".format(field,','.join(*values))) 
        else:
            raise ValueError("Parameters cannot be None.")
        