import requests
from enum import Enum
from pubexpert.utility import convertToASCII
from datetime import datetime
import time


class NovaNetAPI:
    
    
    def __init__(self):
        self.result_url = None
        self.hasError = True
    
    def getData(self, base_url, params):
        """
            Sends request to Novanet API using base url and params.
            Return results in python dictionary.
        """
        
        r = requests.get(base_url, params=params)
        self.result_url = r.url
        
        if not r.status_code == 200:
            raise Exception("Error! Request return status code: {}".format(r.status_code))
        
        if r.text.startswith('<'):
            raise NotImplementedError("XML not supported.")
            # rdict = xmltodict.parse(r.text)  # xml to json
            # jsonstr = json.dumps(rdict)     # convert to json str.
            # jsonstr = unicodedata.normalize('NFKD', jsonstr).encode("ascii")
            # return json.loads(jsonstr)        
        else:
            jdata = r.json()
            if self.get("ERROR", jdata):
                self.hasError = True
            else:
                self.hasError = False
            return jdata
        

    def get(self, key, iterable={}):
        """
            Recursively find the key in dictionary and returns the value against the key. 
            
            Params:
                iterable (dict): Json data converted to python dict.
                key (str): key to be search in iterable 
        """
        if iterable is None:
            raise ValueError("Parameter 'iterable' cannot be None.")
        
        key = convertToASCII(key)
        if isinstance(iterable, unicode):
            print iterable
            raise ValueError("Expected dict, but got {}".format(type(iterable)))
        if iterable.has_key(key):
            return iterable.get(key)
        else:
            for value in iterable.itervalues():
                value = convertToASCII(value)
#                 print '{} ==> {}'.format(k,value) 
                if isinstance(value, dict):
                    val = self.get(key, value)
                    if val:
                        return val 
    

        
    def parseDocuments(self, json_dict):
        """
            Extracts bibliographic text information and an abstract.
            
            Returns the list of parse document
            Params:
                json_dict (dict): Parse json string of type dict.
                
        """
        
        listOfDocs = []
        
        docset = self.get("DOC", json_dict)
        if not docset or not isinstance(docset, list):
            # raise Exception("DOC key expected in parameter of type dict.")
            return listOfDocs
        
        for document in docset:
            add_data = self.get("addata", document)
            display = self.get('display', document)
            doc = {}
            
            if add_data and display:
                
                authors = self.get('au', add_data)
                
                if isinstance(authors, list):
                    doc[Document.authors.value] = authors
                elif isinstance(authors, str):
                    doc[Document.authors.value] = [authors]
                else:
                    doc[Document.authors.value] = []
                
                doc[Document.document_title.value] = convertToASCII(self.get('atitle', add_data))
                doc[Document.journal_name.value] = convertToASCII(self.get('jtitle', add_data))
                doc[Document.doi.value] = convertToASCII(self.get('doi', add_data))
                
                spage = convertToASCII(self.get('spage', add_data))
                doc[Document.startpage.value] = int(spage) if isinstance(spage, int) else 0
                
                epage = convertToASCII(self.get('epage', add_data))
                doc[Document.endpage.value] = int(epage) if isinstance(epage, int) else 0
                
                doc[Document.genre.value] = convertToASCII(self.get('genre', add_data))
                doc[Document.issn.value] = convertToASCII(self.get('issn', add_data))
                
                issueno = convertToASCII(self.get('issue', add_data))
                doc[Document.issue.value] = int(issueno) if isinstance(issueno, int) else 0 
                doc[Document.publication.value] = convertToASCII(self.get('pub', add_data))
                doc[Document.resource_type.value] = convertToASCII(self.get('ristype', add_data))
                
                vol = convertToASCII(self.get('volume', add_data))
                doc[Document.volume.value] = int(vol) if isinstance(vol, int) else 0
                
                datestr = convertToASCII(self.get('risdate', add_data))
                try:
                    rdate = datetime(int(datestr[:4]), 1, 1)
                    doc[Document.resource_date.value] = rdate
                except:
                    doc[Document.resource_date.value] = datetime(1900, 01, 01)
                    
                    
                display = self.get('display', document)
                doc[Document.description.value] = convertToASCII(self.get('description', display))
                doc[Document.is_peer_reviewed.value] = True if self.get('Ids50', display) == 'peer_reviewed' else False
                
                for k in doc.iterkeys():
                    if not doc.get(k):
                        doc[k] = ''
                listOfDocs.append(doc)
        return listOfDocs
    
    def getTotalDocuments(self, json_dict):
        return int(self.get("@TOTALHITS", json_dict))
    
    def getStartAndEndIndex(self, json_dict):
        return int(self.get("@FIRSTHIT", json_dict)), int(self.get("@LASTHIT", json_dict))
    
    def getTopicFacetList(self, json_dict):
        topics = []
        try:
            topics =  [convertToASCII(topic.get('@KEY')) for facet in self.get("FACET", json_dict) if facet.get('@NAME') == 'topic' for topic in facet.get('FACET_VALUES')]           
        except:
            print "Failed to extract facet topics."
        finally:
            return topics
        
    def getFacetCreationDate(self, json_dict):
        dranges = []
        try:
            dranges = [int(convertToASCII(topic.get('@KEY'))) for facet in self.get("FACET", json_dict) if facet.get('@NAME') == 'creationdate' for topic in facet.get('FACET_VALUES')]
        except:
            print "Failed to extract facet creation date range."
        return sorted(dranges, reverse=True)
    
    def exhaustiveGet(self, qObj):
        """
            Iterator that returns tuple of total number of documents, parsed documents, and topics in particular year, and return url for each call to an API.
            
            Param:
                qObj: query object that has required parameter for API call.
                All other parameter are not required.            
        """
    
        while 1:
            total, endidx = 0, 0
            docs = []
            topics_by_year = []
            self.result_url = None
            
            result = self.getData(qObj.base_url, qObj.params)
            time.sleep(2)  # allows maximum of 70,834 calls in a day
            total = self.getTotalDocuments(result)
            endidx = self.getStartAndEndIndex(result)[1]
              
            docs = self.parseDocuments(result)
            topics_by_year = self.getTopicFacetList(result)
              
            if total - endidx == 0 or endidx >= 1999 or len(docs) == 0:
                yield total, docs, topics_by_year, self.result_url 
                break
            
            yield total, docs, topics_by_year, self.result_url  
              
            if (total - endidx) < qObj.getBulkSize():
                qObj.setBulkSize(total - endidx)
                  
            qObj.setIndex(endidx + 1)
            
    
    def isAPILimitReached(self):
        """
            Limitation on API call is enforced for fair usage.
            5000 requests/min     i.e 83 requests/sec
            85000 requests/day
        
        """
        pass

    def addAPICalls(self, value=0):
        pass
    
    
class Document(Enum):
    resource_type = 'resource_type'
    genre = 'genre'
    publication = 'publication'
    journal_name = 'journal_name'
    document_title = 'document_title'
    description = 'description'
    authors = 'authors'
    resource_date = 'resource_date'
    volume = 'volume'
    issue = 'issue'
    startpage = 'startpage'
    endpage = 'endpage'
    issn = 'issn'
    doi = 'doi'
    is_peer_reviewed = 'is_peer_reviewed'
    nserc_cat_id = 'nserc_cat_id'

