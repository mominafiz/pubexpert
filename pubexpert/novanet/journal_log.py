from enum import Enum
from db import MongoDb
from datetime import datetime
from pubexpert.utility import config
from pymongo.errors import ConfigurationError


class JournalLog:
    """
        Class to handle journal logs.
        
        Document structure:
            {
                issn: '',
                nserc_cat_id: '',
                nserc_cat_name: '',
                journal_given_name: '',
                total_docs: ''
                facet_topics: []
                year_range : []
                docs_by_year: [
                                {
                                    last_url: '',
                                    total_docs_retrieved: '',
                                    total_docs: '',
                                    status: '',
                                    year: ''
                                    topics: ''
                                }
                            ]
            
            }
    
    """
    __dbObj = None

    def __init__(self, issn, nserc_cat_id, nserc_cat_name, journal_given_name, total_docs, \
                 year_range=[], facet_topics=[], docs_by_year=[]):
        """
            Initializes journal with basic information and sets mongodb connection for 
            CRUD operation on journal_log.        
        """
        
        if issn and nserc_cat_id and nserc_cat_name and journal_given_name  \
             and isinstance(facet_topics, list)  and isinstance(year_range, list)\
             and isinstance(docs_by_year, list):
            
            self.doc = locals()
            del self.doc['self']    
        else:
            raise ValueError("Unexpected None in parameters.")
    
    def connectDb(self, host, port, dbname=None):
        JournalLog.__dbObj = MongoDb(host, port)
        if not dbname:
            dbname = config.dbname
        
        JournalLog.__dbObj.setDatabase(dbname)
        JournalLog.__dbObj.setCollection(config.journal_log_collection_name) 
    
        
    def update(self, last_url, total_docs_retrieved, total_docs, status, year, topics):
        """
            Updates JournalLog if exists else inserts document. 
        """
        self.subdoc = locals()
        self.subdoc['updated_datetime'] = datetime.utcnow()
        del self.subdoc['self']
        
        if not JournalLog.__dbObj:
            raise ConfigurationError("Database not initialized. Call connetDb() first.")
        
        # Before appending check in database 
        docs = [doc for doc in JournalLog.__dbObj.getDocument({'issn':self.doc.get('issn')}, multi=True)]
        if len(docs) == 1:
            update_docs = []
            flag = False
            
            for d in docs[0].get('docs_by_year')[:]:
                if d.get('year') == year:
                    update_docs.append(self.subdoc)
                    flag = True
                else:
                    update_docs.append(d)
            
            if flag == False:
                update_docs.append(self.subdoc)
                
            return JournalLog.__dbObj.updateDocument({'issn':self.doc.get('issn')}, {"$set":{"docs_by_year": update_docs}}, multi=False, upsert=False)
            
            
        elif len(docs) == 0:
            # append subdoc for update
            append = getattr(self.doc.get('docs_by_year'), 'append')
            append(self.subdoc) 
            return JournalLog.__dbObj.insertDocument(self.doc)
        else:
            raise Exception("Redundancy in log.")
        
    def insert(self, doc):
        raise NotImplementedError()
    
    def find(self, issn):
        raise NotImplementedError()
    
    def delete(self):
        raise NotImplementedError()
    
    def __str__(self):
        return repr(self) + "\t" + self.doc.get('issn')
            
    def dropJournalLogCollection(self):
        JournalLog.__dbObj.dropCollection()
        
    def close_connection(self):
        # TODO: Forgot to write the code earlier. Need to fix this.
        raise NotImplementedError("Needs  to be implemented.")


class Status(Enum):
    incomplete = 'incomplete'
    done = 'done'
    notstarted = 'notstarted'
