'''
Make a histogram of normally distributed random numbers and plot the
analytic PDF over it.

Created on Sep 2, 2016

@author: Afiz
'''

import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import norm

# plot histogram
colors = ['b','g','r','c','m','y','k','w']

def histogram(top_dict, bins=25):
    for i,k in enumerate(top_dict.keys()):
        Xedges = top_dict.get(k)
        mu, std = norm.fit(Xedges)
        print "Iteration: " + str(i)
        print "Mean: " + str(mu)
        print "2 sd: " + str(mu+(2*std))
        # Plot the histogram
        plt.hist(Xedges, bins=bins, normed=True, alpha=0.5, color=colors[i])
        
        # Plot the PDF.
        xmin, xmax = plt.xlim()
        x = np.linspace(xmin, xmax, 100)
        p = norm.pdf(x, mu, std)
        plt.plot(x, p, colors[i], linewidth=2)
        plt.title("Normal distribution of labels in top[{i}] probabilities.".format(i=i))
        plt.ylabel("frequency of articles")
        plt.xlabel("Probabilities")
    plt.show()
    #plt.close('all')
    
#     root = getAndChangeToRootDirectory()
#     fpath = os.path.join(root,'resources','plots','fig1.))
#     plt.savefig(fpath)
    
    
