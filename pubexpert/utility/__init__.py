import unicodedata
import os
import sys
import pandas as pd

def convertToASCII(unicode_str):
    if isinstance(unicode_str, unicode):
        return unicodedata.normalize('NFKD', unicode_str).encode('ascii', 'ignore')
    else:
        return unicode_str
    
def getFileContent(resource_dir, filename):
    """
        Returns everything in the file. Not ideal for use if file size is too big.

        Args:
        resource_dir(string): absolute path to file's directory.
        filename(string): filename with extension.
    """
    issn_list = []
    try:
        for line in readFileByEachLine(resource_dir, filename):
            if len(line) == 0:
                break
            issn_list.append(line)
        
    except IOError:
        print sys.exc_info()
    return issn_list

def writeToFile(resource_dir, filename, content, option='a'):
    if not (resource_dir and filename):
        raise ValueError("Unexpected parameter value(s)")
    
    with open(os.path.join(resource_dir, filename), option) as outfile:
        if isinstance(content, list):
            for c in content:
                if isinstance(c, unicode):
                    c = convertToASCII(c)
                outfile.write(c + '\n')
        else:
            outfile.write(content)
            
            
def readFileByEachLine(resource_dir, filename):
    """
        Return each line. This function return generator to iterate file contents
        line by line.
        
        Args:
        resource_dir(string): absolute path to file's directory.
        filename(string): filename with extension.
    """
    infile = None
    try:
        infile = open(os.path.join(resource_dir, filename), 'r')
        
        while True:
            yield infile.readline()
                
    except (IOError, StopIteration):
        print sys.exc_info()
    finally:
        infile.close() if infile else None

def size_mb(docs):
    """
        returns size in Mb. Accepts list of list as docs
    """
    return sum(len(s.encode('utf-8')) for s in docs) / 1e6 


def readCSVToDF(filename, dirpath=None, header_names=None):
    """
        Converts CSV file into pandas dataframe.
        Params:
            filename (string): csv file with extension.
            dirpath(string): Default is the project root directory.
            
    """
    cwd = getAndChangeToRootDirectory()
    if not dirpath:
        dirpath = os.path.join(cwd, 'resources', 'data')
    
    filepath = os.path.join(dirpath, filename)
    df = pd.read_csv(filepath, header=0, names=header_names)
    return df


def createVirtualAuthors(data, target, docs_per_author=1, verbose=False):
    # Convert data_test and target_test into dataframe
    # testdf = pd.DataFrame({'Text':data_test, 'NCatId': target_test})
    
    if docs_per_author < 1:
        raise ValueError('docs_per_author cannot be less than 1.')
    
    virtauth_dict = {}
    virtauth_data_test, virtauth_target_test = [], []
    for vals in zip(data, target):
        key = vals[1]
        d = vals[0]
        if virtauth_dict.has_key(key):
            virtauth_dict[key].append(d)
        else:
            virtauth_dict[key] = [d]
    
    for k, v in virtauth_dict.iteritems():
        vlen = len(v)
        
        iteration = vlen / docs_per_author
        if verbose:
            print "SubCategory: {0}\t#Virtual Authors: {1}\t#Docs: {2}".format(k, iteration, docs_per_author)
        for i in xrange(iteration):
            start = i * docs_per_author
            stop = i * docs_per_author + docs_per_author
            docs = v[start:stop]
            string = ''
            for d in docs:
                string += d + ' '
            virtauth_data_test.append(string)
            virtauth_target_test.append(k)
    data_test = virtauth_data_test
    target_test = virtauth_target_test
    del virtauth_data_test
    del virtauth_target_test
    
    return data_test, target_test


def getAndChangeToRootDirectory():
    resource_dir = os.path.abspath(__file__)
    dname = os.path.dirname(resource_dir)
    #===========================================================================
    # 
    # os.chdir('..')
    # os.chdir(dname)
    #===========================================================================
    return os.path.split(dname)[0]


def convert2unicode(obj):

    if isinstance(obj, str):
        return unicode(obj, errors = 'replace')
    if isinstance(obj,list):
        return [convert2unicode(val) for val in obj]
    if isinstance(obj, dict):
        for k, v in obj.iteritems():
            obj[k] = convert2unicode(v)
    return obj