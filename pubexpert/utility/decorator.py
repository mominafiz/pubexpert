def decorateHeading(func):
    def decoratePrint(string):
        lines = []
        sep = '-' * 75
        lines.append(sep)
        lines.append("\n")
        lines.append(func(string))
        lines.append("\n")
        lines.append(sep)
        lines.append("\n")
        return lines
    return decoratePrint

def verbose(string, verbosity=True):
    
    
    def func_wrapper(func):
        def wrapped_func(*args, **kwargs):
            if verbosity == True:
                print "="*80
                print string
                print "="*80
            return func(*args, **kwargs)
        return wrapped_func
       
    return func_wrapper
    