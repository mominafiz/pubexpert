from pubexpert.utility import getAndChangeToRootDirectory
import os

host = 'localhost'
port = 27017

dbname = 'pubexpert'
test_dbname = 'test_pubexpert'
journal_log_collection_name = 'journal_logs'
application_log_collection_name = 'logs'
# TODO: Ask user to enter collection name to store extracted data
articles_collection_name = 'articles.raw.bsf'

basedir = getAndChangeToRootDirectory()


wikipedia_miner_domain = "localhost"
wikipedia_miner_port = 8080
sunflower_domain = "localhost"
sunflower_port = 8080


# create directories for the project.
# Find absolute path to current directory
root_path = os.path.abspath(os.path.curdir)

resource_path = os.path.join(root_path, 'resources')
if not os.path.exists(resource_path):
    os.mkdir(resource_path)
    
    data_path = os.path.join(resource_path,"data")
    
    if not os.path.exists(data_path):
        os.mkdir(data_path)