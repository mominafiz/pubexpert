### This project is based on the thesis attached with this repo. ###


** Title: ** Towards Expertise Modeling Using Hierarchical Classification and Wikipedia Knowledge.

###_Abstract_###


We define expertise modeling as profiling an expert, a knowledgeable person in one or more domains, based on evidence from research articles into one or more research topics. Millions of enthusiastic researchers contribute in the form of research articles in conferences or journal publications and apply for research grants, and the task of assigning reviewers to research articles and correct research topic for the grant application is trivial.

For our research, we have trained a hierarchical classifier on titles and abstracts of research articles and it predicts one or more research topics for a given article of an expert. We have used traditional Bag-of-Words (BOW) representations of the text which is enriched using a semantic knowledge from Wikipedia's concepts (BOC) and categories (BOK). For each of these document representations, a hierarchical classifier is trained and their outputs are combined using consensus methods to predict a research topic. In reality, research articles can belong to multiple research topics and therefore two approaches to multi-label a research article are proposed.


**Version:** 0.10.1

**Configuration:**
Python, Scipy, Matplotlib, Numpy, Pandas, MongoDb


### _How do I get set up?_ ###

It is recommended to use python's virtualenv and install relevant packages of your project in it. This enables you to separate global packages from your project-specific packages. One of the best installation packages that works with ease is "Anaconda". Install "Anaconda" specific to your OS distribution. 

For more information on using Anaconda, [follow conda cheat sheet.](https://conda.io/docs/using/cheatsheet.html)

 * Once installed, create a new project enviroment from command line using: 
 	`conda create --name <project_name> python=2.7` 
 	
 	
 * Once the virtual environment is created, activate environment: 
		`On windows: activate <project_name>` 
		`On Unix:	 source activate <project_name>`
		
		
 * To deactivate the project enviroment in command-line: 
		`On windows: deactivate <project_name>` 
		`On unix:	source deactivate <project_name>`


 * Installing packages after activating the project environment: 
		`conda install mkl nltk nose numpy pandas pip python-dateutil pytz requests scikit-learn scipy matplotlib pymongo enum34` 
		`pip install xmltodict`
			
### _How to run the project in Eclipse IDE?_ ###

 * You should have project cloned from bitbucket/GitHub to your desktop or eclipse workplace. Use the same name of the folder and path for the project cloned from bitbucket/Github and Eclipse project. This will merge project configuration and project files into the same folder.
 * To run python project on eclipse, you need to install PyDev from eclipse marketplace.
 
 
 * Then create a new PyDev project by selecting Grammer Version= 2.7 and configure Interpreter to point to your environment.
 * Configure interpreter manually by clicking on the hyperlink.
 * In a new pop-up window click on "New". In the new window enter the custom name of the interpreter and browse to your environment's python executable/binary (defaults to installed Anaconda/envs directory). Click "Ok" until you reach create new project window. Now you should see the option to select Interpreter. Select your custom interpreter and click on "Finish".
 * You should now ready to run the project.
	
### _How to get the data for the project?_ ###

The project support classification of research document into three evaluation groups defined by [NSERC](http://www.nserc-crsng.gc.ca/Professors-Professeurs/Grants-Subs/DGPList-PSDListe_eng.asp). The data to train the model on Bag-Of-Words (BOW), Bag-of-Concepts (BOC), and Bag-of-Categories (BOK) can be downloaded in [csv format](https://app.box.com/s/ljbfdr668qp8twoheufojoapjoxguy1c). The zip file consists of files in the format "articles_{gcm|cs|bsf}_v2_after_2000_filtered.csv" where gcm, cs, and bsf are evaluation groups "Genes, Cells, and Molecules", "Computer Science", "Biological Systems and Functions", respectively. 
	
Ignore other files as they are not used anymore now.
	
Now, put all these files under resources/data folder of the project.
	
 * **Database Configuration** 
 This project uses MongoDB as the database to store and process the results. All the articles and its metadata is stored in JSON format as a document. The database is accessed in python using the pymongo client. 
The configuration specific to this project can be found in config.py module of utility package. Edit this file to suit your need.


* **Dependencies** 
The requirements for the project are listed in environment.yml. 


* **How to run tests** 
As of the current version, tests are written for NovaNet API.


* **Deployment instructions** 
	Deployment of the project is not covered here. One can choose to run the project on Apache HTTP server or run locally by invoking one of the scripts in the scripts folder.

### Project Structure ###

* _**pubexpert**_: is the root directory of the project and it consists of different packages that make this project run. 
	* _**log4mongo**_: This package implements logging mechanism using MongoDB as a backend. Though it is not being used in the project, future support is expected.
	* _**ml**_: This package consists of modules that helps with machine learning using scipy package.
		classifier_helper.py: consists of helper functions and classes used repeatedly by the scripts in scripts.py files for machine learning.
		* _**pca.py, roc.py, X2.py**_: These files are not relevant to the final version of the proeject. One can re-use for research purpose.
	* _**novanet**_: This package is important that helps to extract data from Novanet Inc via Dalhousie University Libraries. It will not work for code running on any machine. It runs on white-listed IP within Dal.
		* _**api.py**_: Makes calls to a Novanet API and returns a response for each API call as a list of record. The information extracted from the API call is limited to the scope of the project.
		* _**db.py**_: Create, Read, Update and Delete (CRUD) operations are handled by this module and allow interaction with MongoDB.
	* _**journal_log**_: This module maintains the log about the API calls made to extract the data. The logs are used to resume the data extraction stopped due to errors.
		* **_urlbuilder.py_**: It is helper class that allows to build an URL for making a call to Novanet.
	* **_resources_**: This is the folder where the data, important files, input and output from the program and tests are stored.
	* **_scripts_**: This package consists of modules to run the scripts for data extraction, data pre-processiong, hierarhical classification, one-vs-rest classification, wikification, wikipedia categoriration, one-class classification, and more.
		* **_data_extraction.py_**: used to extract data from Novanet API and store the documents in collection defined in config.py under utility package. 
		* **_preprocess.py_**: it tokenizes the title and abstract by removing stop words and store the processed text in "bow_text". The stemmed words are stored in "bow_text_v2". After extracting a data and storing it in a collection, it is important to check duplicates and resolve conflicts for documents in different language. A ad-hoc query for pre-processing is done using series of mongodb queries in resources/preprocess_data.js file.
		* **_wikify.py_**: This script depends on wikify package responsible for API calls to wikipedia miner toolkit hosted locally (not a part of this project) and stores annotated text in the same document of the collection.
		* **_wiki_category.py_**: This script depends on sunflower package which is responsible to extract categories for a concept extracted from Wikipedia Miner Toolkit by running wikify.py
		* **_hierarchical\_classifier.py_**: It depends on .csv file exported from Mongodb for each NSERC evaluation group. A hierarchical classifier is trained for a given list of data in .csv file and other input parameters and write an output to MongoDb collection. Using this script, a hierarchical classifier for BOW, BOC, and BOK can be trained and results can be stored in mongodb in the same collection.
		* **_hierarchical\_results.py_**: This script depends on hierarchical_classifier.py, which aggregates the results from the output of BOW, BOC and BOK hierarchical classifer using consensus method. 
		* **_run\_regression.py_**: It is one of the consensus method used to generalize the result. Other consensus method are implemented in heirarchical_results.py 
		* **_multilabel.py_**: This script implements two methods, threshold based hierarchical classifier and consensus method, to multi-label a document. 
		* **_run\_tsne.py_**: A t-SNE algorithm that allows to visualize documents in 2D or 3D. Current implementation is limited to 2D.
		* **_matplot\_colormpas.py_**: Use this script for various color options that can be used with matplot lib that plots the graphs.
		* _**one_class_classifier_outliers.py, run_multiclass\*, run_one_class_classifer.py**_: These scripts and other scripts not mentioned here are no longer relevant to the final version of thesis. There left as it is for the purpose of reproducing the research.
	* **_utility_**: This package consists of modules for project configuration, helper functions for the project, other than machine learning related helper. 
		* **_config.py_**: The project related configuration are defined in this modules and needs to be changes as per your need.
		* **_decorator.py_**: Impements various decorators used on functions to generate console based messages.
		* _**\_\_init\_\_.py**_: contains various frequently used helper functions used in the project.
		
		

### Who do I talk to? ###

* Repo owner or admin
	The repository is owned by Afiz Momin. Please feel free to contribute. You can reach me at mominafiz@gmail.com.
	
* Other community or team contact